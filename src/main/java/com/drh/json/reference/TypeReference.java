package com.drh.json.reference;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import lombok.Getter;

/**
 * Defines a type reference to deserialize a generically typed object.
 * A common example of this is:
 * <pre>
 * JsonMapper mapper = new JsonMapper();
 * List&lt;String&gt; list = mapper.toJson(
 *        "[\"hello\",\"world\"]",
 *        new TypeReference&lt;List&lt;String&gt;&gt;(){});
 * </pre>
 *
 * @param <T> the type to deserialize to
 */
public class TypeReference<T> {

  @Getter
  private final Type type;

  /**
   * Defines a type reference to deserialize a generically typed object.
   * A common example of this is:
   * <pre>
   * JsonMapper mapper = new JsonMapper();
   * List&lt;String&gt; list = mapper.toJson(
   *        "[\"hello\",\"world\"]",
   *        new TypeReference&lt;List&lt;String&gt;&gt;(){});
   * </pre>
   */
  public TypeReference() {
    this.type = parseType();
  }

  private Type parseType() {
    Type superclassType = this.getClass().getGenericSuperclass();
    if (superclassType instanceof ParameterizedType parameterizedType) {
      Type[] types = parameterizedType.getActualTypeArguments();
      if (types == null || types.length != 1) {
        throw new IllegalArgumentException("Invalid Parameterized Type Reference");
      }
      return types[0];
    }
    throw new IllegalArgumentException("Invalid Type Reference");
  }
}
