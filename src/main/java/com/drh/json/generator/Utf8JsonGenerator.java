package com.drh.json.generator;

import com.drh.json.serializer.utils.TypeStack;
import com.drh.json.styling.JsonStyling;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

/**
 * Defines a {@link JsonGenerator} that can write UTF-8 characters.
 */
//TODO: implement annotation processing
public class Utf8JsonGenerator extends JsonGenerator {
  private static final byte LCURLY = '{';
  private static final byte RCURLY = '}';
  private static final byte LSQUARE = '[';
  private static final byte RSQUARE = ']';
  private static final byte COLON = ':';
  private static final byte COMMA = ',';
  private static final byte QUOTE = '"';
  private static final byte SPACE = ' ';
  private static final byte NEW_LINE = '\n';
  private static final byte BACKSLASH = '\\';
  private static final byte UNICODE_ESCAPE = 'u';

  private static final Map<Character, Byte> SPECIAL_CHARS_MAP = Map.of(
      '"', QUOTE,
      '\\', BACKSLASH,
      '\n', (byte) 'n',
      '\r', (byte) 'r',
      '\b', (byte) 'b',
      '\f', (byte) 'f',
      '\t', (byte) 't');
  private static final byte[] HEX_BYTES
      = new byte[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

  private static final int BYTE_BUFFER_SIZE = 6000;

  private static final byte[] nullAsBytes = new byte[] {'n', 'u', 'l', 'l'};
  private static final byte[] trueAsBytes = new byte[] {'t', 'r', 'u', 'e'};
  private static final byte[] falseAsBytes = new byte[] {'f', 'a', 'l', 's', 'e'};

  private int byteBufferIndex;

  private int currentDepth;

  private final byte[] byteBuffer;

  private final ByteArrayOutputStream outputOs;
  private final JsonStyling jsonStyling;

  /**
   * Defines a {@link JsonGenerator} that can write UTF-8 characters.
   *
   * @param jsonStyling json structure styling
   */
  public Utf8JsonGenerator(JsonStyling jsonStyling) {
    this.jsonStyling = jsonStyling;
    this.outputOs = new ByteArrayOutputStream();
    this.byteBuffer = new byte[BYTE_BUFFER_SIZE];
    this.byteBufferIndex = 0;
    this.currentDepth = 0;
  }

  @Override
  public JsonGenerator objectStart() {
    formatIfInArray();
    ensureStatus(EXPECT_START | EXPECT_VALUE, "Cannot start object");
    writeByte(LCURLY);

    currentDepth++;
    setInternalStatus(EXPECT_FIELD);
    getTypeStack().push(TypeStack.TYPE_OBJ);
    return this;
  }

  @Override
  public JsonGenerator objectEnd() {
    ensureStatus(EXPECT_FIELD | OK_AFTER_COMMA_OR_END, "Cannot end object");
    currentDepth--;

    if (getInternalStatus() == OK_AFTER_COMMA_OR_END) {
      writeNewLine();
      writeSpaces(jsonStyling.getTabSize() * currentDepth);
    }

    writeByte(RCURLY);

    getTypeStack().pop();
    setInternalStatus(getTypeStack().head() == TypeStack.TYPE_NONE ? DONE : OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator arrayStart() {
    formatIfInArray();
    ensureStatus(EXPECT_START | EXPECT_VALUE, "Cannot start array");
    writeByte(LSQUARE);

    currentDepth++;
    setInternalStatus(EXPECT_VALUE);
    getTypeStack().push(TypeStack.TYPE_ARR);
    return this;
  }

  @Override
  public JsonGenerator arrayEnd() {
    ensureStatus(EXPECT_VALUE | OK_AFTER_COMMA_OR_END, "Cannot end array");
    currentDepth--;

    if (getInternalStatus() == OK_AFTER_COMMA_OR_END) {
      writeNewLine();
      writeSpaces(jsonStyling.getTabSize() * currentDepth);
    }

    writeByte(RSQUARE);

    getTypeStack().pop();
    setInternalStatus(getTypeStack().head() == TypeStack.TYPE_NONE ? DONE : OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator write(String field, String value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator write(String field, char value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator write(String field, int value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator write(String field, long value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator write(String field, float value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator write(String field, double value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator write(String field, boolean value) {
    return writeField(field).writeValue(value);
  }

  @Override
  public JsonGenerator writeField(String field) {
    Objects.requireNonNull(field,  "field cannot ne null");
    ensureStatus(EXPECT_FIELD | OK_AFTER_COMMA_OR_END, "Cannot write field '%s'", field);

    if (getInternalStatus() == OK_AFTER_COMMA_OR_END) {
      writeByte(COMMA);
      setInternalStatus(EXPECT_FIELD);
    }

    writeNewLine();
    writeSpaces(jsonStyling.getTabSize() * currentDepth);

    writeString(field);

    writeByte(COLON);
    writeSpaces(jsonStyling.getKeyValueSeparatorSize());

    setInternalStatus(EXPECT_VALUE);
    return this;
  }

  @Override
  public JsonGenerator writeValue(String value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write String value '%s'", value);

    if (value == null) {
      writeNull();
    } else {
      writeString(value);
    }
    setInternalStatus(OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator writeValue(char value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write char value '%s'", value);

    writeByte(QUOTE);
    writeByte((byte) value);
    writeByte(QUOTE);

    setInternalStatus(OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator writeValue(int value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write int value '%s'", value);
    writeNumber(value);
    return this;
  }

  @Override
  public JsonGenerator writeValue(long value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write long value '%s'", value);
    writeNumber(value);
    return this;
  }

  @Override
  public JsonGenerator writeValue(float value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write float value '%s'", value);
    byte[] floatBytes = String.valueOf(value).getBytes();
    writeBytes(floatBytes);
    setInternalStatus(OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator writeValue(double value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write double value '%s'", value);
    byte[] doubleBytes = String.valueOf(value).getBytes();
    writeBytes(doubleBytes);
    setInternalStatus(OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator writeValue(boolean value) {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write boolean value '%s'", value);
    if (value) {
      writeBytes(trueAsBytes);
    } else {
      writeBytes(falseAsBytes);
    }
    setInternalStatus(OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public JsonGenerator writeNull() {
    formatIfInArray();
    ensureStatus(EXPECT_VALUE, "Cannot write null value");
    writeBytes(nullAsBytes);
    setInternalStatus(OK_AFTER_COMMA_OR_END);
    return this;
  }

  @Override
  public String getOutput() {
    ensureStatus(DONE, "Cannot get output, json is incomplete");

    flushBuffer();
    String output = outputOs.toString(StandardCharsets.UTF_8);
    outputOs.reset();
    setInternalStatus(EXPECT_START);
    return output;
  }

  private void flushBuffer() {
    if (byteBufferIndex > 0) {
      outputOs.write(byteBuffer, 0, byteBufferIndex);
      byteBufferIndex = 0;
    }
  }

  private void writeNumber(long value) {
    if (value < 0) {
      writeByte((byte) '-');
    }

    if (value == 0) {
      writeByte((byte) '0');
    } else {
      byte[] numAsBytes = new byte[10];
      int byteCount = 0;
      while (value != 0) {
        numAsBytes[byteCount++] = (byte) (Math.abs(value % 10) + 48);
        value /= 10;
      }
      for (int i = byteCount - 1; i >= 0; i--) {
        writeByte(numAsBytes[i]);
      }
    }
    setInternalStatus(OK_AFTER_COMMA_OR_END);
  }

  private void writeString(String value) {
    int length = value.length();
    char[] chars = new char[length];
    value.getChars(0, length, chars, 0);

    writeByte(QUOTE);
    for (int i = 0; i < length; i++) {
      char c = chars[i];
      writeByteEncoded(c);
    }
    writeByte(QUOTE);
  }

  private void writeSpaces(int count) {
    if (count <= 0) {
      return;
    }

    for (int i = 0; i < count; i++) {
      writeByte(SPACE);
    }
  }

  private void writeNewLine() {
    if (jsonStyling.fieldNewLine()) {
      writeByte(NEW_LINE);
    }
  }

  private void formatIfInArray() {
    if (getTypeStack().head() == TypeStack.TYPE_ARR) {
      if (getInternalStatus() == OK_AFTER_COMMA_OR_END) {
        writeByte(COMMA);
      }
      writeNewLine();
      writeSpaces(jsonStyling.getTabSize() * currentDepth);
      setInternalStatus(EXPECT_VALUE);
    }
  }

  private void writeByte(byte b) {
    if (byteBufferIndex + 1 >= byteBuffer.length) {
      flushBuffer();
    }
    byteBuffer[byteBufferIndex++] = b;
  }

  private void writeBytes(byte[] bytes) {
    for (byte b : bytes) {
      writeByte(b);
    }
  }

  private void writeByteEncoded(char c) {
    if (SPECIAL_CHARS_MAP.containsKey(c)) {
      writeByte(BACKSLASH);
      writeByte(SPECIAL_CHARS_MAP.get(c));
    } else if (c < 20) {
      writeByteFullUnicodeEscaped((byte) c);
    } else if (c > 127) {
      byte b1 = (byte) (0xc0 | (c >> 6));
      byte b2 = (byte) (0x80 | (c & 0x3f));
      writeByte(b1);
      writeByte(b2);
    } else {
      writeByte((byte) c);
    }
  }

  private void writeByteFullUnicodeEscaped(byte b) {
    writeByte(BACKSLASH);
    writeByte(UNICODE_ESCAPE);

    byte[] bytes = new byte[] {'0', '0', '0', '0'};
    int bytesIndex = 3;
    while (b > 0) {
      bytes[bytesIndex--] = HEX_BYTES[b % 16];
      b /= 16;
    }
    writeBytes(bytes);
  }
}