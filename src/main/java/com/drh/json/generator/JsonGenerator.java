package com.drh.json.generator;

import com.drh.json.exception.JsonGeneratorException;
import com.drh.json.serializer.utils.TypeStack;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * JsonGenerator provides functionality to create JSON strings.
 */
public abstract class JsonGenerator {

  /**
   * Flag indicating an expected value of '{' or '['
   *
   * @see JsonGenerator#objectStart()
   * @see JsonGenerator#arrayStart()
   */
  protected static final int EXPECT_START           = 0x01;

  /**
   * Flag indicating an expected value of ',' (the next value), '}', or ']'
   *
   * @see JsonGenerator#objectEnd()
   * @see JsonGenerator#arrayEnd()
   */
  protected static final int OK_AFTER_COMMA_OR_END  = 0x02;

  /**
   * Flag indicating the next value is expected to be the field name in an object
   *
   * @see JsonGenerator#writeField(String)
   */
  protected static final int EXPECT_FIELD           = 0x04;

  /**
   * Flag indicating the next value is expected to be the field value in an object
   *
   * @see JsonGenerator#writeValue
   */
  protected static final int EXPECT_VALUE           = 0x08;

  /**
   * Flag indicating we expect no more json generation to be requested
   */
  protected static final int DONE                   = 0x10;

  @Getter(value = AccessLevel.PROTECTED)
  @Setter(value = AccessLevel.PROTECTED)
  private int internalStatus;

  @Getter(value = AccessLevel.PROTECTED)
  private final TypeStack typeStack;

  /**
   * JsonGenerator provides functionality to create JSON strings.
   */
  public JsonGenerator() {
    this.internalStatus = EXPECT_START;
    this.typeStack = new TypeStack();
  }

  /**
   * Starts a JSON object '{'.
   *
   * @return the generator instance.
   */
  public abstract JsonGenerator objectStart();

  /**
   * Ends a JSON object '}'.
   *
   * @return the generator instance.
   */
  public abstract JsonGenerator objectEnd();

  /**
   * Starts a JSON array '['.
   *
   * @return the generator instance.
   */
  public abstract JsonGenerator arrayStart();

  /**
   * Ends a JSON array ']'.
   *
   * @return the generator instance.
   */
  public abstract JsonGenerator arrayEnd();

  /**
   * Write a JSON field and {@link String} value. e.g. "key":"value"
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, String value);

  /**
   * Write a JSON field and {@code char} value. e.g. "key":"a"
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, char value);

  /**
   * Write a JSON field and {@code int} value. e.g. "key":123
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, int value);

  /**
   * Write a JSON field and {@code long} value. e.g. "key":123
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, long value);

  /**
   * Write a JSON field and {@code float} value. e.g. "key":1.5
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, float value);

  /**
   * Write a JSON field and {@code double} value. e.g. "key":1.5
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, double value);

  /**
   * Write a JSON field and {@code boolean} value. e.g. "key":true
   *
   * @param field the field name.
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator write(String field, boolean value);

  /**
   * Write a JSON field. e.g. "key":
   *
   * @param field the field name.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeField(String field);

  /**
   * Write a {@link String} value. e.g. "value"
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(String value);

  /**
   * Write a {@code char} value. e.g. "a"
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(char value);

  /**
   * Write an {@code int} value. e.g. 1
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(int value);

  /**
   * Write a {@code long} value. e.g. 1
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(long value);

  /**
   * Write a {@code float} value. e.g. 1.2F
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(float value);

  /**
   * Write a {@code double} value. e.g. 1.2D
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(double value);

  /**
   * Write a {@code boolean} value. e.g. true
   *
   * @param value the value.
   * @return the generator instance.
   */
  public abstract JsonGenerator writeValue(boolean value);

  /**
   * Write a {@code null} value.
   *
   * @return the generator instance.
   */
  public abstract JsonGenerator writeNull();

  /**
   * Returns the generated json if valid
   *
   * @return json value
   */
  public abstract String getOutput();

  /**
   * Checks that the json generation requested is something that was expected.
   * If the expected doesn't match the current status, throw a {@link JsonGeneratorException}
   *
   * @param status the type of generation expected
   * @param message the error message if the status is not what is expected
   * @param args error message arguments
   *
   * @see JsonGenerator#EXPECT_START
   * @see JsonGenerator#OK_AFTER_COMMA_OR_END
   * @see JsonGenerator#EXPECT_FIELD
   * @see JsonGenerator#EXPECT_VALUE
   * @see JsonGenerator#DONE
   */
  protected void ensureStatus(int status, String message, Object... args) {
    if ((internalStatus & status) != internalStatus) {
      String throwMessage = message;
      if (args.length > 0) {
        throwMessage = String.format(message, args);
      }
      throw new JsonGeneratorException(throwMessage + ": " + getStatusDesc());
    }
  }

  private String getStatusDesc() {
    return switch (internalStatus) {
      case EXPECT_START -> "Expected '{' or '['";
      case OK_AFTER_COMMA_OR_END -> "Expected ',' or '}' or ']'";
      case EXPECT_FIELD -> "Expected field";
      case EXPECT_VALUE -> "Expected value";
      case DONE -> "Json Structure complete, no more can be added";
      default -> "Unexpected error occurred";
    };
  }
}
