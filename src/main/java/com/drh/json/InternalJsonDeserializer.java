package com.drh.json;

import com.drh.json.deserializer.JsonDeserializer;
import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNode;
import com.drh.json.parser.JsonParser;
import com.drh.json.property.ParameterizedTypeProperty;
import com.drh.json.property.Property;
import com.drh.json.reference.TypeReference;
import java.util.Objects;

final class InternalJsonDeserializer {

  private static final String ERR_SRC_NULL = "src is null";
  private static final String ERR_TYPE_REF_NULL = "typeRef is null";

  private final DeserializerManager manager;

  InternalJsonDeserializer(DeserializerManager manager) {
    //TODO: setup standard config (fail_on_unknown_properties, etc.)
    this.manager = manager;
  }

  <T> void addDeserializer(Class<T> clazz, JsonDeserializer<T> deserializer) {
    manager.addDeserializer(clazz, deserializer);
  }

  <T> T deserialize(String src, Class<T> clazz) {
    Objects.requireNonNull(src, ERR_SRC_NULL);

    return deserialize(JsonParser.parse(src), clazz, null);
  }

  <T> T deserialize(String src, TypeReference<T> typeRef) {
    Objects.requireNonNull(src, ERR_SRC_NULL);
    Objects.requireNonNull(src, ERR_TYPE_REF_NULL);

    Property property = new ParameterizedTypeProperty(typeRef.getType());
    return deserialize(JsonParser.parse(src), (Class<T>) property.getClassType(), property);
  }

  <T> T deserialize(JsonNode src, TypeReference<T> typeRef) {
    Objects.requireNonNull(src, ERR_SRC_NULL);
    Objects.requireNonNull(src, ERR_TYPE_REF_NULL);

    Property property = new ParameterizedTypeProperty(typeRef.getType());
    return deserialize(src, (Class<T>) property.getClassType(), property);
  }

  <T> T deserialize(JsonNode node, Class<T> clazz, Property property) {
    Objects.requireNonNull(node, "node is null");
    Objects.requireNonNull(clazz, "clazz is null");

    return manager.deserialize(clazz, node, property);
  }
}