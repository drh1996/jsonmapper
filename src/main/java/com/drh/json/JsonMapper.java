package com.drh.json;

import com.drh.json.deserializer.DateDeserializer;
import com.drh.json.deserializer.JsonDeserializer;
import com.drh.json.deserializer.LocalDateDeserializer;
import com.drh.json.deserializer.LocalDateTimeDeserializer;
import com.drh.json.deserializer.PrimitiveDeserializers;
import com.drh.json.deserializer.UuidDeserializer;
import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonNull;
import com.drh.json.node.JsonObject;
import com.drh.json.node.JsonPrimitive;
import com.drh.json.reference.TypeReference;
import com.drh.json.serializer.DateSerializer;
import com.drh.json.serializer.JsonArraySerializer;
import com.drh.json.serializer.JsonNullSerializer;
import com.drh.json.serializer.JsonObjectSerializer;
import com.drh.json.serializer.JsonPrimitiveSerializer;
import com.drh.json.serializer.JsonSerializer;
import com.drh.json.serializer.LocalDateSerializer;
import com.drh.json.serializer.LocalDateTimeSerializer;
import com.drh.json.serializer.PrimitiveSerializers;
import com.drh.json.serializer.UuidSerializer;
import com.drh.json.serializer.manager.SerializerManager;
import com.drh.json.styling.JsonStyling;
import com.drh.json.styling.PrettyJsonStyling;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * JsonMapper provides functions to convert a {@link String} to a Plain Old Java Object (POJO)
 * and vice versa.
 * <br><br>
 * JsonMapper also provides default serializers and deserializers for primitives and collections,
 * as well as functionality to define your own serializers and deserializers for  different classes,
 * {@link JsonStyling} can be used to define how serialized JSON strings will be formatted.
 *
 * @since 1.0
 * @see JsonNode
 * @see JsonSerializer
 * @see JsonDeserializer
 * @see JsonStyling
 * @author Dan Hayes
 */
public final class JsonMapper {

  private static final JsonStyling DEFAULT_JSON_STYLING =
      new JsonStyling(0, 0, false);
  private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
  private static final DateTimeFormatter DEFAULT_LOCAL_DATE_TIME_FORMAT =
      DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT);
  private static final DateTimeFormatter DEFAULT_LOCAL_DATE_FORMAT =
      DateTimeFormatter.ofPattern("yyyy-MM-dd");

  private final InternalJsonSerializer internalJsonSerializer;
  private final InternalJsonDeserializer internalJsonDeserializer;

  /**
   * Creates a JsonMapper that will serialize with no extra whitespaces.
   */
  public JsonMapper() {
    this(DEFAULT_JSON_STYLING);
  }

  /**
   * Creates a JsonMapper that will serialize using custom stying.
   *
   * @param jsonStyling the stying to use when serializing.
   */
  public JsonMapper(JsonStyling jsonStyling) {
    internalJsonSerializer = new InternalJsonSerializer(
        jsonStyling, new SerializerManager(getDefaultSerializers()));
    internalJsonDeserializer = new InternalJsonDeserializer(
        new DeserializerManager(getDefaultDeserializers()));
  }

  /**
   * Create a JSON representation of an object.
   *
   * @param obj the object to serialize
   * @param <T> the object type
   * @return a JSON representation of the object.
   */
  public <T> String toJson(T obj) {
    return internalJsonSerializer.serialize(obj);
  }

  /**
   * Create a JSON representation of an object using custom styling.
   *
   * @param obj the object to serialize
   * @param styling the styling to use when serializing this object
   * @param <T> the object type
   * @return a JSON representation of the object.
   */
  public <T> String toJson(T obj, JsonStyling styling) {
    return internalJsonSerializer.serialize(obj, styling);
  }

  /**
   * Convert a JSON {@link String} to an object.
   *
   * @param src   the JSON string to convert.
   * @param clazz the type of object to convert to.
   * @param <T>   the object type.
   * @return an object created from a JSON string.
   */
  public <T> T fromJson(String src, Class<T> clazz) {
    return internalJsonDeserializer.deserialize(src, clazz);
  }

  /**
   * Convert a JSON {@link String} to an object using a type reference.
   *
   * @param src   the JSON string to convert.
   * @param typeRef the type of object to convert to.
   * @param <T>   the object type.
   * @return an object created from a JSON string.
   * @see TypeReference
   */
  public <T> T fromJson(String src, TypeReference<T> typeRef) {
    return internalJsonDeserializer.deserialize(src, typeRef);
  }

  /**
   * Convert a {@link JsonNode} to an object.
   *
   * @param src   the node to convert.
   * @param clazz the type of object to convert to.
   * @param <T>   the object type.
   * @return an object created from a {@link JsonNode}.
   */
  public <T> T fromJson(JsonNode src, Class<T> clazz) {
    return internalJsonDeserializer.deserialize(src, clazz, null);
  }

  /**
   * Convert a {@link JsonNode} to an object using a type reference.
   *
   * @param src     the node to convert.
   * @param typeRef the type of object to convert to.
   * @param <T>     the object type.
   * @return an object created from a JSON string.
   * @see TypeReference
   */
  public <T> T fromJson(JsonNode src, TypeReference<T> typeRef) {
    return internalJsonDeserializer.deserialize(src, typeRef);
  }

  /**
   * Add a serializer to be used whenever an object of a given type is being serialized.
   *
   * @param clazz      the type to use this serializer on.
   * @param serializer the serializer to use.
   * @param <T>        the object type.
   */
  public <T> void addSerializer(Class<T> clazz, JsonSerializer<T> serializer) {
    internalJsonSerializer.addSerializer(clazz, serializer);
  }

  /**
   * Add a deserializer to be used whenever a {@link String} is being deserialized to an object.
   *
   * @param clazz        the type to use this deserializer on.
   * @param deserializer the deserializer to use.
   * @param <T>          the object type.
   */
  public <T> void addDeserializer(Class<T> clazz, JsonDeserializer<T> deserializer) {
    internalJsonDeserializer.addDeserializer(clazz, deserializer);
  }

  private static Map<Class<?>, JsonSerializer> getDefaultSerializers() {
    Map<Class<?>, JsonSerializer> defaultSerializers =
        new HashMap<>(PrimitiveSerializers.getPrimitiveSerializers());
    defaultSerializers.put(Date.class,
        new DateSerializer(new SimpleDateFormat(DEFAULT_DATE_FORMAT)));
    defaultSerializers.put(LocalDateTime.class,
        new LocalDateTimeSerializer(DEFAULT_LOCAL_DATE_TIME_FORMAT));
    defaultSerializers.put(LocalDate.class,
        new LocalDateSerializer(DEFAULT_LOCAL_DATE_FORMAT));
    defaultSerializers.put(UUID.class, new UuidSerializer());


    defaultSerializers.put(JsonObject.class, new JsonObjectSerializer());
    defaultSerializers.put(JsonArray.class, new JsonArraySerializer());
    defaultSerializers.put(JsonPrimitive.class, new JsonPrimitiveSerializer());
    defaultSerializers.put(JsonNull.class, new JsonNullSerializer());

    return defaultSerializers;
  }

  private static Map<Class<?>, JsonDeserializer> getDefaultDeserializers() {
    Map<Class<?>, JsonDeserializer> deserializerMap =
        new HashMap<>(PrimitiveDeserializers.getPrimitiveDeserializers());
    deserializerMap.put(JsonNode.class,
        (JsonDeserializer<JsonNode>) (node, manager) -> node);
    deserializerMap.put(JsonObject.class,
        (JsonDeserializer<JsonObject>) (node, manager) -> node.getAsObject());
    deserializerMap.put(JsonArray.class,
        (JsonDeserializer<JsonArray>) (node, manager) -> node.getAsArray());
    deserializerMap.put(Date.class,
        new DateDeserializer(new SimpleDateFormat(DEFAULT_DATE_FORMAT)));
    deserializerMap.put(LocalDateTime.class,
        new LocalDateTimeDeserializer(DEFAULT_LOCAL_DATE_TIME_FORMAT));
    deserializerMap.put(LocalDate.class,
        new LocalDateDeserializer(DEFAULT_LOCAL_DATE_FORMAT));
    deserializerMap.put(UUID.class, new UuidDeserializer());
    return deserializerMap;
  }

  /**
   * Creates a builder for a {@link JsonMapper}.
   *
   * @return An initial Builder.
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * A builder for a {@link JsonMapper}.
   */
  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  public static class Builder {
    private JsonStyling styling;

    /**
     * Define the styling to use for a {@link JsonMapper} when serializing.
     *
     * @param jsonStyling the styling to use.
     * @return the current builder instance.
     * @see JsonStyling
     */
    public Builder styling(JsonStyling jsonStyling) {
      this.styling = jsonStyling;
      return this;
    }

    /**
     * Use {@link PrettyJsonStyling} for a {@link JsonMapper} when serializing.
     *
     * @return the current builder instance.
     * @see PrettyJsonStyling
     */
    public Builder prettyStyling() {
      this.styling = new PrettyJsonStyling();
      return this;
    }

    /**
     * Build the {@link JsonMapper}.
     *
     * @return the json mapper instance.
     * @see JsonMapper
     */
    public JsonMapper build() {
      if (styling == null) {
        styling = DEFAULT_JSON_STYLING;
      }
      return new JsonMapper(styling);
    }
  }
}
