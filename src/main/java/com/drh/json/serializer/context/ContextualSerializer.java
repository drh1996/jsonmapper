package com.drh.json.serializer.context;

import com.drh.json.property.Property;
import com.drh.json.serializer.JsonSerializer;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * An interface that will be called for a {@link JsonSerializer} whenever context (e.g. state)
 * needs to be used in serialization.
 * Example see: {@link com.drh.json.serializer.ArraySerializer ArraySerializer}
 */
public interface ContextualSerializer {

  /**
   * An interface that will be called for a {@link JsonSerializer} whenever context (e.g. state)
   * needs to be used in serialization.
   * Example see: {@link com.drh.json.serializer.ArraySerializer ArraySerializer}
   *
   * @param manager to pass on the serializing of an object to another serializer
   * @param property properties of the class / field / method / parameterized type
   *                 being serialized
   * @return a serializer with a context (e.g. state)
   */
  JsonSerializer<?> createContextualSerializer(SerializerManager manager, Property property);
}
