package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.Collection;

/**
 * A standard java collection serializer. Serializing of the collection elements will be delegated
 * to a custom {@link JsonSerializer}, if defined, or a default serializer.
 */
public class CollectionSerializer implements JsonSerializer<Object> {

  @Override
  public void serialize(Object obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
      return;
    }

    Object[] value = collectionToArray(obj);

    gen.arrayStart();
    if (value.length > 0) {
      //get array type
      Class<?> arrayType = getClassType(value);
      for (Object o : value) {
        serializerManager.serialize(arrayType, o, gen);
      }
    }
    gen.arrayEnd();
  }

  private static Object[] collectionToArray(Object value) {
    Collection<?> collection = (Collection<?>) value;
    Object[] arr = new Object[collection.size()];
    int i = 0;
    for (Object o : collection) {
      arr[i++] = o;
    }
    return arr;
  }

  private static Class<?> getClassType(Object[] arr) {
    for (Object o : arr) {
      if (o != null) {
        return o.getClass();
      }
    }
    return Object.class;
  }
}
