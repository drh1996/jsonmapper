package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * A standard java enum serializer. Serializing of the enum is dictated by {@link Enum#name()}
 */
public class EnumSerializer implements JsonSerializer<Enum<?>> {

  @Override
  public void serialize(Enum<?> obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
    } else {
      gen.writeValue(obj.name());
    }
  }
}
