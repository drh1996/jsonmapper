package com.drh.json.serializer;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.generator.JsonGenerator;
import com.drh.json.property.Property;
import com.drh.json.serializer.context.ContextualSerializer;
import com.drh.json.serializer.manager.SerializerManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Converts a {@link LocalDate} to a JSON string with a given format.
 */
public class LocalDateSerializer implements JsonSerializer<LocalDate>, ContextualSerializer {

  private final DateTimeFormatter dateTimeFormatter;

  /**
   * Creates a {@link JsonSerializer} to convert a
   * {@link LocalDate} to a String with a given format.
   *
   * @param format the format to convert to.
   * @throws IllegalArgumentException if the format is invalid.
   */
  public LocalDateSerializer(DateTimeFormatter format) {
    this.dateTimeFormatter = format;
  }

  @Override
  public JsonSerializer<LocalDate> createContextualSerializer(
      SerializerManager manager, Property property) {

    if (property.hasAnnotation(JsonDateFormat.class)) {
      return new LocalDateSerializer(
          DateTimeFormatter.ofPattern(property.getAnnotation(JsonDateFormat.class).format()));
    }
    return this;
  }

  @Override
  public void serialize(LocalDate obj, JsonGenerator gen, SerializerManager manager) {
    if (obj != null) {
      gen.writeValue(dateTimeFormatter.format(obj));
    } else {
      gen.writeNull();
    }
  }
}
