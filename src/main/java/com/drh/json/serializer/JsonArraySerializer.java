package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * A default {@link JsonArray} serializer. Serializing of the arrays elements will be delegated to
 * a custom {@link JsonSerializer}, if defined, or a default serializer.
 */
public class JsonArraySerializer implements JsonSerializer<JsonArray> {

  @Override
  public void serialize(JsonArray obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
      return;
    }

    gen.arrayStart();
    for (JsonNode node : obj) {
      serializerManager.serialize(node.getClass(), node, gen);
    }
    gen.arrayEnd();
  }
}
