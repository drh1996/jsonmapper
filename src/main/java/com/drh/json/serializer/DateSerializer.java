package com.drh.json.serializer;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.generator.JsonGenerator;
import com.drh.json.property.Property;
import com.drh.json.serializer.context.ContextualSerializer;
import com.drh.json.serializer.manager.SerializerManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Converts a {@link Date} to a JSON string with a given format.
 */
public class DateSerializer implements JsonSerializer<Date>, ContextualSerializer {

  private final DateFormat dateFormat;

  /**
   * Creates a {@link JsonSerializer} to convert a {@link Date} to a String with a given format.
   *
   * @param format the format to convert to.
   * @throws IllegalArgumentException if the format is invalid.
   */
  public DateSerializer(DateFormat format) {
    this.dateFormat = format;
  }

  @Override
  public JsonSerializer<Date> createContextualSerializer(SerializerManager manager,
                                                      Property property) {
    if (property.hasAnnotation(JsonDateFormat.class)) {
      return new DateSerializer(
          new SimpleDateFormat(property.getAnnotation(JsonDateFormat.class).format()));
    }
    return this;
  }

  @Override
  public void serialize(Date obj, JsonGenerator gen, SerializerManager manager) {
    if (obj != null) {
      gen.writeValue(dateFormat.format(obj));
    } else {
      gen.writeNull();
    }
  }
}
