package com.drh.json.serializer;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.generator.JsonGenerator;
import com.drh.json.property.Property;
import com.drh.json.serializer.context.ContextualSerializer;
import com.drh.json.serializer.manager.SerializerManager;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Converts a {@link LocalDateTime} to a JSON string with a given format.
 */
//TODO: add ZonedDateTime serializer
public class LocalDateTimeSerializer
    implements JsonSerializer<LocalDateTime>, ContextualSerializer {

  private final DateTimeFormatter defaultFormatter;


  /**
   * Creates a {@link JsonSerializer} to convert a
   * {@link LocalDateTime} to a String with a given format.
   *
   * @param format the format to convert to.
   * @throws IllegalArgumentException if the format is invalid.
   */
  public LocalDateTimeSerializer(DateTimeFormatter format) {
    this.defaultFormatter = format;
  }

  @Override
  public JsonSerializer<LocalDateTime> createContextualSerializer(
      SerializerManager manager, Property property) {

    if (property.hasAnnotation(JsonDateFormat.class)) {
      return new LocalDateTimeSerializer(
          DateTimeFormatter.ofPattern(property.getAnnotation(JsonDateFormat.class).format()));
    }
    return this;
  }

  @Override
  public void serialize(LocalDateTime obj, JsonGenerator gen, SerializerManager manager) {
    if (obj != null) {
      gen.writeValue(defaultFormatter.format(obj));
    } else {
      gen.writeNull();
    }
  }
}
