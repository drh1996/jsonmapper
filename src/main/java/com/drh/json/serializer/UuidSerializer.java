package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.UUID;

/**
 * Converts a {@link UUID} to a JSON string.
 *
 * @see UUID#toString()
 */
public class UuidSerializer implements JsonSerializer<UUID> {


  @Override
  public void serialize(UUID obj, JsonGenerator gen, SerializerManager manager) {
    if (obj != null) {
      gen.writeValue(obj.toString());
    } else {
      gen.writeNull();
    }
  }
}
