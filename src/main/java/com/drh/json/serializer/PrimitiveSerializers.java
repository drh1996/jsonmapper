package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * The standard set of serializers for any primitive and wrapper types.
 * These can be overridden to define your own way to serialize these types.
 * <br/>
 * Types include:
 * <ul>
 *  <li>{@link String}</li>
 *  <li>{@link Character} / {@code char}</li>
 *  <li>{@link Boolean} / {@code boolean}</li>
 *  <li>{@link Byte} / {@code byte}</li>
 *  <li>{@link Short} / {@code short}</li>
 *  <li>{@link Integer} / {@code int}</li>
 *  <li>{@link Long} / {@code long}</li>
 *  <li>{@link Float} / {@code float}</li>
 *  <li>{@link Double} / {@code double}</li>
 * </ul>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PrimitiveSerializers {

  /**
   * The standard set of serializers for any primitive and wrapper types.
   * These can be overridden to define your own way to serialize these types.
   * <br/>
   * Types include:
   * <ul>
   *  <li>{@link String}</li>
   *  <li>{@link Character} / {@code char}</li>
   *  <li>{@link Boolean} / {@code boolean}</li>
   *  <li>{@link Byte} / {@code byte}</li>
   *  <li>{@link Short} / {@code short}</li>
   *  <li>{@link Integer} / {@code int}</li>
   *  <li>{@link Long} / {@code long}</li>
   *  <li>{@link Float} / {@code float}</li>
   *  <li>{@link Double} / {@code double}</li>
   * </ul>
   *
   * @return a map of primitive serializers
   */
  public static Map<Class<?>, JsonSerializer<?>> getPrimitiveSerializers() {
    final StringSerializer stringSerializer = new StringSerializer();
    final CharacterSerializer characterSerializer = new CharacterSerializer();
    final BooleanSerializer booleanSerializer = new BooleanSerializer();
    final ByteSerializer byteSerializer = new ByteSerializer();
    final ShortSerializer shortSerializer = new ShortSerializer();
    final IntegerSerializer integerSerializer = new IntegerSerializer();
    final LongSerializer longSerializer = new LongSerializer();
    final FloatSerializer floatSerializer = new FloatSerializer();
    final DoubleSerializer doubleSerializer = new DoubleSerializer();

    Map<Class<?>, JsonSerializer<?>> primitiveSerializers = new HashMap<>();
    primitiveSerializers.put(String.class, stringSerializer);
    primitiveSerializers.put(char.class, characterSerializer);
    primitiveSerializers.put(Character.class, characterSerializer);

    primitiveSerializers.put(boolean.class, booleanSerializer);
    primitiveSerializers.put(Boolean.class, booleanSerializer);

    primitiveSerializers.put(byte.class, byteSerializer);
    primitiveSerializers.put(Byte.class, byteSerializer);
    primitiveSerializers.put(short.class, shortSerializer);
    primitiveSerializers.put(Short.class, shortSerializer);
    primitiveSerializers.put(int.class, integerSerializer);
    primitiveSerializers.put(Integer.class, integerSerializer);
    primitiveSerializers.put(long.class, longSerializer);
    primitiveSerializers.put(Long.class, longSerializer);

    primitiveSerializers.put(float.class, floatSerializer);
    primitiveSerializers.put(Float.class, floatSerializer);
    primitiveSerializers.put(double.class, doubleSerializer);
    primitiveSerializers.put(Double.class, doubleSerializer);
    return primitiveSerializers;
  }

  /**
   * Default serializer for a {@link String}
   */
  public static class StringSerializer implements JsonSerializer<String> {
    @Override
    public void serialize(String obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Character} / {@code char}
   */
  public static class CharacterSerializer implements JsonSerializer<Character> {
    @Override
    public void serialize(Character obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Boolean} / {@code boolean}
   */
  public static class BooleanSerializer implements JsonSerializer<Boolean> {
    @Override
    public void serialize(Boolean obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Integer} / {@code int}
   */
  public static class IntegerSerializer implements JsonSerializer<Integer> {
    @Override
    public void serialize(Integer obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Short} / {@code short}
   */
  public static class ShortSerializer implements JsonSerializer<Short> {
    @Override
    public void serialize(Short obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj); //TODO: add gen.writeValue(short) ??
      }
    }
  }

  /**
   * Default serializer for a {@link Long} / {@code long}
   */
  public static class LongSerializer implements JsonSerializer<Long> {
    @Override
    public void serialize(Long obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Byte} / {@code byte}
   */
  public static class ByteSerializer implements JsonSerializer<Byte> {
    @Override
    public void serialize(Byte obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Float} / {@code float}
   */
  public static class FloatSerializer implements JsonSerializer<Float> {
    @Override
    public void serialize(Float obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }

  /**
   * Default serializer for a {@link Double} / {@code double}
   */
  public static class DoubleSerializer implements JsonSerializer<Double> {
    @Override
    public void serialize(Double obj, JsonGenerator gen, SerializerManager manager) {
      if (obj == null) {
        gen.writeNull();
      } else {
        gen.writeValue(obj);
      }
    }
  }
}
