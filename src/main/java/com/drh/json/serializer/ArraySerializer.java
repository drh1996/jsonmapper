package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.lang.reflect.Array;

/**
 * A standard array serializer. Serializing of the arrays elements will be delegated to
 * a custom {@link JsonSerializer}, if defined, or a default serializer.
 */
public class ArraySerializer implements JsonSerializer<Object> {

  @Override
  public void serialize(Object obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
      return;
    }

    Object[] value = objectToArray(obj);

    gen.arrayStart();
    if (value.length > 0) {
      //get array type
      Class<?> arrayType = getClassType(value);
      for (Object o : value) {
        serializerManager.serialize(arrayType, o, gen);
      }
    }
    gen.arrayEnd();
  }

  private static Object[] objectToArray(Object arrObj) {
    int length = Array.getLength(arrObj);
    Object[] arr = new Object[length];
    for (int i = 0; i < length; i++) {
      arr[i] = Array.get(arrObj, i);
    }
    return arr;
  }

  private static Class<?> getClassType(Object[] arr) {
    for (Object o : arr) {
      if (o != null) {
        return o.getClass();
      }
    }
    return Object.class;
  }
}
