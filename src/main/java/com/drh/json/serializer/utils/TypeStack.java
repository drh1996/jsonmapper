package com.drh.json.serializer.utils;

import java.util.NoSuchElementException;
import lombok.AllArgsConstructor;

/**
 * An implementation of a stack data structure used to keep track of the depth of a json structure.
 * Depth increases with each nested array or object start, and decreases when the array or object
 * closes.
 */
public class TypeStack {

  /**
   * Indicates root of json structure
   */
  public static final int TYPE_NONE = 0;

  /**
   * Indicates a json object
   */
  public static final int TYPE_OBJ = 1;

  /**
   * Indicates a json array
   */
  public static final int TYPE_ARR = 2;

  private TypeRef head;

  /**
   * An implementation of a stack data structure used to keep track of the depth of a
   * json structure.
   * Depth increases with each nested array or object start, and decreases when the array or object
   * closes.
   */
  public TypeStack() {
    head = new TypeRef(TYPE_NONE, null); //indicates root
  }

  /**
   * Checks if the stack is empty. indicating the json structure is complete
   *
   * @return true if the json structure is complete, false otherwise
   */
  public boolean empty() {
    return head == null;
  }

  /**
   * Peak at the top value in the stack
   *
   * @return the top value in the stack
   */
  public int head() {
    if (empty()) {
      throw new NoSuchElementException("Root of json exceeded, cannot write json outside of root");
    }
    return head.type;
  }

  /**
   * Get and remove the top value in the stack
   *
   * @return the top value in the stack
   */
  public int pop() {
    if (empty()) {
      throw new NoSuchElementException("Root of json exceeded, cannot write json outside of root");
    }

    int poppedVal = head.type;
    head = head.next;
    return poppedVal;
  }

  /**
   * Add a value to the top of the stack
   *
   * @param type the type to add
   */
  public void push(int type) {
    this.head = new TypeRef(type, head);
  }

  @AllArgsConstructor
  private static class TypeRef {
    private int type;
    private TypeRef next;
  }
}
