package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNull;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * A default {@link JsonNull} serializer. Serializing of this object will always result in
 * {@code null}
 */
public class JsonNullSerializer implements JsonSerializer<JsonNull> {

  @Override
  public void serialize(JsonNull obj, JsonGenerator gen, SerializerManager serializerManager) {
    gen.writeNull();
  }
}
