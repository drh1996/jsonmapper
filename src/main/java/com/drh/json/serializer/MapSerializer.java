package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.Map;

/**
 * A standard java map serializer. Serializing of the map values will
 * be delegated to a custom {@link JsonSerializer}, if defined, or a default serializer.
 * The keys for the map will be determined by the {@link Object#toString()} method.
 */
public class MapSerializer implements JsonSerializer<Object> {

  @Override
  public void serialize(Object obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
      return;
    }

    gen.objectStart();
    Map<?, ?> map = (Map<?, ?>) obj;
    // get json string of keys & values
    for (Map.Entry<?, ?> entry : map.entrySet()) {
      gen.writeField(entry.getKey().toString());
      if (entry.getValue() != null) {
        serializerManager.serialize(entry.getValue().getClass(), entry.getValue(), gen);
      } else {
        gen.writeNull();
      }
    }
    gen.objectEnd();
  }
}
