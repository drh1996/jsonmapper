package com.drh.json.serializer;

import com.drh.json.exception.JsonSerializingException;
import com.drh.json.generator.JsonGenerator;
import com.drh.json.property.FieldProperty;
import com.drh.json.property.MethodProperty;
import com.drh.json.property.Property;
import com.drh.json.serializer.manager.SerializerManager;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * The default serializer for any Object without a defined serializer.
 * Object field values are retrieved via getter methods.
 * Serializing of the Object fields will be delegated to
 * a custom {@link JsonSerializer}, if defined, or a default serializer.
 */
public class ObjectSerializer implements JsonSerializer<Object> {
  private final Map<Class<?>, List<MethodProperties>> methodPropertiesMap;

  /**
   * The default serializer for any Object without a defined serializer.
   * Object field values are retrieved via getter methods.
   * Serializing of the Object fields will be delegated to
   * a custom {@link JsonSerializer}, if defined, or a default serializer.
   */
  public ObjectSerializer() {
    this.methodPropertiesMap = new HashMap<>();
  }

  @Override
  public void serialize(Object obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
      return;
    }

    List<MethodProperties> methodProperties = getMethods(obj.getClass());
    gen.objectStart();
    if (methodProperties.isEmpty()) {
      gen.objectEnd();
      return;
    }

    for (MethodProperties methodProperty : methodProperties) {
      Method method = methodProperty.getMethod();
      method.setAccessible(true);

      Object value;
      try {
        //get field value
        value = method.invoke(obj);
      } catch (IllegalAccessException e) {
        throw new JsonSerializingException(
            "Couldn't access value for field: " + methodProperty.fieldName, e);
      } catch (InvocationTargetException e) {
        throw new JsonSerializingException(
            "Exception thrown by method: " + method.getName(), e.getTargetException());
      }

      gen.writeField(methodProperty.getFieldName());
      serializerManager.serialize(
          method.getReturnType(), value, gen, createProperty(methodProperty));
    }
    gen.objectEnd();
  }

  private List<MethodProperties> getMethods(Class<?> clazz) {
    if (this.methodPropertiesMap.containsKey(clazz)) {
      return this.methodPropertiesMap.get(clazz);
    }
    List<MethodProperties> methodPropertiesList = createMethodProperties(clazz);
    this.methodPropertiesMap.put(clazz, methodPropertiesList);
    return methodPropertiesList;
  }

  private static List<MethodProperties> createMethodProperties(Class<?> clazz) {
    Method[] methods = clazz.getMethods();
    ArrayList<MethodProperties> methodPropertiesList = new ArrayList<>(methods.length);
    for (Method method : methods) {
      if (Modifier.isStatic(method.getModifiers())) {
        continue;
      }

      String methodName = method.getName();
      if (methodName.equals("getClass")) {
        continue;
      }

      int trimIndex;
      if (methodName.startsWith("is") && methodName.length() > 2) {
        trimIndex = 2;
      } else if (methodName.startsWith("get") && methodName.length() > 3) {
        trimIndex = 3;
      } else {
        continue;
      }
      String fieldName = Character.toLowerCase(methodName.charAt(trimIndex))
          + methodName.substring(trimIndex + 1);

      methodPropertiesList.add(new MethodProperties(
          method, findField(fieldName, clazz), fieldName));
    }
    methodPropertiesList.trimToSize();
    return methodPropertiesList;
  }

  private static Field findField(String fieldName, Class<?> clazz) {
    try {
      return clazz.getDeclaredField(fieldName);
    } catch (NoSuchFieldException e) {
      return null;
    }
  }

  private static Property createProperty(MethodProperties methodProperty) {
    Field field = methodProperty.getField();
    if (field == null) {
      return new MethodProperty(methodProperty.getMethod());
    }

    return new MethodProperty(methodProperty.getMethod(), new FieldProperty(field));
  }


  @Getter
  @RequiredArgsConstructor
  private static class MethodProperties {
    private final Method method;
    private final Field field;
    private final String fieldName;
  }
}
