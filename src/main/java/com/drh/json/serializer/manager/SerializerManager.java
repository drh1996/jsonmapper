package com.drh.json.serializer.manager;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.generator.Utf8JsonGenerator;
import com.drh.json.node.JsonNode;
import com.drh.json.property.Property;
import com.drh.json.serializer.ArraySerializer;
import com.drh.json.serializer.CollectionSerializer;
import com.drh.json.serializer.EnumSerializer;
import com.drh.json.serializer.JsonSerializer;
import com.drh.json.serializer.MapSerializer;
import com.drh.json.serializer.ObjectSerializer;
import com.drh.json.serializer.context.ContextualSerializer;
import com.drh.json.styling.JsonStyling;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Holds {@link JsonSerializer}s and delegates serialization to the correct serializer
 * depending upon a few factors:
 * <ul>
 *   <li>if a custom serializer has been defined for the object</li>
 *   <li>if all else fails, use a default serializer</li>
 * </ul>
 */
public class SerializerManager {

  private final Map<Class<?>, JsonSerializer> serializerMap;

  private final JsonSerializer<Enum<?>> defaultEnumSerializer;
  private final JsonSerializer<Object> defaultObjectSerializer;
  private final JsonSerializer<Object> defaultCollectionSerializer;
  private final JsonSerializer<Object> defaultArraySerializer;
  private final JsonSerializer<Object> defaultMapSerializer;

  /**
   * Holds {@link JsonSerializer}s and delegates serialization to the correct serializer
   * depending upon a few factors:
   * <ul>
   *   <li>if a custom serializer has been defined for the object</li>
   *   <li>if all else fails, use a default serializer</li>
   * </ul>
   *
   * @param serializerMap a map of serializers to assign to this manager
   */
  public SerializerManager(Map<Class<?>, JsonSerializer> serializerMap) {
    this.serializerMap = serializerMap;

    this.defaultEnumSerializer = new EnumSerializer();
    this.defaultObjectSerializer = new ObjectSerializer();
    this.defaultCollectionSerializer = new CollectionSerializer();
    this.defaultArraySerializer = new ArraySerializer();
    this.defaultMapSerializer = new MapSerializer();
  }

  /**
   * Create a {@link JsonGenerator} dependent on the {@link Charset} provided
   *
   * @param charset the charset
   * @param styling serialization styling
   * @return a generator
   */
  public JsonGenerator createGenerator(Charset charset, JsonStyling styling) {
    Objects.requireNonNull(charset, "charset is null");
    Objects.requireNonNull(styling, "styling is null");

    if (charset.equals(StandardCharsets.UTF_8)) {
      return new Utf8JsonGenerator(styling);
    }

    throw new IllegalArgumentException("Unsupported charset: " + charset.displayName());
  }

  /**
   * Serialize a POJO with a defined serializer. If one cannot be found,
   * a default serializer is used.
   *
   * @param clazz the type of serializer to use
   * @param obj object to serialize
   * @param generator generator to use when serializing
   */
  public void serialize(Class<?> clazz, Object obj, JsonGenerator generator) {
    if (hasSerializer(clazz)) {
      serializerMap.get(clazz).serialize(obj, generator, this);
    } else {
      useDefaultSerializer(clazz, obj, generator);
    }
  }

  /**
   * Serialize a POJO with a defined serializer. If one cannot be found,
   * a default serializer is used. {@link Property} is used for contextual serializing.
   *
   * @param clazz the type of serializer to use
   * @param obj object to serialize
   * @param generator generator to use when serializing
   * @param property context information
   */
  // TODO create wrapper for annotated type & include option for method, class, etc level annots
  public void serialize(Class<?> clazz, Object obj, JsonGenerator generator, Property property) {
    if (hasSerializer(clazz)) {
      JsonSerializer serializer = serializerMap.get(clazz);
      if (serializer instanceof ContextualSerializer) {
        JsonSerializer newSerializer = ((ContextualSerializer) serializer)
            .createContextualSerializer(this, property);
        newSerializer.serialize(obj, generator, this);
      } else {
        serializer.serialize(obj, generator, this);
      }
    } else {
      useDefaultSerializer(clazz, obj, generator);
    }
  }

  /**
   * Check if a given class has a serializer assigned to it
   *
   * @param clazz the class to check
   * @return true if a serializer exists for the class, otherwise false
   */
  public boolean hasSerializer(Class<?> clazz) {
    return serializerMap.containsKey(clazz);
  }

  /**
   * Add a custom serializer to be used for a given class.
   *
   * @param clazz the class to use the serializer with
   * @param serializer the serializer to use
   * @param <T> the type being serialized
   */
  public <T> void addSerializer(Class<T> clazz, JsonSerializer<T> serializer) {
    serializerMap.put(clazz, serializer);
  }

  private void useDefaultSerializer(Class<?> clazz, Object obj, JsonGenerator generator) {
    if (clazz.isEnum()) {
      defaultEnumSerializer.serialize((Enum<?>) obj, generator, this);
    } else if (Collection.class.isAssignableFrom(clazz)) {
      defaultCollectionSerializer.serialize(obj, generator, this);
    } else if (clazz.isArray()) {
      defaultArraySerializer.serialize(obj, generator, this);
    } else if (Map.class.isAssignableFrom(clazz)) {
      defaultMapSerializer.serialize(obj, generator, this);
    } else {
      defaultObjectSerializer.serialize(obj, generator, this);
    }
  }
}
