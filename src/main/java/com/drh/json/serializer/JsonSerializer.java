package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonNode;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * Defines a function callback for serializing an object to a JSON string.
 * This can be used to customize how a JSON string is created from a given object.
 *
 * @param <T> the object type.
 */
public interface JsonSerializer<T> {

  /**
   * Create a JSON string from a given object.
   *
   * @param obj               the object to be serialized
   * @param gen               a generator for creating JSON.
   * @param serializerManager to pass on the serializing of an object to another serializer.
   * @see JsonGenerator
   * @see SerializerManager
   */
  void serialize(T obj, JsonGenerator gen, SerializerManager serializerManager);
}
