package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonObject;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * A default {@link JsonObject} serializer. Serializing of the objects fields will be delegated to
 * a custom {@link JsonSerializer}, if defined, or a default serializer.
 */
public class JsonObjectSerializer implements JsonSerializer<JsonObject> {

  @Override
  public void serialize(JsonObject obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj == null) {
      gen.writeNull();
      return;
    }

    gen.objectStart();
    obj.forEach((key, value) -> {
      gen.writeField(key);
      serializerManager.serialize(value.getClass(), value, gen);
    });
    gen.objectEnd();
  }
}
