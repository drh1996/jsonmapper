package com.drh.json.serializer;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonPrimitive;
import com.drh.json.serializer.manager.SerializerManager;

/**
 * A default {@link JsonPrimitive} serializer. Serializing of this object will always result in a
 * primitive or {@link String} value
 */
public class JsonPrimitiveSerializer implements JsonSerializer<JsonPrimitive> {

  @Override
  public void serialize(JsonPrimitive obj, JsonGenerator gen, SerializerManager serializerManager) {
    if (obj.isString()) {
      gen.writeValue(obj.getAsString());
    } else if (obj.isBool()) {
      gen.writeValue(obj.getAsBool());
    } else if (obj.isInt()) {
      gen.writeValue(obj.getAsInt());
    } else if (obj.isDouble()) {
      gen.writeValue(obj.getAsDouble());
    } else if (obj.isFloat()) {
      gen.writeValue(obj.getAsFloat());
    } else if (obj.isLong()) {
      gen.writeValue(obj.getAsLong());
    } else if (obj.isShort()) {
      gen.writeValue(obj.getAsShort());
    } else if (obj.isByte()) {
      gen.writeValue(obj.getAsByte());
    } else {
      gen.writeNull(); //TODO: should throw??
    }
  }
}
