package com.drh.json.parser;

import com.drh.json.exception.JsonParsingException;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonNull;
import com.drh.json.node.JsonObject;
import com.drh.json.node.JsonPrimitive;
import com.drh.json.node.JsonType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * JsonParser provides the ability to convert a JSON {@link String} to a {@link JsonNode}.
 *
 * @since 1.0
 * @see JsonNode
 * @author Dan Hayes
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JsonParser {

  private static final String ERR_START = "Json does not start with object or array reference.";
  private static final String ERR_INVALID_JSON_AT_INDEX =  "Invalid json '%c' at index: %d";
  private static final String ERR_INVALID_KEY_VAL_SEPARATOR =
      "Invalid key value separator '%c' at index: %d";
  private static final String ERR_MISSING_OBJ_END = "Missing end of json '}': %s";
  private static final String ERR_KEY_START =
      "Invalid json start of key '%c' at index: %d, json: %s";
  private static final String ERR_STRUCTURE = "Invalid Json Structure at: %s";
  private static final String ERR_SPECIAL_CHAR =
      "Invalid special character '\\%c' found in string: %s";
  private static final String ERR_UNICODE_TOO_SHORT =
      "Failed to parse '\\u' special character in the String: \"%s\", "
      + "Reason: '\\u' required exactly 4 hexadecimal characters to be parsed.";
  private static final String ERR_UNICODE_HEX =
      "Failed to parse '\\u' special character in the String: \"%s\", "
      + "Reason: The 4 characters following '\\u' must be hexadecimal.";

  private static final Pattern NUMBER_PATTERN = Pattern.compile(
      "^([+-]?\\d+(\\.\\d+)?([eE][+-]?\\d+)?)");

  /**
   * Parse a JSON string to a {@link JsonNode}.
   *
   * @param src the JSON string to be parsed
   * @return an object representation of the JSON string
   */
  public static JsonNode parse(String src) {
    String json = removeWhitespaces(src);

    JsonType type = getJsonType(json);
    if (type == JsonType.OBJECT) {
      return parseObject(json);
    } else if (type == JsonType.ARRAY) {
      return parseArray(json);
    } else {
      throw new JsonParsingException(ERR_START);
    }
  }

  private static JsonObject parseObject(final String json) {
    JsonObject object = new JsonObject();

    int currentIndex = 1;
    while (currentIndex < json.length() - 1) {
      JsonKeyReference jsonKeyRef = nextKeyRef(currentIndex, json);
      currentIndex = jsonKeyRef.endIndex + 1; //+2 for end quote & ':'

      //check key value separator is ':'
      char afterKey = json.charAt(currentIndex);
      if (afterKey != ':') {
        throw new JsonParsingException(
            String.format(ERR_INVALID_KEY_VAL_SEPARATOR, afterKey, currentIndex));
      }
      currentIndex++;

      JsonValueReference jsonValRef = nextValRef(currentIndex, json);

      if (jsonValRef.endIndex == -1) {
        throw new JsonParsingException(
            String.format(ERR_INVALID_JSON_AT_INDEX, json.charAt(currentIndex), currentIndex));
      }

      //evaluate node
      String key = evaluateString(json.substring(jsonKeyRef.startIndex, jsonKeyRef.endIndex));
      String value = json.substring(jsonValRef.startIndex, jsonValRef.endIndex + 1);
      JsonNode toAdd = evaluateNode(value, jsonValRef.type);
      object.add(key, toAdd);

      currentIndex = jsonValRef.endIndex + 1;

      if (currentIndex >= json.length()) {
        throw new JsonParsingException(String.format(ERR_MISSING_OBJ_END, json));
      }

      //check has value separator or end of node after value
      char afterVal = json.charAt(currentIndex);
      if (afterVal != ',' && afterVal != '}') {
        throw new JsonParsingException(
            String.format(ERR_INVALID_JSON_AT_INDEX, afterVal, currentIndex));
      }

      currentIndex++;
    }

    return object;
  }

  private static JsonArray parseArray(final String json) {
    JsonArray array = new JsonArray();
    int currentIndex = 1;
    while (currentIndex < json.length() - 1) {
      JsonValueReference jsonValRef = nextValRef(currentIndex, json);

      if (jsonValRef.endIndex == -1) {
        throw new JsonParsingException(
            String.format(ERR_INVALID_JSON_AT_INDEX, json.charAt(currentIndex), currentIndex));
      }

      String value = json.substring(jsonValRef.startIndex, jsonValRef.endIndex + 1);
      JsonNode toAdd = evaluateNode(value, jsonValRef.type);
      array.add(toAdd);

      currentIndex = jsonValRef.endIndex + 1;

      //check has value separator or end of node after value
      char afterVal = json.charAt(currentIndex);
      if (afterVal != ',' && afterVal != ']') {
        throw new JsonParsingException(
            String.format(ERR_INVALID_JSON_AT_INDEX, afterVal, currentIndex));
      }

      currentIndex++;

    }
    return array;
  }

  private static String removeWhitespaces(String json) {
    boolean inQuote = false;
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < json.length(); i++) {
      char c = json.charAt(i);
      boolean isWhitespace = Character.isWhitespace(c);

      if (inQuote
          && c == '\\'
          && i + 1 < json.length()) { //add escaped chars

        sb.append(c);
        sb.append(json.charAt(i + 1));
        i++;
        continue;
      }

      if (c == '"') {
        inQuote = !inQuote;
      } else if (isWhitespace && !inQuote) {
        continue;
      }

      sb.append(c);
    }
    return sb.toString();
  }

  private static JsonType getJsonType(String json) {
    return switch (json.charAt(0)) {
      case '{' -> JsonType.OBJECT;
      case '[' -> JsonType.ARRAY;
      default -> null;
    };
  }

  private static JsonKeyReference nextKeyRef(int index, final String json) {
    if (json.charAt(index) != '"') {
      throw new JsonParsingException(
          String.format(ERR_KEY_START, json.charAt(index), index, json));
    }
    int end = -1;
    for (int i = index + 1; i < json.length(); i++) {
      char c = json.charAt(i);

      if (c == '\\'
          && i + 1 < json.length()) { //skip escaped chars
        i++;
        continue;
      }

      if (c == '"') {
        end = i;
        break;
      }
    }
    return new JsonKeyReference(index + 1, end);
  }

  private static JsonValueReference nextValRef(int index, final String json) {
    JsonType jsonType = null;
    int end = -1;
    char c = json.charAt(index);
    if (c == '{' || c == '[') {
      jsonType = (c == '{' ? JsonType.OBJECT : JsonType.ARRAY);
      end = getTokenEndIndex(index, json, c, (char) (c + 2));
    } else if (c == '"') {
      jsonType = JsonType.STRING;
      for (int i = index + 1; i < json.length(); i++) {
        char current = json.charAt(i);

        if (current == '\\'
            && i + 1 < json.length()) { //skip escaped chars
          i++;
          continue;
        }
        if (current == '"') {
          end = i;
          break;
        }
      }
    } else {
      String jsonToAnalyse = json.substring(index);
      if (jsonToAnalyse.startsWith("null")) {
        jsonType = JsonType.NULL;
        end = index + 3;
      } else if (jsonToAnalyse.startsWith("true")) {
        jsonType = JsonType.BOOLEAN;
        end = index + 3;
      } else if (jsonToAnalyse.startsWith("false")) {
        jsonType = JsonType.BOOLEAN;
        end = index + 4;
      } else { //check if number
        Matcher numberMatcher = NUMBER_PATTERN.matcher(jsonToAnalyse);
        if (numberMatcher.find()) {
          int numberLength = numberMatcher.group().length();
          jsonType = JsonType.NUMBER;
          end = index + numberLength - 1;
        }
      }
    }
    return new JsonValueReference(index, end, jsonType);
  }

  private static int getTokenEndIndex(
      int startIndex, String json, char increaseDepthChar, char decreaseDepthChar) {

    int depth = 0;
    boolean inQuote = false;
    for (int i = startIndex; i < json.length(); i++) {
      char c = json.charAt(i);

      if (c == '\\'
          && i + 1 < json.length()) { //skip escaped chars
        i++;
        continue;
      }

      if (c == '"') {
        inQuote = !inQuote;
      }

      if (inQuote) {
        continue;
      }

      if (c == increaseDepthChar) {
        depth++;
      } else if (c == decreaseDepthChar) {
        depth--;
        if (depth == 0) {
          return i;
        }
      }
    }
    throw new JsonParsingException(String.format(ERR_STRUCTURE, json));
  }

  private static JsonNode evaluateNode(String value, JsonType type) {
    return switch (type) {
      case STRING -> {
        String evaluatedString = evaluateString(value.substring(1, value.length() - 1));
        yield new JsonPrimitive(evaluatedString);
      }
      case NUMBER -> new JsonPrimitive(evaluateNumber(value));
      case BOOLEAN -> new JsonPrimitive(Boolean.parseBoolean(value));
      case OBJECT -> parseObject(value);
      case ARRAY -> parseArray(value);
      case NULL -> new JsonNull();
    };
  }

  private static Number evaluateNumber(final String value) {
    if (value.contains(".")) {
      return Double.parseDouble(value);
    } else {
      try {
        return Integer.parseInt(value);
      } catch (NumberFormatException e) {
        return Long.parseLong(value);
      }
    }
  }

  private static String evaluateString(final String value) {
    StringBuilder sb = new StringBuilder(value.length());
    char[] chars = value.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      if (chars[i] != '\\') {
        sb.append(chars[i]);
        continue;
      }

      int evaluatedChars = 1;
      char special = chars[i + 1];
      switch (special) {
        case '\\':
        case '"':
          sb.append(special);
          break;
        case 'n':
          sb.append('\n');
          break;
        case 'r':
          sb.append('\r');
          break;
        case 't':
          sb.append('\t');
          break;
        case 'u':
          sb.append(getUnicodeCharacter(value, i + 2));
          evaluatedChars = 5;
          break;
        default:
          throw new JsonParsingException(String.format(ERR_SPECIAL_CHAR, special, value));
      }
      i += evaluatedChars; //skip next characters (already evaluated)
    }
    return sb.toString();
  }

  private static char getUnicodeCharacter(final String value, int startIndex) {
    //check has 4 more characters
    if (startIndex + 4 > value.length()) {
      throw new JsonParsingException(String.format(ERR_UNICODE_TOO_SHORT, value));
    }

    //check those 4 characters are hexadecimal
    String str = value.substring(startIndex, startIndex + 4);
    for (int i = 0; i < str.length(); i++) {
      char c = str.charAt(i);
      if ((c < '0' || c > '9') && (c < 'a' || c > 'f') && (c < 'A' || c > 'F')) {
        throw new JsonParsingException(String.format(ERR_UNICODE_HEX, value));
      }
    }

    //parse the 4 hex characters to a char
    return (char) Integer.parseInt(str, 16);
  }

  @AllArgsConstructor
  private static class JsonKeyReference {
    int startIndex;
    int endIndex;
  }

  private static class JsonValueReference extends JsonKeyReference {
    JsonType type;

    public JsonValueReference(int startIndex, int endIndex, JsonType type) {
      super(startIndex, endIndex);
      this.type = type;
    }
  }
}
