package com.drh.json;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.JsonSerializer;
import com.drh.json.serializer.manager.SerializerManager;
import com.drh.json.styling.JsonStyling;
import java.nio.charset.StandardCharsets;

class InternalJsonSerializer {

  private final SerializerManager serializerManager;
  private final JsonStyling styling;

  InternalJsonSerializer(JsonStyling styling, SerializerManager manager) {
    this.serializerManager = manager;
    this.styling = styling;
  }

  String serialize(Object value) {
    return serialize(value, styling);
  }

  String serialize(Object value, JsonStyling styling) {
    JsonGenerator generator = serializerManager.createGenerator(StandardCharsets.UTF_8, styling);
    serializerManager.serialize(value.getClass(), value, generator);
    return generator.getOutput();
  }

  <T> void addSerializer(Class<T> clazz, JsonSerializer<T> serializer) {
    serializerManager.addSerializer(clazz, serializer);
  }
}
