package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.exception.JsonTransformException;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonObject;
import com.drh.json.property.FieldProperty;
import com.drh.json.property.Property;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import lombok.RequiredArgsConstructor;

/**
 * The default deserializer for any Object without a defined deserializer.
 * Deserializing of the Object fields will be delegated to
 * a custom {@link JsonDeserializer}, if defined, or a default deserializer.
 */
@RequiredArgsConstructor
public class ObjectDeserializer implements JsonDeserializer<Object>, ContextualDeserializer {

  private final Class<?> clazz;

  /**
   * The default deserializer for any Object without a defined deserializer.
   * Deserializing of the Object fields will be delegated to
   * a custom {@link JsonDeserializer}, if defined, or a default deserializer.
   */
  public ObjectDeserializer() {
    this.clazz = Object.class;
  }

  @Override
  public JsonDeserializer<?> createContextualDeserializer(DeserializerManager manager,
                                                          Property property) {
    Class<?> ctxClass = property.getClassType();
    if (clazz.equals(ctxClass)) {
      return this;
    }
    return new ObjectDeserializer(property.getClassType());
  }

  @Override
  public Object deserialize(JsonNode rootNode, DeserializerManager manager) {
    JsonObject jsonObj = rootNode.getAsObject();
    Object obj = createNewInstance(clazz);
    Field[] fields = clazz.getDeclaredFields();
    //TODO change to loop through jsonObj to allow for fail on unknown property setting
    for (Field field : fields) {
      String fieldName = field.getName();
      if (jsonObj.has(fieldName)) { //TODO: check annotations for other names for field
        JsonNode node = jsonObj.get(fieldName);
        try {
          field.setAccessible(true);
          field.set(obj, manager.deserialize(field.getType(), node, new FieldProperty(field)));
          field.setAccessible(false);
        } catch (IllegalAccessException e) {
          throw new JsonDeserializingException("Unable to access field: \""
              + field.getName() + "\" for class: " + clazz.getName(), e);
        } catch (JsonTransformException e) {
          throw new JsonDeserializingException("Failed to Deserialize to field '"
              + field.getName() + "', reason: " + e.getMessage(), e);
        }
      }
    }
    return obj;
  }

  private static Object createNewInstance(Class<?> clazz) {
    try {
      Constructor<?> constructor = clazz.getConstructor();
      constructor.setAccessible(true);
      Object o = constructor.newInstance();
      constructor.setAccessible(false);

      return o;
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new JsonDeserializingException(
          "Failed to constructor object for class: " + clazz.getName(), e);
    } catch (NoSuchMethodException e) {
      throw new JsonDeserializingException(
          "No default constructor is present in class: " + clazz.getName(), e);
    }
  }
}
