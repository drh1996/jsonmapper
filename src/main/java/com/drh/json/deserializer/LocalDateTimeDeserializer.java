package com.drh.json.deserializer;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNode;
import com.drh.json.property.Property;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.RequiredArgsConstructor;

/**
 * Converts a JSON string to a {@link LocalDateTime} with a given format.
 * Default format is yyyy-MM-dd'T'HH:mm:ss.SSS
 */
@RequiredArgsConstructor
public class LocalDateTimeDeserializer
    implements JsonDeserializer<LocalDateTime>, ContextualDeserializer {

  private final DateTimeFormatter format;

  @Override
  public JsonDeserializer<?> createContextualDeserializer(DeserializerManager manager,
                                                          Property property) {
    if (property.hasAnnotation(JsonDateFormat.class)) {
      return new LocalDateTimeDeserializer(
          DateTimeFormatter.ofPattern(property.getAnnotation(JsonDateFormat.class).format()));
    }
    return this;
  }

  @Override
  public LocalDateTime deserialize(JsonNode node, DeserializerManager manager) {
    if (node.isNull()) {
      return null;
    } else {
      return LocalDateTime.parse(node.getAsString(), format);
    }
  }
}
