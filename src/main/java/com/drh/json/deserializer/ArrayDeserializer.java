package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.property.Property;
import java.lang.reflect.Array;
import lombok.RequiredArgsConstructor;

/**
 * A standard array deserializer. Deserializing of the arrays elements will be delegated to
 * a custom {@link JsonDeserializer}, if defined, or a default deserializer.
 */
@RequiredArgsConstructor
public class ArrayDeserializer implements JsonDeserializer<Object>, ContextualDeserializer {

  private final Class<?> clazz;

  /**
   * A standard array deserializer. Deserializing of the arrays elements will be delegated to
   * a custom {@link JsonDeserializer}, if defined, or a default deserializer.
   */
  public ArrayDeserializer() {
    this.clazz = Object[].class;
  }

  @Override
  public JsonDeserializer<?> createContextualDeserializer(
      DeserializerManager manager, Property property) {

    Class<?> ctxClass = property.getClassType();
    if (clazz.equals(ctxClass)) {
      return this;
    }
    return new ArrayDeserializer(ctxClass);
  }

  @Override
  public Object deserialize(JsonNode node, DeserializerManager manager) {
    Class<?> componentType = clazz.getComponentType();
    JsonArray array = node.getAsArray();
    Object arrObj = Array.newInstance(componentType, array.size());
    for (int i = 0; i < array.size(); i++) {
      Array.set(arrObj, i, manager.deserialize(componentType, array.get(i)));
    }
    return arrObj;
  }
}
