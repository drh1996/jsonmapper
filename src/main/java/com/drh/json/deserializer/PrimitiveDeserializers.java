package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNode;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * The standard set of deserializers for any primitive and wrapper types.
 * These can be overridden to define your own way to deserialize these types.
 * <br/>
 * Types include:
 * <ul>
 *  <li>{@link String}</li>
 *  <li>{@link Character} / {@code char}</li>
 *  <li>{@link Boolean} / {@code boolean}</li>
 *  <li>{@link Byte} / {@code byte}</li>
 *  <li>{@link Short} / {@code short}</li>
 *  <li>{@link Integer} / {@code int}</li>
 *  <li>{@link Long} / {@code long}</li>
 *  <li>{@link Float} / {@code float}</li>
 *  <li>{@link Double} / {@code double}</li>
 * </ul>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PrimitiveDeserializers {

  /**
   * The standard set of deserializers for any primitive and wrapper types.
   * These can be overridden to define your own way to deserialize these types.
   * <br/>
   * Types include:
   * <ul>
   *  <li>{@link String}</li>
   *  <li>{@link Character} / {@code char}</li>
   *  <li>{@link Boolean} / {@code boolean}</li>
   *  <li>{@link Byte} / {@code byte}</li>
   *  <li>{@link Short} / {@code short}</li>
   *  <li>{@link Integer} / {@code int}</li>
   *  <li>{@link Long} / {@code long}</li>
   *  <li>{@link Float} / {@code float}</li>
   *  <li>{@link Double} / {@code double}</li>
   * </ul>
   *
   * @return a map of primitive deserializers
   */
  public static Map<Class<?>, JsonDeserializer<?>> getPrimitiveDeserializers() {
    final StringDeserializer stringDeserializer = new StringDeserializer();
    final CharacterDeserializer characterDeserializer = new CharacterDeserializer();
    final BooleanDeserializer booleanDeserializer = new BooleanDeserializer();
    final ByteDeserializer byteDeserializer = new ByteDeserializer();
    final ShortDeserializer shortDeserializer = new ShortDeserializer();
    final IntegerDeserializer integerDeserializer = new IntegerDeserializer();
    final LongDeserializer longDeserializer = new LongDeserializer();
    final FloatDeserializer floatDeserializer = new FloatDeserializer();
    final DoubleDeserializer doubleDeserializer = new DoubleDeserializer();

    Map<Class<?>, JsonDeserializer<?>> primitiveDeserializers = new HashMap<>();
    primitiveDeserializers.put(String.class, stringDeserializer);
    primitiveDeserializers.put(char.class, characterDeserializer);
    primitiveDeserializers.put(Character.class, characterDeserializer);

    primitiveDeserializers.put(boolean.class, booleanDeserializer);
    primitiveDeserializers.put(Boolean.class, booleanDeserializer);

    primitiveDeserializers.put(byte.class, byteDeserializer);
    primitiveDeserializers.put(Byte.class, byteDeserializer);
    primitiveDeserializers.put(short.class, shortDeserializer);
    primitiveDeserializers.put(Short.class, shortDeserializer);
    primitiveDeserializers.put(int.class, integerDeserializer);
    primitiveDeserializers.put(Integer.class, integerDeserializer);
    primitiveDeserializers.put(long.class, longDeserializer);
    primitiveDeserializers.put(Long.class, longDeserializer);

    primitiveDeserializers.put(float.class, floatDeserializer);
    primitiveDeserializers.put(Float.class, floatDeserializer);
    primitiveDeserializers.put(double.class, doubleDeserializer);
    primitiveDeserializers.put(Double.class, doubleDeserializer);

    return primitiveDeserializers;
  }

  /**
   * Default deserializer for a {@link String}
   */
  public static class StringDeserializer implements JsonDeserializer<String> {
    @Override
    public String deserialize(JsonNode node, DeserializerManager manager) {
      return node.isNull() ? null : node.getAsString();
    }
  }

  /**
   * Default deserializer for a {@link Character} / {@code char}
   */
  public static class CharacterDeserializer implements JsonDeserializer<Character> {
    @Override
    public Character deserialize(JsonNode node, DeserializerManager manager) {
      return node.isNull() ? null : node.getAsChar();
    }
  }

  /**
   * Default deserializer for a {@link Boolean} / {@code boolean}
   */
  public static class BooleanDeserializer implements JsonDeserializer<Boolean> {
    @Override
    public Boolean deserialize(JsonNode node, DeserializerManager manager) {
      return node.isNull() ? null : node.getAsBool();
    }
  }

  /**
   * Default deserializer for a {@link Integer} / {@code int}
   */
  public static class IntegerDeserializer implements JsonDeserializer<Integer> {

    @Override
    public Integer deserialize(JsonNode node, DeserializerManager manager) {
      if (node.isNull()) {
        return null;
      }

      if (node.isString()) {
        return Integer.parseInt(node.getAsString());
      }

      return node.getAsInt();
    }
  }

  /**
   * Default deserializer for a {@link Short} / {@code short}
   */
  public static class ShortDeserializer implements JsonDeserializer<Short> {

    @Override
    public Short deserialize(JsonNode node, DeserializerManager manager) {
      if (node.isNull()) {
        return null;
      }

      if (node.isString()) {
        return Short.parseShort(node.getAsString());
      }

      return node.getAsShort();
    }
  }

  /**
   * Default deserializer for a {@link Long} / {@code long}
   */
  public static class LongDeserializer implements JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonNode node, DeserializerManager manager) {
      if (node.isNull()) {
        return null;
      }

      if (node.isString()) {
        return Long.parseLong(node.getAsString());
      }

      return node.getAsLong();
    }
  }

  /**
   * Default deserializer for a {@link Byte} / {@code byte}
   */
  public static class ByteDeserializer implements JsonDeserializer<Byte> {

    @Override
    public Byte deserialize(JsonNode node, DeserializerManager manager) {
      if (node.isNull()) {
        return null;
      }

      if (node.isString()) {
        return Byte.parseByte(node.getAsString());
      }

      return node.getAsByte();
    }
  }

  /**
   * Default deserializer for a {@link Float} / {@code float}
   */
  public static class FloatDeserializer implements JsonDeserializer<Float> {

    @Override
    public Float deserialize(JsonNode node, DeserializerManager manager) {
      if (node.isNull()) {
        return null;
      }

      if (node.isString()) {
        return Float.parseFloat(node.getAsString());
      }

      return node.getAsFloat();
    }
  }

  /**
   * Default deserializer for a {@link Double} / {@code double}
   */
  public static class DoubleDeserializer implements JsonDeserializer<Double> {

    @Override
    public Double deserialize(JsonNode node, DeserializerManager manager) {
      if (node.isNull()) {
        return null;
      }

      if (node.isString()) {
        return Double.parseDouble(node.getAsString());
      }

      return node.getAsDouble();
    }
  }
}
