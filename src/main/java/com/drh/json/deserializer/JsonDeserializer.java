package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNode;

/**
 * Defines a function callback for deserializing a JSON string to an object.
 * This can be used to customize how an object is created from a given JSON string.
 * A {@link JsonNode} is used to represent the JSON string being deserialized.
 *
 * @param <T> the object type.
 */
public interface JsonDeserializer<T> {

  /**
   * Create an object from a given JSON string.
   *
   * @param node a node representing a JSON string.
   * @param manager a holder for all other deserializers, can be used to delegate
   *               further deserialization
   * @return an object created from a JSON string.
   * @see JsonNode
   */
  T deserialize(JsonNode node, DeserializerManager manager);
}
