package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonNode;
import java.util.UUID;

/**
 * Converts a JSON string to a {@link UUID}.
 *
 * @see UUID#fromString(String)
 */
public class UuidDeserializer implements JsonDeserializer<UUID> {

  @Override
  public UUID deserialize(JsonNode node, DeserializerManager manager) {
    if (node.isNull()) {
      return null;
    } else {
      try {
        return UUID.fromString(node.getAsString());
      } catch (Exception e) {
        throw new JsonDeserializingException("failed to deserialize to a java.util.UUID", e);
      }
    }
  }
}
