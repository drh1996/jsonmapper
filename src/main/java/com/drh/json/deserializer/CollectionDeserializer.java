package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.Property;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import lombok.RequiredArgsConstructor;

/**
 * A standard java collection deserializer. Deserializing of the collections elements will
 * be delegated to a custom {@link JsonDeserializer}, if defined, or a default deserializer.
 */
@RequiredArgsConstructor
public class CollectionDeserializer
    implements JsonDeserializer<Collection<?>>, ContextualDeserializer {

  private final Class clazz;
  private final Type[] parameterizedTypes;
  private final String name;

  /**
   * A standard java collection deserializer. Deserializing of the collections elements will
   * be delegated to a custom {@link JsonDeserializer}, if defined, or a default deserializer.
   */
  public CollectionDeserializer() {
    this.clazz = Collection.class;
    this.parameterizedTypes = new Type[0];
    this.name = null;
  }

  @Override
  public JsonDeserializer<?> createContextualDeserializer(
      DeserializerManager manager, Property property) {

    return new CollectionDeserializer(
        property.getClassType(),
        property.getParameterizedTypes(),
        property.getName());
  }

  @Override
  public Collection<?> deserialize(JsonNode node, DeserializerManager manager) {
    Class<?> parameterizedType = (Class<?>) getParameterizedTypes()[0];
    ClassProperty property = new ClassProperty(parameterizedType);

    JsonArray array = node.getAsArray();
    Collection collectionObj = createNewCollectionInstance(clazz);

    for (int i = 0; i < array.size(); i++) {
      collectionObj.add(manager.deserialize(parameterizedType, array.get(i), property));
    }
    return collectionObj;
  }

  private Type[] getParameterizedTypes() {
    if (parameterizedTypes.length != 0) {
      return parameterizedTypes;
    } else {
      throw new JsonDeserializingException("Could not obtain parameterized types for: '"
          + createFieldString(clazz, name) + "'");
    }
  }

  private static String createFieldString(Class<?> clazz, String name) {
    return clazz.getTypeName() + ' ' + name;
  }

  private static Collection createNewCollectionInstance(Class<?> clazz) {
    try {
      if (Modifier.isAbstract(clazz.getModifiers())) {
        //TODO: add more collection types https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html
        if (Set.class.isAssignableFrom(clazz)) {
          return new HashSet();
        }
        return new ArrayList(); //use arraylist by default
      }
      return (Collection) clazz.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new JsonDeserializingException(
          "Failed to constructor object for class: " + clazz.getName(), e);
    } catch (NoSuchMethodException e) {
      throw new JsonDeserializingException(
          "No default constructor is present in class: " + clazz.getName(), e);
    }
  }
}
