package com.drh.json.deserializer;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonNode;
import com.drh.json.property.Property;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.RequiredArgsConstructor;

/**
 * Converts a JSON string to a {@link Date} with a given format.
 * Default format is yyyy-MM-dd'T'HH:mm:ss.SSS
 */
@RequiredArgsConstructor
public class DateDeserializer
    implements JsonDeserializer<Date>, ContextualDeserializer {

  private final SimpleDateFormat format;

  @Override
  public JsonDeserializer<?> createContextualDeserializer(DeserializerManager manager,
                                                          Property property) {
    if (property.hasAnnotation(JsonDateFormat.class)) {
      return new DateDeserializer(
          new SimpleDateFormat(property.getAnnotation(JsonDateFormat.class).format()));
    }
    return this;
  }

  @Override
  public Date deserialize(JsonNode node, DeserializerManager manager) {
    if (node.isNull()) {
      return null;
    } else {
      try {
        return format.parse(node.getAsString());
      } catch (Exception e) {
        throw new JsonDeserializingException("failed to deserialize to a java.util.Date", e);
      }
    }
  }
}
