package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNode;
import com.drh.json.property.Property;
import lombok.RequiredArgsConstructor;

/**
 * A standard java enum deserializer. Deserializing of the enum is dictated by
 * {@link Enum#valueOf(Class, String)}
 */
@RequiredArgsConstructor
public class EnumDeserializer implements JsonDeserializer<Enum<?>>, ContextualDeserializer {

  private final Class<Enum> clazz;

  /**
   * A standard java enum deserializer. Deserializing of the enum is dictated by
   * {@link Enum#valueOf(Class, String)}
   */
  public EnumDeserializer() {
    this.clazz = Enum.class;
  }

  @Override
  public JsonDeserializer<?> createContextualDeserializer(DeserializerManager manager,
                                                          Property property) {
    Class<?> ctxClass = property.getClassType();
    if (clazz.equals(ctxClass)) {
      return this;
    }
    if (!ctxClass.isEnum()) {
      throw new IllegalArgumentException("class " + clazz + " is not an enum");
    }

    return new EnumDeserializer((Class<Enum>) ctxClass);

  }

  @Override
  public Enum<?> deserialize(JsonNode node, DeserializerManager manager) {
    return Enum.valueOf(clazz, node.getAsString());
  }
}
