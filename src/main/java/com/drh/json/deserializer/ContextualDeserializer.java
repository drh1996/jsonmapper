package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.property.Property;

/**
 * An interface that will be called for a {@link JsonDeserializer} whenever context (e.g. state)
 * needs to be used in deserialization.
 * Example see: {@link ArrayDeserializer}
 */
public interface ContextualDeserializer {

  /**
   * An interface that will be called for a {@link JsonDeserializer} whenever context (e.g. state)
   * needs to be used in deserialization.
   * Example see: {@link ArrayDeserializer}
   *
   * @param manager to pass on the deserializing of an object to another deserializer
   * @param property properties of the class / field / method / parameterized type
   *                 being deserialized
   * @return a deserializer with a context (e.g. state)
   * @see DeserializerManager
   * @see Property
   */
  JsonDeserializer<?> createContextualDeserializer(DeserializerManager manager, Property property);
}
