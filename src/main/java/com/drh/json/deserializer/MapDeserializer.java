package com.drh.json.deserializer;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonObject;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.Property;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;

/**
 * A standard java map deserializer. Deserializing of the map values will
 * be delegated to a custom {@link JsonDeserializer}, if defined, or a default deserializer.
 * The keys for the map must be a {@link String}.
 */
@RequiredArgsConstructor
public class MapDeserializer implements JsonDeserializer<Map<?, ?>>, ContextualDeserializer {
  private final Class clazz;
  private final Type[] parameterizedTypes;
  private final String name;

  /**
   * A standard java map deserializer. Deserializing of the map values will
   * be delegated to a custom {@link JsonDeserializer}, if defined, or a default deserializer.
   * The keys for the map must be a {@link String}.
   */
  public MapDeserializer() {
    this.clazz = Map.class;
    this.parameterizedTypes = new Type[0];
    this.name = null;
  }

  @Override
  public JsonDeserializer<?> createContextualDeserializer(DeserializerManager manager,
                                                          Property property) {
    return new MapDeserializer(
        property.getClassType(),
        property.getParameterizedTypes(),
        property.getName());
  }

  @Override
  public Map<?, ?> deserialize(JsonNode node, DeserializerManager manager) {
    Type[] types = getParameterizedTypes(clazz, name);
    Class<?> keyClass = (Class<?>) types[0];

    if (!keyClass.equals(String.class)) {
      throw new JsonDeserializingException("Map key type must be String for field: "
          + createFieldString(clazz, name));
    }

    Class<?> valClass = (Class<?>) types[1];
    ClassProperty property = new ClassProperty(valClass);

    JsonObject object = node.getAsObject();
    Map mapObj = createNewMapInstance(clazz);
    for (Map.Entry<String, JsonNode> entry : object.entrySet()) {
      mapObj.put(entry.getKey(), manager.deserialize(valClass, entry.getValue(), property));
    }
    return mapObj;
  }

  private Type[] getParameterizedTypes(Class<?> clazz, String name) {
    if (parameterizedTypes.length != 0) {
      return parameterizedTypes;
    } else {
      throw new JsonDeserializingException("Could not obtain parameterized types for: '"
          + createFieldString(clazz, name) + "'");
    }
  }

  private static String createFieldString(Class<?> clazz, String name) {
    return clazz.getTypeName() + ' ' + name;
  }

  private static Map createNewMapInstance(Class<?> clazz) {
    try {
      if (Modifier.isAbstract(clazz.getModifiers())) {
        //TODO: add more defaults
        return new HashMap();
      }
      return (Map) clazz.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new JsonDeserializingException(
          "Failed to constructor object for class: " + clazz.getName(), e);
    } catch (NoSuchMethodException e) {
      throw new JsonDeserializingException(
          "No default constructor is present in class: " + clazz.getName(), e);
    }
  }
}
