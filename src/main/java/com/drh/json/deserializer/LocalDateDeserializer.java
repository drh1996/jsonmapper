package com.drh.json.deserializer;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNode;
import com.drh.json.property.Property;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import lombok.RequiredArgsConstructor;

/**
 * Converts a JSON string to a {@link LocalDate} with a given format.
 * Default format is yyyy-MM-dd
 */
@RequiredArgsConstructor
public class LocalDateDeserializer implements JsonDeserializer<LocalDate>, ContextualDeserializer {

  private final DateTimeFormatter format;

  @Override
  public JsonDeserializer<?> createContextualDeserializer(
      DeserializerManager manager, Property property) {
    if (property != null && property.hasAnnotation(JsonDateFormat.class)) {
      return new LocalDateDeserializer(
          DateTimeFormatter.ofPattern(property.getAnnotation(JsonDateFormat.class).format()));
    }
    return this;
  }

  @Override
  public LocalDate deserialize(JsonNode node, DeserializerManager manager) {
    if (node.isNull()) {
      return null;
    } else {
      return LocalDate.parse(node.getAsString(), format);
    }
  }
}
