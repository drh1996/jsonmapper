package com.drh.json.deserializer.manager;

import com.drh.json.annotation.JsonDeserialize;
import com.drh.json.deserializer.ArrayDeserializer;
import com.drh.json.deserializer.CollectionDeserializer;
import com.drh.json.deserializer.ContextualDeserializer;
import com.drh.json.deserializer.EnumDeserializer;
import com.drh.json.deserializer.JsonDeserializer;
import com.drh.json.deserializer.MapDeserializer;
import com.drh.json.deserializer.ObjectDeserializer;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonNode;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.Property;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Holds {@link JsonDeserializer}s and delegates deserialization to the correct deserializer
 * depending upon a few factors:
 * <ul>
 *   <li>if the object being deserialized to has a {@link JsonDeserialize} annotation present</li>
 *   <li>if a custom deserializer has been defined for the object</li>
 *   <li>if all else fails, use a default deserializer</li>
 * </ul>
 */
public class DeserializerManager {

  private final Map<Class<?>, JsonDeserializer> deserializerMap;
  private final Map<Class<?>, JsonDeserializer> annotatedDeserializerMap;

  private final JsonDeserializer<Object> defaultArrayDeserializer;
  private final JsonDeserializer<Enum<?>> defaultEnumDeserializer;
  private final JsonDeserializer<Collection<?>> defaultCollectionDeserializer;
  private final JsonDeserializer<Map<?, ?>> defaultMapDeserializer;
  private final JsonDeserializer<Object> defaultObjectDeserializer;

  /**
   * Holds {@link JsonDeserializer}s and delegates deserialization to the correct deserializer
   * depending upon a few factors:
   * <ul>
   *   <li>if the object being deserialized to has a {@link JsonDeserialize} annotation present</li>
   *   <li>if a custom deserializer has been defined for the object</li>
   *   <li>if all else fails, use a default deserializer</li>
   * </ul>
   *
   * @param deserializerMap a map of deserializers to assign to this manager
   */
  public DeserializerManager(Map<Class<?>, JsonDeserializer> deserializerMap) {
    this.deserializerMap = deserializerMap;
    this.annotatedDeserializerMap = new HashMap<>();

    this.defaultArrayDeserializer = new ArrayDeserializer();
    this.defaultEnumDeserializer = new EnumDeserializer();
    this.defaultCollectionDeserializer = new CollectionDeserializer();
    this.defaultMapDeserializer = new MapDeserializer();
    this.defaultObjectDeserializer = new ObjectDeserializer();
  }

  /**
   * Add a custom deserializer to be used for a given class.
   *
   * @param clazz the class to use the deserializer with
   * @param deserializer the deserializer to use
   * @param <T> the type being deserialized to
   */
  public <T> void addDeserializer(Class<T> clazz, JsonDeserializer<T> deserializer) {
    deserializerMap.put(clazz, deserializer);
  }

  /**
   * Deserialize a {@link JsonNode} with a defined deserializer. If one cannot be found,
   * a default deserializer is used.
   *
   * @param clazz the type of deserializer to use
   * @param node the object being deserialized
   * @param <T> the type being deserialized
   * @return the deserialized object
   */
  public <T> T deserialize(Class<T> clazz, JsonNode node) {
    return deserialize(clazz, node, null);
  }

  /**
   * Deserialize a {@link JsonNode} with a defined deserializer. If one cannot be found,
   * a default deserializer is used. {@link Property} is used for contextual deserializing.
   *
   * @param clazz the type of deserializer to use
   * @param node the object being deserialized
   * @param property context information
   * @param <T> the type being deserialized
   * @return the deserialized object
   */
  public <T> T deserialize(Class<T> clazz, JsonNode node, Property property) {
    if (property != null && property.hasAnnotation(JsonDeserialize.class)) {
      return clazz.cast(deserializeFromAnnotatedDeserializer(
          property.getAnnotation(JsonDeserialize.class), node, property));
    }

    JsonDeserialize clsAnn = clazz.getAnnotation(JsonDeserialize.class);
    if (clsAnn != null) {
      return clazz.cast(deserializeFromAnnotatedDeserializer(clsAnn, node, property));
    }

    if (deserializerMap.containsKey(clazz)) {
      JsonDeserializer<T> deserializer = deserializerMap.get(clazz);
      if (deserializer instanceof ContextualDeserializer ctxDeserializer) {
        return clazz.cast(ctxDeserializer
            .createContextualDeserializer(this, property)
            .deserialize(node, this));
      }
      return clazz.cast(deserializer.deserialize(node, this));
    }

    if (node.isNull()) {
      return null;
    }

    return clazz.cast(useDefaultDeserializer(clazz, node, property));
  }

  private Object useDefaultDeserializer(Class<?> clazz, JsonNode node, Property property) {
    if (property == null) {
      property = new ClassProperty(clazz);
    }
    ContextualDeserializer context;
    if (clazz.isArray()) {
      context = (ContextualDeserializer) defaultArrayDeserializer;
    } else if (clazz.isEnum()) {
      context = (ContextualDeserializer) defaultEnumDeserializer;
    } else if (Collection.class.isAssignableFrom(clazz)) {
      context = (ContextualDeserializer) defaultCollectionDeserializer;
    } else if (Map.class.isAssignableFrom(clazz)) {
      context = (ContextualDeserializer) defaultMapDeserializer;
    } else {
      context = (ContextualDeserializer) defaultObjectDeserializer;
    }
    return context.createContextualDeserializer(this, property)
        .deserialize(node, this);
  }

  private Object deserializeFromAnnotatedDeserializer(
      JsonDeserialize ann, JsonNode node, Property property) {

    JsonDeserializer<?> deserializer = getAnnotatedDeserializer(ann);
    if (property != null && deserializer instanceof ContextualDeserializer ctxDeserializer) {
      return ctxDeserializer
          .createContextualDeserializer(this, property)
          .deserialize(node, this);
    }
    return deserializer.deserialize(node, this);
  }

  private JsonDeserializer<?> getAnnotatedDeserializer(JsonDeserialize ann) {
    Class<? extends JsonDeserializer<?>> cls = ann.value();
    if (annotatedDeserializerMap.containsKey(cls)) {
      return annotatedDeserializerMap.get(cls);
    } else {
      JsonDeserializer<?> deserializer = createAnnotatedDeserializer(
          (Class<JsonDeserializer<?>>) cls);
      annotatedDeserializerMap.put(cls, deserializer);
      return deserializer;
    }
  }

  private JsonDeserializer<?> createAnnotatedDeserializer(Class<JsonDeserializer<?>> cls) {
    try {
      Constructor<JsonDeserializer<?>> constructor = cls.getConstructor();
      constructor.setAccessible(true);
      JsonDeserializer<?> deserializer = constructor.newInstance();
      constructor.setAccessible(false);

      return deserializer;
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException
             | NoSuchMethodException e) {
      throw new JsonDeserializingException(
          "Failed to deserialize using annotated deserializer: " + cls.getName(), e);
    }
  }
}
