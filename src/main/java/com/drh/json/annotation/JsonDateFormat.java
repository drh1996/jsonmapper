package com.drh.json.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defined at the field or method level to specify the shape a {@link java.util.Date Date},
 * {@link java.time.LocalDate LocalDate}, or {@link java.time.LocalDateTime LocalDateTime}
 * should take when serializing an object.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface JsonDateFormat {

  /**
   * Returns the format that should be used when serializing a given Date object
   *
   * @return the format
   */
  String format();
}
