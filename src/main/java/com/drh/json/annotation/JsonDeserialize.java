package com.drh.json.annotation;

import com.drh.json.deserializer.JsonDeserializer;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defined at the field or method level to specify which {@link JsonDeserializer} to use
 * when deserializing.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface JsonDeserialize {

  /**
   * Returns the {@link JsonDeserializer} to use when deserializing.
   *
   * @return the deserializer
   */
  Class<? extends JsonDeserializer<?>> value();
}
