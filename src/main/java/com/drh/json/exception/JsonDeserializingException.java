package com.drh.json.exception;

/**
 * An error thrown when deserialization fails for any reason
 */
public class JsonDeserializingException extends RuntimeException {

  /**
   * An error thrown when deserialization fails for any reason
   *
   * @param message error message
   */
  public JsonDeserializingException(String message) {
    super(message);
  }

  /**
   * An error thrown when deserialization fails for any reason
   *
   * @param message error message
   * @param cause exception causing this error
   */
  public JsonDeserializingException(String message, Throwable cause) {
    super(message, cause);
  }
}
