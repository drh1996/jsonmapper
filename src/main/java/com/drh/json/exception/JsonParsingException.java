package com.drh.json.exception;

/**
 * An error thrown when parsing json fails for any reason
 */
public class JsonParsingException extends RuntimeException {

  /**
   * An error thrown when parsing json fails for any reason
   *
   * @param message error message
   */
  public JsonParsingException(String message) {
    super(message);
  }
}
