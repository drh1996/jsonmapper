package com.drh.json.exception;

/**
 * An error thrown when generating json fails for any reason
 */
public class JsonGeneratorException extends RuntimeException {

  /**
   * An error thrown when generating json fails for any reason
   *
   * @param message error message
   */
  public JsonGeneratorException(String message) {
    super(message);
  }
}
