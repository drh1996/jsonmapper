package com.drh.json.exception;

/**
 * An error thrown when a {@link com.drh.json.node.JsonNode JsonNode} fails to be converted to
 * any type
 */
public class JsonTransformException extends RuntimeException {

  /**
   * An error thrown when a {@link com.drh.json.node.JsonNode JsonNode} fails to be converted to
   * any type
   *
   * @param message error message
   */
  public JsonTransformException(String message) {
    super(message);
  }
}
