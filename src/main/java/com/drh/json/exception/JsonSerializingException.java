package com.drh.json.exception;

/**
 * An error thrown when serialization fails for any reason
 */
public class JsonSerializingException extends RuntimeException {

  /**
   * An error thrown when serialization fails for any reason
   *
   * @param message error message
   */
  public JsonSerializingException(String message) {
    super(message);
  }

  /**
   * An error thrown when serialization fails for any reason
   *
   * @param message error message
   * @param cause exception causing this error
   */
  public JsonSerializingException(String message, Throwable cause) {
    super(message, cause);
  }
}
