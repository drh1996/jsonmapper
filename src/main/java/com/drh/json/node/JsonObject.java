package com.drh.json.node;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * A POJO representation of a map of String keys and JSON values.
 * Values can be a {@link JsonObject}, {@link JsonArray}, or a {@link JsonPrimitive}.
 * They can be added, removed, and retrieved similarly to how the standard
 * {@link Map} interface works.
 *
 * @since 1.0
 * @see JsonNode
 * @author Dan Hayes
 */
public class JsonObject extends JsonNode implements Iterable<Map.Entry<String, JsonNode>> {

  private final Map<String, JsonNode> nodeMap;

  /**
   * Creates an empty {@link JsonObject}.
   */
  public JsonObject() {
    this.nodeMap = new LinkedHashMap<>();
  }

  /**
   * Returns true if the provided key is present within this {@link JsonObject}.
   *
   * @param key the key to check if it has a value
   * @return true if the provided key is present within this {@link JsonObject}, otherwise false
   */
  public boolean has(String key) {
    return nodeMap.containsKey(key);
  }

  /**
   * Returns the {@link JsonNode} that is associated to the key provided if it is present.
   * Example usage without try catch:
   * <pre>
   * JsonObject object = ...
   * if (object.has("example")) {
   *   JsonNode exampleNode = object.get("example");
   * }
   * </pre>
   *
   * @param key the key used to retrieve a {@link JsonNode}
   * @return the {@link JsonNode} that is associated to the key provided if it is present
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @see JsonObject#has(String)
   */
  public JsonNode get(String key) {
    if (has(key)) {
      return nodeMap.get(key);
    }
    throw new NoSuchElementException("No JsonNode found for key: '" + key + "'");
  }

  @Override
  public JsonObject getAsObject() {
    return this;
  }

  @Override
  public boolean isObject() {
    return true;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@link JsonObject}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@link JsonObject}, otherwise false.
   */
  public boolean isObject(String key) {
    return get(key).isObject();
  }

  /**
   * Returns the {@link JsonObject} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsObject()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link JsonObject} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsObject() JsonNode#getAsObject()
   */
  public JsonObject getObject(String key) {
    return get(key).getAsObject();
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@link JsonArray}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@link JsonArray}, otherwise false.
   */
  public boolean isArray(String key) {
    return get(key).isArray();
  }

  /**
   * Returns the {@link JsonArray} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsArray()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link JsonArray} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsArray() JsonNode#getAsArray()
   */
  public JsonArray getArray(String key) {
    return get(key).getAsArray();
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@link JsonPrimitive}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive}, otherwise false.
   */
  public boolean isPrimitive(String key) {
    return get(key).isPrimitive();
  }

  /**
   * Returns the {@link JsonPrimitive} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsPrimitive()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link JsonPrimitive} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsPrimitive() JsonNode#getAsPrimitive()
   */
  public JsonPrimitive getPrimitive(String key) {
    return get(key).getAsPrimitive();
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@link String}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@link String}, otherwise false.
   */
  public boolean isString(String key) {
    return get(key).isString();
  }

  /**
   * Returns the {@link String} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsString()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link String} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsString() JsonNode#getAsString()
   */
  public String getString(String key) {
    return get(key).getAsString();
  }

  /**
   * Returns the {@link String} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link String} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsString() JsonNode#getAsString()
   */
  public String getString(String key, String defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsString();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code char}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a  {@code char}, otherwise false.
   */
  public boolean isChar(String key) {
    return get(key).isChar();
  }

  /**
   * Returns the {@code char} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsChar()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code char} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsChar() JsonNode#getAsChar()
   */
  public char getChar(String key) {
    return get(key).getAsChar();
  }

  /**
   * Returns the {@code char} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code char} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsChar() JsonNode#getAsChar()
   */
  public char getChar(String key, char defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsChar();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@link Number}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a  {@link Number}, otherwise false.
   */
  public boolean isNumber(String key) {
    return get(key).isNumber();
  }

  /**
   * Returns the {@link Number} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsPrimitive().getAsNumber()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link Number} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsNumber() JsonNode#getAsNumber()
   */
  public Number getNumber(String key) {
    return get(key).getAsPrimitive().getAsNumber();
  }

  /**
   * Returns the {@link Number} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@link Number} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsNumber() JsonNode#getAsNumber()
   */
  public Number getNumber(String key, Number defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsNumber();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code int}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a  {@code int}, otherwise false.
   */
  public boolean isInt(String key) {
    return get(key).isInt();
  }

  /**
   * Returns the {@code int} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsInt()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code int} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsInt() JsonNode#getAsInt()
   */
  public int getInt(String key) {
    return get(key).getAsInt();
  }

  /**
   * Returns the {@code int} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code int} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsInt() JsonNode#getAsInt()
   */
  public int getInt(String key, int defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsInt();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code long}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a  {@code long}, otherwise false.
   */
  public boolean isLong(String key) {
    return get(key).isLong();
  }

  /**
   * Returns the {@code long} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsLong()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code long} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsLong() JsonNode#getAsLong()
   */
  public long getLong(String key) {
    return get(key).getAsLong();
  }

  /**
   * Returns the {@code long} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code long} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsLong() JsonNode#getAsLong()
   */
  public long getLong(String key, long defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsLong();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code short}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a  {@code short}, otherwise false.
   */
  public boolean isShort(String key) {
    return get(key).isShort();
  }

  /**
   * Returns the {@code short} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsShort()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code short} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsShort() JsonNode#getAsShort()
   */
  public short getShort(String key) {
    return get(key).getAsShort();
  }

  /**
   * Returns the {@code short} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code short} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsShort() JsonNode#getAsShort()
   */
  public short getShort(String key, short defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsShort();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code byte}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a  {@code byte}, otherwise false.
   */
  public boolean isByte(String key) {
    return get(key).isByte();
  }

  /**
   * Returns the {@code byte} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsByte()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code byte} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsByte() JsonNode#getAsByte()
   */
  public byte getByte(String key) {
    return get(key).getAsByte();
  }

  /**
   * Returns the {@code byte} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code byte} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsByte() JsonNode#getAsByte()
   */
  public byte getByte(String key, byte defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsByte();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code double}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@code double}, otherwise false.
   */
  public boolean isDouble(String key) {
    return get(key).isDouble();
  }

  /**
   * Returns the {@code double} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsDouble()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code double} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsDouble() JsonNode#getAsDouble()
   */
  public double getDouble(String key) {
    return get(key).getAsDouble();
  }

  /**
   * Returns the {@code double} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code double} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsDouble() JsonNode#getAsDouble()
   */
  public double getDouble(String key, double defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsDouble();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code float}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@code float}, otherwise false.
   */
  public boolean isFloat(String key) {
    return get(key).isFloat();
  }

  /**
   * Returns the {@code float} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsFloat()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code float} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsFloat() JsonNode#getAsFloat()
   */
  public float getFloat(String key) {
    return get(key).getAsFloat();
  }

  /**
   * Returns the {@code float} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code float} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsFloat() JsonNode#getAsFloat()
   */
  public float getFloat(String key, float defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsFloat();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is assignable to a {@code boolean}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is assignable to a {@code boolean}, otherwise false.
   */
  public boolean isBool(String key) {
    return get(key).isBool();
  }

  /**
   * Returns the {@code boolean} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsBool()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code boolean} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsBool() JsonNode#getAsBool()
   */
  public boolean getBool(String key) {
    return get(key).getAsBool();
  }

  /**
   * Returns the {@code boolean} associated with a given key in this {@link JsonObject}.
   * If the value isnt present, return {@code defaultValue}
   *
   * @param key key of the value to return
   * @param defaultValue the default value
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is not assignable to a {@code boolean} type
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsBool() JsonNode#getAsBool()
   */
  public boolean getBool(String key, boolean defaultValue) {
    if (has(key)) {
      return nodeMap.get(key).getAsBool();
    }
    return defaultValue;
  }

  /**
   * Returns true if a {@link JsonNode} associated with a given key in this {@link JsonObject}
   * is {@code null}.
   *
   * @param key key of the value to check
   * @return true if the JsonNode is null, otherwise false.
   */
  public boolean isNull(String key) {
    return get(key).isNull();
  }

  /**
   * Returns a {@code null} associated with a given key in this {@link JsonObject}.
   * This is a shorthand for
   * <pre>
   * get(key).getAsNull()
   * </pre>
   *
   * @param key key of the value to return
   * @return the element associated with the provided key in this {@link JsonObject}
   * @throws NoSuchElementException if the key provided doesn't have an associated {@link JsonNode}
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} associated with
   *     a given key in this {@link JsonObject} is {@code null}
   * @see JsonObject#get(String) JsonObject#get(String)
   * @see JsonNode#getAsNull() JsonNode#getAsNull()
   */
  public JsonNull getNull(String key) {
    return get(key).getAsNull();
  }

  /**
   * Add a key value pair to this {@link JsonObject}.
   *
   * @param key the key for the value
   * @param value the value
   * @return this {@link JsonObject}
   */
  public JsonObject add(String key, JsonNode value) {
    Objects.requireNonNull(value, "value is null");
    if (!has(key)) {
      nodeMap.put(key, value);
    } else {
      throw new IllegalArgumentException("The key '" + key + "' already has a value");
    }
    return this;
  }

  /**
   * Add a {@link String} key value pair to this {@link JsonObject}.
   *
   * @param key the key for the value
   * @param value the value
   * @return this {@link JsonObject}
   */
  public JsonObject add(String key, String value) {
    return addPrimitive(key, value);
  }

  /**
   * Add a {@link Number} key value pair to this {@link JsonObject}.
   *
   * @param key the key for the value
   * @param value the value
   * @return this {@link JsonObject}
   */
  public JsonObject add(String key, Number value) {
    return addPrimitive(key, value);
  }

  /**
   * Add a boolean key value pair to this {@link JsonObject}.
   *
   * @param key the key for the value
   * @param value the value
   * @return this {@link JsonObject}
   */
  public JsonObject add(String key, boolean value) {
    return addPrimitive(key, value);
  }

  private JsonObject addPrimitive(String key, Object value) {
    if (!has(key)) {
      if (value == null) {
        nodeMap.put(key, new JsonNull());
      } else {
        nodeMap.put(key, new JsonPrimitive(value));
      }
    } else {
      throw new IllegalArgumentException("The key '" + key + "' already has a value");
    }
    return this;
  }

  /**
   * Perform a given action with each key / value pair in this {@link JsonObject}.
   *
   * @param action the action to be executed for each key / value pair in this {@link JsonObject}
   */
  public void forEach(BiConsumer<String, JsonNode> action) {
    Objects.requireNonNull(action);
    for (Map.Entry<String, JsonNode> entry : this) {
      action.accept(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Returns an iterator over the key / value pairs in this {@link JsonObject} in
   * any particular order.
   *
   * @return an iterator over the key / value pairs in this {@link JsonObject} in
   *     any particular order
   */
  @Override
  public Iterator<Map.Entry<String, JsonNode>> iterator() {
    return entrySet().iterator();
  }

  /**
   * Returns a set of entries representing the key / value pairs in this {@link JsonObject}.
   *
   * @return a set of entries representing the key / value pairs in this {@link JsonObject}
   */
  public Set<Map.Entry<String, JsonNode>> entrySet() {
    return nodeMap.entrySet();
  }

  /**
   * Compares the object provided for equality with all the key / value pairs in this
   * {@link JsonObject} using their {@code equals(obj)} method.
   * Each element must be in the same order and contain the same values.
   *
   * @param obj the object to be compared for equality with this {@link JsonObject}
   * @return true if this {@link JsonObject} is equal to the provided object, otherwise false.
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof JsonObject)) {
      return false;
    }

    JsonObject other = (JsonObject) obj;

    return this.nodeMap.equals(other.nodeMap);
  }

  /**
   * Returns a {@link String} representation of this {@link JsonObject}. For example:
   * <pre>
   * JsonObject obj = new JsonObject()
   *    .add("one", 1)
   *    .add("two", "2")
   *    .add("three", false);
   *
   * obj.toString() == {"one":1,"two":"2","three":false}
   * </pre>
   *
   * @return a {@link String} representation of this {@link JsonObject}.
   */
  @Override
  public String toString() {
    String properties = this.nodeMap.entrySet().stream()
        .map(entry -> '"' + entry.getKey() + "\":" + entry.getValue())
        .reduce((prev, next) -> prev + ',' + next)
        .orElse("");
    return '{' + properties + '}';
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(nodeMap);
  }
}
