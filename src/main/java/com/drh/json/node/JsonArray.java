package com.drh.json.node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * A POJO representation of a list of JSON elements. Elements can be a {@link JsonObject},
 * {@link JsonArray}, or a {@link JsonPrimitive}. They can be added, removed, and retrieved
 * similarly to how the standard {@link List} interface works.
 *
 * @since 1.0
 * @see JsonNode
 * @author Dan Hayes
 */
public class JsonArray extends JsonNode implements Iterable<JsonNode> {

  private final List<JsonNode> nodes;

  /**
   * Creates a new empty {@link JsonArray}.
   */
  public JsonArray() {
    this.nodes = new ArrayList<>();
  }

  /**
   * Returns the {@link JsonNode} at the specified index in this {@link JsonArray}.

   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonArray}
   * @throws IndexOutOfBoundsException if the index is out of range
   */
  public JsonNode get(int index) {
    return nodes.get(index);
  }

  @Override
  public JsonArray getAsArray() {
    return this;
  }

  @Override
  public boolean isArray() {
    return true;
  }

  /**
   * Returns true if a {@link JsonNode} at a given index in this {@link JsonArray} is assignable
   * to a {@link JsonArray}.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonArray}, otherwise false.
   */
  public boolean isArray(int index) {
    return get(index).isArray();
  }

  /**
   * Returns the {@link JsonArray} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsArray()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonArray}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@link JsonArray} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsArray() JsonNode#getAsArray()
   */
  public JsonArray getArray(int index) {
    return get(index).getAsArray();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonObject}.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonObject}, otherwise false.
   */
  public boolean isObject(int index) {
    return get(index).isObject();
  }

  /**
   * Returns the {@link JsonObject} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsObject()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonObject}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@link JsonObject} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsObject() JsonNode#getAsObject()
   */
  public JsonObject getObject(int index) {
    return get(index).getAsObject();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive}.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive}, otherwise false.
   */
  public boolean isPrimitive(int index) {
    return get(index).isPrimitive();
  }

  /**
   * Returns the {@link JsonPrimitive} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsPrimitive()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@link JsonPrimitive} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsPrimitive() JsonNode#getAsPrimitive()
   */
  public JsonPrimitive getPrimitive(int index) {
    return get(index).getAsPrimitive();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@link String} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@link String} type, otherwise false.
   */
  public boolean isString(int index) {
    return get(index).isString();
  }

  /**
   * Returns the {@link String} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsString()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@link String} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsString() JsonNode#getAsString()
   */
  public String getString(int index) {
    return get(index).getAsString();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code char} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code char} type, otherwise false.
   */
  public boolean isChar(int index) {
    return get(index).isChar();
  }

  /**
   * Returns the {@code char} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsChar()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code char} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsChar() JsonNode#getAsChar()
   */
  public char getChar(int index) {
    return get(index).getAsChar();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@link Number} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@link Number} type, otherwise false.
   */
  public boolean isNumber(int index) {
    return get(index).isNumber();
  }

  /**
   * Returns the {@link Number} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsNumber()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@link Number} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsNumber() JsonNode#getAsNumber()
   */
  public Number getNumber(int index) {
    return get(index).getAsPrimitive().getAsNumber();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code int} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code int} type, otherwise false.
   */
  public boolean isInt(int index) {
    return get(index).isInt();
  }

  /**
   * Returns the {@code int} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsInt()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code int} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsInt() JsonNode#getAsInt()
   */
  public int getInt(int index) {
    return get(index).getAsInt();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code long} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code long} type, otherwise false.
   */
  public boolean isLong(int index) {
    return get(index).isLong();
  }

  /**
   * Returns the {@code long} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsLong()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code long} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsLong() JsonNode#getAsLong()
   */
  public long getLong(int index) {
    return get(index).getAsLong();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code short} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code short} type, otherwise false.
   */
  public boolean isShort(int index) {
    return get(index).isShort();
  }

  /**
   * Returns the {@code short} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsShort()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code short} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsShort() JsonNode#getAsShort()
   */
  public short getShort(int index) {
    return get(index).getAsShort();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code byte} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code byte} type, otherwise false.
   */
  public boolean isByte(int index) {
    return get(index).isByte();
  }

  /**
   * Returns the {@code byte} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsByte()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code byte} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsByte() JsonNode#getAsByte()
   */
  public byte getByte(int index) {
    return get(index).getAsByte();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code double} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code double} type, otherwise false.
   */
  public boolean isDouble(int index) {
    return get(index).isDouble();
  }

  /**
   * Returns the {@code double} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsDouble()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code double} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsDouble() JsonNode#getAsDouble()
   */
  public double getDouble(int index) {
    return get(index).getAsDouble();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code float} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code float} type, otherwise false.
   */
  public boolean isFloat(int index) {
    return get(index).isFloat();
  }

  /**
   * Returns the {@code float} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsFloat()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code float} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsFloat() JsonNode#getAsFloat()
   */
  public float getFloat(int index) {
    return get(index).getAsFloat();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonPrimitive} and is of the {@code boolean} type.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@code boolean} type, otherwise false.
   */
  public boolean isBool(int index) {
    return get(index).isBool();
  }

  /**
   * Returns the {@code boolean} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsBool()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@code boolean} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsBool() JsonNode#getAsBool()
   */
  public boolean getBool(int index) {
    return get(index).getAsBool();
  }

  /**
   * Returns true if the {@link JsonNode} at the specified index in this {@link JsonArray}
   * is assignable to a {@link JsonNull}.
   *
   * @param index index of the element to check
   * @return true if the JsonNode is assignable to a {@link JsonPrimitive} and is of the
   *     {@link JsonNull} type, otherwise false.
   */
  public boolean isNull(int index) {
    return get(index).isNull();
  }

  /**
   * Returns the {@link JsonNull} at the specified index in this {@link JsonArray}.
   * This is a shorthand for
   * <pre>
   * get(index).getAsNull()
   * </pre>
   *
   * @param index index of the element to return
   * @return the element at the specified position in this {@link JsonPrimitive}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws com.drh.json.exception.JsonTransformException if the {@link JsonNode} at the specified
   *     index in this {@link JsonArray} is not assignable to a {@link JsonNull} type
   * @see JsonArray#get(int) JsonArray#get(int index)
   * @see JsonNode#getAsNull() JsonNode#getAsNull()
   */
  public JsonNull getNull(int index) {
    return get(index).getAsNull();
  }

  /**
   * Adds a {@link JsonNode} to the end of this {@link JsonArray}.
   *
   * @param element the {@link JsonNode} to add
   * @return the reference to this {@link JsonArray}
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(JsonNode element) {
    Objects.requireNonNull(element, "element is null");
    this.nodes.add(element);
    return this;
  }

  /**
   * Adds a {@link String} to the end of this {@link JsonArray}. The {@link String} is converted
   * to a {@link JsonPrimitive} before being added to the array.
   *
   * @param element the {@link String} to add
   * @return the reference to this {@link JsonArray}
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(String element) {
    return addInternal(element);
  }

  /**
   * Adds a {@link Number} to the end of this {@link JsonArray}. The {@link Number} is converted
   * to a {@link JsonPrimitive} before being added to the array.
   *
   * @param element the {@link Number} to add
   * @return the reference to this {@link JsonArray}
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(Number element) {
    return addInternal(element);
  }

  /**
   * Adds a {@code boolean} to the end of this {@link JsonArray}. The {@code boolean} is converted
   * to a {@link JsonPrimitive} before being added to the array.
   *
   * @param element the {@link Number} to add
   * @return the reference to this {@link JsonArray}
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(boolean element) {
    return addInternal(element);
  }

  /**
   * Insert a {@link JsonNode} into the specified index of this {@link JsonArray}.
   * Any elements at or after this index will be shifted by 1 to the right.
   *
   * @param index the index to insert into
   * @param element the {@link JsonNode} to add
   * @return the reference to this {@link JsonArray}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(int index, JsonNode element) {
    Objects.requireNonNull(element, "element is null");
    this.nodes.add(index, element);
    return this;
  }

  /**
   * Insert a {@link String} into the specified index of this {@link JsonArray}.
   * The {@link String} is converted to a {@link JsonPrimitive} before being added to the array.
   * Any elements at or after this index will be shifted by 1 to the right.
   *
   * @param index the index to insert into
   * @param element the {@link String} to add
   * @return the reference to this {@link JsonArray}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(int index, String element) {
    return addInternal(index, element);
  }

  /**
   * Insert a {@link Number} into the specified index of this {@link JsonArray}.
   * The {@link Number} is converted to a {@link JsonPrimitive} before being added to the array.
   * Any elements at or after this index will be shifted by 1 to the right.
   *
   * @param index the index to insert into
   * @param element the {@link Number} to add
   * @return the reference to this {@link JsonArray}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(int index, Number element) {
    return addInternal(index, element);
  }

  /**
   * Insert a {@code boolean} into the specified index of this {@link JsonArray}.
   * The {@code boolean} is converted to a {@link JsonPrimitive} before being added to the array.
   * Any elements at or after this index will be shifted by 1 to the right.
   *
   * @param index the index to insert into
   * @param element the {@code boolean} to add
   * @return the reference to this {@link JsonArray}
   * @throws IndexOutOfBoundsException if the index is out of range
   * @throws NullPointerException if the {@code element} parameter is null
   */
  public JsonArray add(int index, boolean element) {
    return addInternal(index, element);
  }

  /**
   * Returns the current number of {@link JsonNode}s in this {@link JsonArray}.
   *
   * @return the number of elements in the array
   */
  public int size() {
    return nodes.size();
  }

  /**
   * Returns true if there are no {@link JsonNode}s in this {@link JsonArray}.
   *
   * @return true if there are no {@link JsonNode}s in this {@link JsonArray}, otherwise false.
   */
  public boolean isEmpty() {
    return nodes.isEmpty();
  }

  private JsonArray addInternal(int index, Object element) {
    if (element == null) {
      this.nodes.add(index, new JsonNull());
    } else {
      this.nodes.add(index, new JsonPrimitive(element));
    }
    return this;
  }

  private JsonArray addInternal(Object element) {
    if (element == null) {
      this.nodes.add(new JsonNull());
    } else {
      this.nodes.add(new JsonPrimitive(element));
    }
    return this;
  }

  /**
   * Returns an iterator over the {@link JsonNode}s in this {@link JsonArray} in sequential order.
   *
   * @return an iterator over the {@link JsonNode}s in this {@link JsonArray} in sequential order.
   */
  @Override
  public Iterator<JsonNode> iterator() {
    return this.nodes.iterator();
  }

  /**
   * Compares the object provided for equality with all of the {@link JsonNode}s in this
   * {@link JsonArray} using their {@code equals(obj)} method.
   * Each element must be in the same order and contain the same values.
   *
   * @param obj the object to be compared for equality with this {@link JsonArray}
   * @return true if this {@link JsonArray} is equal to the provided object, otherwise false.
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof JsonArray other)) {
      return false;
    }

    return this.nodes.equals(other.nodes);
  }

  /**
   * Returns a {@link String} representation of this {@link JsonArray}. For example:
   * <pre>
   * JsonArray array = new JsonArray()
   *    .add(1)
   *    .add("two")
   *    .add(false);
   *
   * array.toString() == [1,"two",false]
   * </pre>
   *
   * @return a {@link String} representation of this {@link JsonArray}.
   */
  @Override
  public String toString() {
    String properties = this.nodes.stream()
        .map(JsonNode::toString)
        .reduce((prev, next) -> prev + ',' + next)
        .orElse("");
    return '[' + properties + ']';
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(nodes);
  }
}
