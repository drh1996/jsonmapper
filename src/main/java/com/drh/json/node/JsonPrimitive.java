package com.drh.json.node;

import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.exception.JsonTransformException;
import java.util.Objects;

/**
 * A primitive and wrapper representation of a JSON value.
 * Values can be a {@code String}, {@code Character}, {@code Number}, or {@code boolean}
 *
 * @since 1.0
 * @see JsonNode
 * @author Dan Hayes
 */
public class JsonPrimitive extends JsonNode {

  private JsonType type;
  private Object object;

  /**
   * A primitive and wrapper representation of a JSON value.
   * Values can be a {@code String}, {@code Character}, {@code Number}, or {@code boolean}
   *
   * @param value the value this {@link JsonPrimitive} represents
   * @since 1.0
   * @see JsonNode
   * @author Dan Hayes
   */
  public JsonPrimitive(Object value) throws IllegalArgumentException {
    this.type = evaluateType(value);
    this.object = value;
  }

  private static JsonType evaluateType(Object value) throws IllegalArgumentException {
    if (value == null) {
      throw new IllegalArgumentException("JsonPrimitive cannot be null");
    }

    if (value instanceof String) {
      return JsonType.STRING;
    }

    if (value instanceof Number) {
      return JsonType.NUMBER;
    }

    if (value instanceof Boolean) {
      return JsonType.BOOLEAN;
    }

    throw new IllegalArgumentException("JsonPrimitive cannot be "
        + value.getClass().getSimpleName());
  }

  /**
   * Set the value that this {@link JsonPrimitive} represents
   *
   * @param value the value
   * @throws IllegalArgumentException if {@code value} is an invalid type
   */
  public void setValue(Object value) throws IllegalArgumentException {
    this.type = evaluateType(value);
    this.object = value;
  }

  @Override
  public JsonPrimitive getAsPrimitive() {
    return this;
  }

  @Override
  public String getAsString() throws JsonTransformException {
    if (type == JsonType.STRING) {
      return (String) object;
    }
    throw new JsonTransformException("Cannot transform " + this.getClass().getSimpleName()
        + "->" + type + " to String");
  }

  @Override
  public char getAsChar() throws JsonTransformException {
    if (type == JsonType.STRING) {
      String val = (String) object;
      if (val.length() == 1) {
        return val.charAt(0);
      } else {
        throw new JsonDeserializingException("Failed to convert value \""
            + val + "\" to a Character");
      }
    }
    return super.getAsChar();
  }

  @Override
  public Number getAsNumber() {
    if (type == JsonType.NUMBER) {
      return (Number) object;
    }
    return super.getAsNumber();
  }

  @Override
  public int getAsInt() throws JsonTransformException {
    if (type == JsonType.NUMBER) {
      return (int) object;
    }
    return super.getAsInt();
  }

  @Override
  public long getAsLong() throws JsonTransformException {
    if (type == JsonType.NUMBER) {
      return ((Number) object).longValue();
    }
    return super.getAsLong();
  }

  @Override
  public short getAsShort() throws JsonTransformException {
    if (type == JsonType.NUMBER) {
      return ((Number) object).shortValue();
    }
    return super.getAsShort();
  }

  @Override
  public byte getAsByte() throws JsonTransformException {
    if (type == JsonType.NUMBER) {
      return ((Number) object).byteValue();
    }
    return super.getAsByte();
  }

  @Override
  public double getAsDouble() throws JsonTransformException {
    if (type == JsonType.NUMBER) {
      return ((Number) object).doubleValue();
    }
    return super.getAsDouble();
  }

  @Override
  public float getAsFloat() throws JsonTransformException {
    if (type == JsonType.NUMBER) {
      return ((Number) object).floatValue(); //needed for Double to Float casting
    }
    return super.getAsFloat();
  }

  @Override
  public boolean getAsBool() throws JsonTransformException {
    return type == JsonType.BOOLEAN ? (boolean) object : super.getAsBool();
  }

  @Override
  public boolean isPrimitive() {
    return true;
  }

  @Override
  public boolean isString() {
    return type == JsonType.STRING && object instanceof String;
  }

  @Override
  public boolean isChar() {
    return isString() && ((String) object).length() == 1;
  }

  @Override
  public boolean isNumber() {
    return type == JsonType.NUMBER
        && (isInt() || isShort() || isByte() || isLong() || isFloat() || isDouble());
  }

  @Override
  public boolean isInt() {
    return type == JsonType.NUMBER && object instanceof Integer;
  }

  @Override
  public boolean isLong() {
    return type == JsonType.NUMBER && object instanceof Long;
  }

  @Override
  public boolean isShort() {
    return type == JsonType.NUMBER && object instanceof Short;
  }

  @Override
  public boolean isByte() {
    return type == JsonType.NUMBER && object instanceof Byte;
  }

  @Override
  public boolean isDouble() {
    return type == JsonType.NUMBER && object instanceof Double;
  }

  @Override
  public boolean isFloat() {
    return type == JsonType.NUMBER && object instanceof Float;
  }

  @Override
  public boolean isBool() {
    return type == JsonType.BOOLEAN && object instanceof Boolean;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof JsonPrimitive)) {
      return false;
    }

    JsonPrimitive other = (JsonPrimitive) obj;

    return Objects.equals(this.type, other.type)
        && Objects.equals(this.object, other.object);
  }

  @Override
  public String toString() {
    if (type == JsonType.STRING) {
      return '"' + object.toString() + '"';
    }
    return object.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, object);
  }
}
