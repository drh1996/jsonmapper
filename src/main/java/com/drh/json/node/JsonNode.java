package com.drh.json.node;

import com.drh.json.exception.JsonTransformException;
import java.util.Objects;

/**
 * Abstract representation of a JSON element. JSON elements are objects, arrays, strings,
 * numbers and booleans. Each of these are extensions of this class.
 *
 * @since 1.0
 * @see JsonObject
 * @see JsonArray
 * @see JsonPrimitive
 * @author Dan Hayes
 */
public abstract class JsonNode {

  private JsonTransformException createJsonFormatException(Class<?> transformTo)
      throws JsonTransformException {

    return new JsonTransformException("Cannot transform " + this.getClass().getSimpleName()
        + " to " + transformTo.getSimpleName());
  }

  /**
   * Returns a {@link JsonObject} from a {@link JsonNode} reference.
   *
   * @return a {@link JsonObject}
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a {@link JsonObject} type
   */
  public JsonObject getAsObject() throws JsonTransformException {
    throw createJsonFormatException(JsonObject.class);
  }

  /**
   * Returns a {@link JsonArray} from a {@link JsonNode} reference.
   *
   * @return a {@link JsonArray}
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a {@link JsonArray} type
   */
  public JsonArray getAsArray() throws JsonTransformException {
    throw createJsonFormatException(JsonArray.class);
  }

  /**
   * Returns a {@link JsonPrimitive} from a {@link JsonNode} reference.
   *
   * @return a {@link JsonPrimitive}
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a {@link JsonPrimitive} type
   */
  public JsonPrimitive getAsPrimitive() throws JsonTransformException {
    throw createJsonFormatException(JsonPrimitive.class);
  }

  /**
   * Returns a {@link JsonNull} from a {@link JsonNode} reference.
   *
   * @return a {@link JsonNull}
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a {@link JsonNull} type
   */
  public JsonNull getAsNull() throws JsonTransformException {
    throw createJsonFormatException(JsonNull.class);
  }

  /**
   * Returns a {@link String} from a {@link JsonNode} reference.
   *
   * @return a {@link String}
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a {@link String} type
   */
  public String getAsString() throws JsonTransformException {
    throw createJsonFormatException(String.class);
  }

  /**
   * Returns a char from a {@link JsonNode} reference.
   *
   * @return a char
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a char
   */
  public char getAsChar() throws JsonTransformException {
    throw createJsonFormatException(char.class);
  }

  /**
   * Returns a {@link Number} from a {@link JsonNode} reference.
   *
   * @return a {@link Number}
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a {@link Number} type
   */
  public Number getAsNumber() throws JsonTransformException {
    throw createJsonFormatException(Number.class);
  }

  /**
   * Returns an int from a {@link JsonNode} reference.
   *
   * @return an int
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to an int
   */
  public int getAsInt() throws JsonTransformException {
    throw createJsonFormatException(int.class);
  }

  /**
   * Returns a long from a {@link JsonNode} reference.
   *
   * @return a long
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a long
   */
  public long getAsLong() throws JsonTransformException {
    throw createJsonFormatException(long.class);
  }

  /**
   * Returns a short from a {@link JsonNode} reference.
   *
   * @return a short
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a short
   */
  public short getAsShort() throws JsonTransformException {
    throw createJsonFormatException(short.class);
  }

  /**
   * Returns a byte from a {@link JsonNode} reference.
   *
   * @return a byte
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a byte
   */
  public byte getAsByte() throws JsonTransformException {
    throw createJsonFormatException(byte.class);
  }

  /**
   * Returns a double from a {@link JsonNode} reference.
   *
   * @return a double
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a double
   */
  public double getAsDouble() throws JsonTransformException {
    throw createJsonFormatException(double.class);
  }

  /**
   * Returns a float from a {@link JsonNode} reference.
   *
   * @return a float
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a float
   */
  public float getAsFloat() throws JsonTransformException {
    throw createJsonFormatException(float.class);
  }

  /**
   * Returns a boolean from a {@link JsonNode} reference.
   *
   * @return a boolean
   * @throws JsonTransformException if the {@link JsonNode} is not assignable
   *     to a boolean
   */
  public boolean getAsBool() throws JsonTransformException {
    throw createJsonFormatException(boolean.class);
  }

  /**
   * Returns true if a {@link JsonNode} is of a given type. The type checks are as follows:
   * <pre>
   * isType({@link JsonType#OBJECT}) == {@link JsonNode#isObject()}
   * isType({@link JsonType#ARRAY}) == {@link JsonNode#isArray()}
   * isType({@link JsonType#NUMBER}) == {@link JsonNode#isNumber()}
   * isType({@link JsonType#STRING}) == {@link JsonNode#isString()}
   * isType({@link JsonType#BOOLEAN}) == {@link JsonNode#isBool()}
   * isType({@link JsonType#NULL}) == {@link JsonNode#isNull()}
   * </pre>
   *
   * @param type type to check for
   * @return true if the {@link JsonNode} is of a given {@link JsonType}, otherwise false.
   * @see JsonNode#isObject()
   * @see JsonNode#isArray()
   * @see JsonNode#isNumber()
   * @see JsonNode#isString()
   * @see JsonNode#isBool()
   * @see JsonNode#isNull()
   * @throws NullPointerException if the {@code type} parameter is null
   */
  public boolean isType(JsonType type) {
    Objects.requireNonNull(type, "type is null");
    switch (type) {
      case NUMBER:  return isNumber();
      case STRING:  return isString();
      case BOOLEAN: return isBool();
      case ARRAY:   return isArray();
      case OBJECT:  return isObject();
      case NULL:    return isNull();
      default:      return false;
    }
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a {@link JsonNull}.
   *
   * @return true if the {@link JsonNode} is assignable to a {@link JsonNull}, otherwise false.
   */
  public boolean isNull() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a {@link JsonPrimitive}.
   *
   * @return true if the {@link JsonNode} is assignable to a {@link JsonPrimitive}, otherwise false.
   */
  public boolean isPrimitive() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a {@link JsonObject}.
   *
   * @return true if the {@link JsonNode} is assignable to a {@link JsonObject}, otherwise false.
   */
  public boolean isObject() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a {@link JsonArray}.
   *
   * @return true if the {@link JsonNode} is assignable to a {@link JsonArray}, otherwise false.
   */
  public boolean isArray() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a {@link String}.
   *
   * @return true if the {@link JsonNode} is assignable to a {@link String}, otherwise false.
   */
  public boolean isString() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a char.
   *
   * @return true if the {@link JsonNode} is assignable to a char, otherwise false.
   */
  public boolean isChar() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a {@link Number}.
   *
   * @return true if the {@link JsonNode} is assignable to a {@link Number}, otherwise false.
   */
  public boolean isNumber() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to an int.
   *
   * @return true if the {@link JsonNode} is assignable to an int, otherwise false.
   */
  public boolean isInt() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a long.
   *
   * @return true if the {@link JsonNode} is assignable to a long, otherwise false.
   */
  public boolean isLong() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a short.
   *
   * @return true if the {@link JsonNode} is assignable to a short, otherwise false.
   */
  public boolean isShort() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a byte.
   *
   * @return true if the {@link JsonNode} is assignable to a byte, otherwise false.
   */
  public boolean isByte() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a double.
   *
   * @return true if the {@link JsonNode} is assignable to a double, otherwise false.
   */
  public boolean isDouble() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a float.
   *
   * @return true if the {@link JsonNode} is assignable to a float, otherwise false.
   */
  public boolean isFloat() {
    return false;
  }

  /**
   * Returns true if a {@link JsonNode} is assignable to a boolean.
   *
   * @return true if the {@link JsonNode} is assignable to a boolean, otherwise false.
   */
  public boolean isBool() {
    return false;
  }
}
