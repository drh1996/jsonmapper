package com.drh.json.node;

/**
 * A POJO representation of a null JSON value.
 *
 * @since 1.0
 * @see JsonNode
 * @author Dan Hayes
 */
public class JsonNull extends JsonNode {

  @Override
  public JsonNull getAsNull() {
    return this;
  }

  @Override
  public boolean isNull() {
    return true;
  }

  /**
   * Compares an object to ensure it is an instance of a {@link JsonNull}.
   *
   * @param obj the object to be compared for equality with this {@link JsonNull}
   * @return true if the {@code obj} is an instance of {@link JsonNull}, otherwise false.
   */
  @Override
  public boolean equals(Object obj) {
    return obj instanceof JsonNull;
  }

  /**
   * Returns a {@link String} representation of this {@link JsonNull}. For example:
   * <pre>
   * JsonNull jsonNull = new JsonNull()
   *
   * jsonNull.toString() == "null"
   * </pre>
   *
   * @return a {@link String} representation of this {@link JsonNull}.
   */
  @Override
  public String toString() {
    return "null";
  }

  @Override
  public int hashCode() {
    return 0;
  }
}