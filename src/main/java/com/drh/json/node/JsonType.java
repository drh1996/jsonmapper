package com.drh.json.node;

/**
 * The type of json value. Used in a {@link JsonNode}
 */
public enum JsonType {
  /**
   * json object type: e.g. { ... }
   */
  OBJECT,

  /**
   * json array type: e.g. [ ... ]
   */
  ARRAY,

  /**
   * string type: e.g. "example"
   */
  STRING,

  /**
   * number type: e.g. 3.1415
   */
  NUMBER,

  /**
   * boolean type: e.g. true
   */
  BOOLEAN,

  /**
   * null type
   */
  NULL
}
