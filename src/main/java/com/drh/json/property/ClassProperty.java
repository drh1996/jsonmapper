package com.drh.json.property;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import lombok.RequiredArgsConstructor;

/**
 * Holder for class properties required for deserializing with a context.
 */
@RequiredArgsConstructor
public class ClassProperty implements Property {

  private final Class<?> clazz;

  @Override
  public boolean hasAnnotation(Class<? extends Annotation> cls) {
    return clazz.isAnnotationPresent(cls);
  }

  @Override
  public <T extends Annotation> T getAnnotation(Class<T> cls) {
    return clazz.getAnnotation(cls);
  }

  @Override
  public Class<?> getClassType() {
    return clazz;
  }

  //TODO: can get these?? they seem to be striped away from classes at compile time
  @Override
  public Type[] getParameterizedTypes() {
    return new Type[0];
  }

  @Override
  public String getName() {
    return clazz.getName();
  }
}
