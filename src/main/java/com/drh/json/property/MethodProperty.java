package com.drh.json.property;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import lombok.RequiredArgsConstructor;

/**
 * Holder for method properties required for deserializing with a context.
 */
@RequiredArgsConstructor
public class MethodProperty implements Property {

  private final Method method;
  private final FieldProperty linkedField;

  /**
   * Holder for method properties required for deserializing with a context.
   *
   * @param method used to extract properties from
   */
  public MethodProperty(Method method) {
    this(method, null);
  }

  @Override
  public boolean hasAnnotation(Class<? extends Annotation> clazz) {
    if (method.isAnnotationPresent(clazz)) {
      return true;
    }

    return linkedField != null && linkedField.hasAnnotation(clazz);
  }

  @Override
  public <T extends Annotation> T getAnnotation(Class<T> clazz) {
    T annotation = method.getAnnotation(clazz);
    if (annotation != null) {
      return annotation;
    }

    if (linkedField == null) {
      return null;
    }

    return linkedField.getAnnotation(clazz);
  }

  @Override
  public Class<?> getClassType() {
    if (linkedField != null) {
      return linkedField.getClassType();
    }

    return method.getReturnType();
  }

  @Override
  public Type[] getParameterizedTypes() {
    if (linkedField != null) {
      return linkedField.getParameterizedTypes();
    }
    return new Type[0];
  }

  @Override
  public String getName() {
    return linkedField != null ? linkedField.getName() : method.getName();
  }
}
