package com.drh.json.property;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Holder for properties required for deserializing with a context.
 */
public interface Property {

  /**
   * Check if a property has a given annotation
   *
   * @param clazz annotation to check
   * @return true if the annotation present, false otherwise
   */
  boolean hasAnnotation(Class<? extends Annotation> clazz);

  /**
   * Returns this property's annotation for the specified type
   * if such an annotation is present, else null.
   *
   * @param clazz the class for the annotation
   * @return the annotation
   * @param <T> the annotation type
   */
  <T extends Annotation> T getAnnotation(Class<T> clazz);

  /**
   * Returns the class for the property
   *
   * @return the class
   */
  Class<?> getClassType();

  /**
   * Returns the parameterized types for the property's class
   *
   * @return an array of parameterized types
   */
  Type[] getParameterizedTypes();

  /**
   * Returns the name of the property's class
   *
   * @return the name
   */
  String getName();
}
