package com.drh.json.property;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import lombok.RequiredArgsConstructor;

/**
 * Holder for field properties required for deserializing with a context.
 */
@RequiredArgsConstructor
public class FieldProperty implements Property {

  private final Field field;

  @Override
  public boolean hasAnnotation(Class<? extends Annotation> clazz) {
    return field.isAnnotationPresent(clazz);
  }

  @Override
  public <T extends Annotation> T getAnnotation(Class<T> clazz) {
    return field.getAnnotation(clazz);
  }

  @Override
  public Class<?> getClassType() {
    return field.getType();
  }

  @Override
  public Type[] getParameterizedTypes() {
    Type genericFieldType = field.getGenericType();
    if (genericFieldType instanceof ParameterizedType type) {
      return type.getActualTypeArguments();
    } else {
      return new Type[0];
    }
  }

  @Override
  public String getName() {
    return field.getName();
  }
}
