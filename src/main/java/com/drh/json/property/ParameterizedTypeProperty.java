package com.drh.json.property;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Holder for parameterized type properties required for deserializing with a context.
 */
public class ParameterizedTypeProperty implements Property {

  private final ParameterizedType type;
  private final Class<?> rawType;

  /**
   * Holder for parameterized type properties required for deserializing with a context.
   *
   * @param type used to extract properties from
   */
  public ParameterizedTypeProperty(Type type) {
    if (type instanceof ParameterizedType) {
      this.type = (ParameterizedType) type;
      this.rawType = ((Class<?>) this.type.getRawType());
    } else {
      throw new IllegalArgumentException("type is not a ParameterizedType");
    }
  }

  @Override
  public boolean hasAnnotation(Class<? extends Annotation> cls) {
    return rawType.isAnnotationPresent(cls);
  }

  @Override
  public <T extends Annotation> T getAnnotation(Class<T> cls) {
    return rawType.getAnnotation(cls);
  }

  @Override
  public Class<?> getClassType() {
    return rawType;
  }

  @Override
  public Type[] getParameterizedTypes() {
    return type.getActualTypeArguments();
  }

  @Override
  public String getName() {
    return type.getTypeName();
  }
}
