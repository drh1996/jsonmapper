package com.drh.json.styling;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Defines properties to use when serializing an object.
 * <br><br>
 * {@code tabSize} the number of spaces to use for child indenting.<br>
 * {@code keyValueSeparatorSize} the number of spaces to use between a key and value.<br>
 * {@code fieldNewLine} determine if a new line should be placed after each value.<br>
 *
 * @since 1.0
 * @author Dan Hayes
 */
@RequiredArgsConstructor
public class JsonStyling {

  @Getter
  private final int tabSize;

  @Getter
  private final int keyValueSeparatorSize;

  private final boolean fieldNewLine;

  /**
   * True if a new line should be placed after each value.
   *
   * @return should have new line
   */
  public boolean fieldNewLine() {
    return fieldNewLine;
  }
}
