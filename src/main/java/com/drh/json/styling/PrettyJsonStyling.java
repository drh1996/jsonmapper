package com.drh.json.styling;

/**
 * Defines a standard human-readable styling for serialized objects.
 * This style has 2 spaces for child indexing, 1 space between keys and values,
 * and places a new line after each value.
 *
 * @since 1.0
 * @see JsonStyling
 * @author Dan Hayes
 */
public class PrettyJsonStyling extends JsonStyling {

  /**
   * Defines a standard human-readable styling for serialized objects.
   * This style has 2 spaces for child indexing, 1 space between keys and values,
   * and places a new line after each value.
   */
  public PrettyJsonStyling() {
    super(2, 1, true);
  }
}
