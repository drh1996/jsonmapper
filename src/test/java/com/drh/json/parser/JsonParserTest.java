package com.drh.json.parser;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.exception.JsonParsingException;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonNull;
import com.drh.json.node.JsonObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Json Parser Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonParserTest {

  @Test
  public void test_parse_object() {
    String src = "{\"a\":\"hello\",\"b\":1,\"c\":true,\"\\\"d\\\"\":3.1415,\"e\\\\\":\"!\",\"f\":null,\"g\":{\"inner\":\"world\"},\"h\":[\"first\",2]}";
    JsonNode expected = new JsonObject()
        .add("a", "hello")
        .add("b", 1)
        .add("c", true)
        .add("\"d\"", 3.1415D)
        .add("e\\", "!")
        .add("f", new JsonNull())
        .add("g", new JsonObject().add("inner", "world"))
        .add("h", new JsonArray().add("first").add(2));

    JsonNode actual = JsonParser.parse(src);

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void test_parse_array() {
    String src = "[1,2,3,4,5000000000]";
    JsonNode expected = new JsonArray()
        .add(1)
        .add(2)
        .add(3)
        .add(4)
        .add(5);

    JsonNode actual = JsonParser.parse(src);

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void test_parse_special_characters() {
    String src = "[\"\\t\", \"\\\\\", \"\\n\", \"\\r\", \"\\t\", \"\\u1234\", \"\\\"\"]";
    JsonNode expected = new JsonArray()
        .add("\t")
        .add("\\")
        .add("\n")
        .add("\r")
        .add("\t")
        .add("\u1234")
        .add("\"");

    JsonNode actual = JsonParser.parse(src);

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void test_parse_close_bracket_in_string() {
    JsonObject expected = new JsonObject()
        .add("abc", new JsonObject().add("one", "fs}a\\s"));

    JsonNode actual = JsonParser.parse("{\"abc\":{\"one\":\"fs}a\\\\s\"}}");

    assertTrue(actual.isObject());
    assertEquals(expected, actual.getAsObject());
  }

  @Test
  public void test_parse_invalid_start() {
    String src = "(1,2,3,4,5)";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Json does not start with object or array reference.", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_end() {
    String src = "[{\"b\":1)]";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid Json Structure at: [{\"b\":1)]", exception.getMessage());
  }

  @Test
  public void test_parse_missing_end() {
    String src = "{\"a\":{\"b\":true}";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Missing end of json '}': {\"a\":{\"b\":true}", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_key_start() {
    String src = "{'a':1}";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid json start of key ''' at index: 1, json: {'a':1}", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_key_value_separator() {
    String src = "{\"a\"->1}";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid key value separator '-' at index: 4", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_value() {
    String src = "{\"a\":\"1}";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid json '\"' at index: 5", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_value_separator() {
    String src = "{\"a\":1)";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid json ')' at index: 6", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_array_value() {
    String src = "[false, abc, 2]";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid json 'a' at index: 7", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_array_separator() {
    String src = "[1, \"hello world\", 2)";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid json ')' at index: 18", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_escape_char() {
    String src = "[1, \"hello world\", \"\\a\"]";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid special character '\\a' found in string: \\a", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_unicode_char() {
    String src = "[1, \"hello world\", \"\\uZZZZ\"]";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Failed to parse '\\u' special character in the String: \"\\uZZZZ\", Reason: The 4 characters following '\\u' must be hexadecimal.", exception.getMessage());
  }

  @Test
  public void test_parse_invalid_unicode_char_length() {
    String src = "[1, \"hello world\", \"\\u1\"]";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Failed to parse '\\u' special character in the String: \"\\u1\", Reason: '\\u' required exactly 4 hexadecimal characters to be parsed.", exception.getMessage());
  }

  @Test
  public void test_invalid_json_value() {
    String src = "{\"a\":{\"b\":1)}";

    JsonParsingException exception = assertThrows(JsonParsingException.class,
        () -> JsonParser.parse(src));

    assertEquals("Invalid json ')' at index: 6", exception.getMessage());
  }
}
