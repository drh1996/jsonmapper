package com.drh.json.property;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Class Property Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ClassPropertyTest {

  @Test
  public void test_has_annotation() {
    Property property = new ClassProperty(TestCls.class);

    assertTrue(property.hasAnnotation(Ann1.class));
    assertFalse(property.hasAnnotation(Ann2.class));
  }

  @Test
  public void test_get_annotation() {
    Property property = new ClassProperty(TestCls.class);

    assertNotNull(property.getAnnotation(Ann1.class));
    assertNull(property.getAnnotation(Ann2.class));
  }

  @Test
  public void test_get_class_type() {
    Property property = new ClassProperty(TestCls.class);

    assertEquals(TestCls.class, property.getClassType());
  }

  @Test
  public void test_get_parameterized_types() {
    Property property = new ClassProperty(TestCls.class);

    assertEquals(0, property.getParameterizedTypes().length);
  }

  @Test
  public void test_get_name() {
    Property property = new ClassProperty(TestCls.class);

    assertEquals("com.drh.json.property.ClassPropertyTest$TestCls", property.getName());
  }

  @Ann1
  private static class TestCls {
  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann1 {

  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann2 {

  }
}
