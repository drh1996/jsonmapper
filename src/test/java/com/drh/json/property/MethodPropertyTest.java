package com.drh.json.property;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import lombok.Getter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Method Property Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class MethodPropertyTest {

  @Mock
  private FieldProperty fieldProperty;

  @Test
  public void test_has_annotation_field_level() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getString");

    Property property = new MethodProperty(method, fieldProperty);

    when(fieldProperty.hasAnnotation(eq(Ann1.class))).thenReturn(true);
    when(fieldProperty.hasAnnotation(eq(Ann2.class))).thenReturn(false);

    assertTrue(property.hasAnnotation(Ann1.class));
    assertFalse(property.hasAnnotation(Ann2.class));
  }

  @Test
  public void test_has_annotation_method_level() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getString2");

    Property property = new MethodProperty(method, fieldProperty);

    when(fieldProperty.hasAnnotation(eq(Ann1.class))).thenReturn(false);

    assertFalse(property.hasAnnotation(Ann1.class));
    assertTrue(property.hasAnnotation(Ann2.class));
  }

  @Test
  public void test_has_annotation_method_level_no_field() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getString2");

    Property property = new MethodProperty(method);

    assertFalse(property.hasAnnotation(Ann1.class));
    assertTrue(property.hasAnnotation(Ann2.class));
  }

  @Test
  public void test_get_annotation_field_level() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getString");

    Property property = new MethodProperty(method, fieldProperty);

    when(fieldProperty.getAnnotation(eq(Ann1.class))).thenReturn(createAnn1());
    when(fieldProperty.getAnnotation(eq(Ann2.class))).thenReturn(null);

    assertNotNull(property.getAnnotation(Ann1.class));
    assertNull(property.getAnnotation(Ann2.class));
  }

  @Test
  public void test_get_annotation_method_level() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getString2");

    Property property = new MethodProperty(method, fieldProperty);

    when(fieldProperty.getAnnotation(eq(Ann1.class))).thenReturn(null);

    assertNull(property.getAnnotation(Ann1.class));
    assertNotNull(property.getAnnotation(Ann2.class));
  }

  @Test
  public void test_get_annotation_method_level_no_field() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getString2");

    Property property = new MethodProperty(method);

    assertNull(property.getAnnotation(Ann1.class));
    assertNotNull(property.getAnnotation(Ann2.class));
  }

  @Test
  public void test_get_class_type() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getStrings");
    Property property = new MethodProperty(method);

    assertEquals(List.class, property.getClassType());
  }

  @Test
  public void test_get_class_type_field_level() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getStrings");
    Property property = new MethodProperty(method, fieldProperty);

    when(fieldProperty.getClassType()).thenReturn((Class) List.class);

    assertEquals(List.class, property.getClassType());

    verify(fieldProperty).getClassType();
  }

  @Test
  public void test_get_parameterized_types_null() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getStrings");
    Property property = new MethodProperty(method);

    Type[] types = property.getParameterizedTypes();
    assertEquals(0, types.length);
  }

  @Test
  public void test_get_parameterized_types() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getStrings");
    Property property = new MethodProperty(method, fieldProperty);

    when(fieldProperty.getParameterizedTypes()).thenReturn(new Type[] {String.class});

    Type[] types = property.getParameterizedTypes();

    verify(fieldProperty).getParameterizedTypes();

    assertEquals(1, types.length);
    assertEquals(String.class, types[0]);
  }

  @Test
  public void test_get_name() throws NoSuchMethodException {
    Method method = TestCls.class.getDeclaredMethod("getStrings");
    Property property = new MethodProperty(method);

    assertEquals("getStrings", property.getName());
  }

  private static Ann1 createAnn1() {
    return new Ann1() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return Ann1.class;
      }
    };
  }

  private static Ann2 createAnn2() {
    return new Ann2() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return Ann2.class;
      }
    };
  }

  @Getter
  private static class TestCls {

    @Ann1
    private String string;

    private String string2;

    private List<String> strings;

    public String getString() {
      return string;
    }

    @Ann2
    public String getString2() {
      return string2;
    }
  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann1 {

  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann2 {

  }
}
