package com.drh.json.property;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Field Property Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class FieldPropertyTest {

  @Test
  public void test_has_annotation() throws NoSuchFieldException {
    Field field = TestCls.class.getDeclaredField("string");
    FieldProperty property = new FieldProperty(field);

    assertTrue(property.hasAnnotation(Ann1.class));
    assertFalse(property.hasAnnotation(Ann2.class));
  }

  @Test
  public void test_get_annotation() throws NoSuchFieldException {
    Field field = TestCls.class.getDeclaredField("string");
    FieldProperty property = new FieldProperty(field);

    assertNotNull(property.getAnnotation(Ann1.class));
    assertNull(property.getAnnotation(Ann2.class));
  }

  @Test
  public void test_get_class_type() throws NoSuchFieldException {
    Field field = TestCls.class.getDeclaredField("strings");
    FieldProperty property = new FieldProperty(field);

    assertEquals(List.class, property.getClassType());
  }

  @Test
  public void test_get_parameterized_types_null() throws NoSuchFieldException {
    Field field = TestCls.class.getDeclaredField("string");
    FieldProperty property = new FieldProperty(field);

    Type[] types = property.getParameterizedTypes();
    assertEquals(0, types.length);
  }

  @Test
  public void test_get_parameterized_types() throws NoSuchFieldException {
    Field field = TestCls.class.getDeclaredField("strings");
    FieldProperty property = new FieldProperty(field);

    Type[] types = property.getParameterizedTypes();
    assertEquals(1, types.length);
    assertEquals(String.class, types[0]);
  }

  @Test
  public void test_get_name() throws NoSuchFieldException {
    Field field = TestCls.class.getDeclaredField("strings");
    FieldProperty property = new FieldProperty(field);

    assertEquals("strings", property.getName());
  }

  private static class TestCls {

    @Ann1
    private String string;

    private List<String> strings;
  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann1 {

  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann2 {

  }
}
