package com.drh.json.property;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.reference.TypeReference;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Type;
import java.time.LocalDate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Parameterized Type Property Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ParameterizedTypePropertyTest {

  @Test
  public void test_has_annotation() {
    TypeReference<TestCls<LocalDate>> typeRef = new TypeReference<>() {};
    Property property = new ParameterizedTypeProperty(typeRef.getType());

    assertTrue(property.hasAnnotation(Ann1.class));
    assertFalse(property.hasAnnotation(Ann2.class));
  }

  @Test
  public void test_get_annotation() {
    Property property = new ClassProperty(TestCls.class);

    assertNotNull(property.getAnnotation(Ann1.class));
    assertNull(property.getAnnotation(Ann2.class));
  }

  @Test
  public void test_get_class_type() {
    TypeReference<TestCls<LocalDate>> typeRef = new TypeReference<>() {};
    Property property = new ParameterizedTypeProperty(typeRef.getType());

    assertEquals(TestCls.class, property.getClassType());
  }

  @Test
  public void test_get_parameterized_types() {
    TypeReference<TestCls<LocalDate>> typeRef = new TypeReference<>() {};
    Property property = new ParameterizedTypeProperty(typeRef.getType());

    Type[] types = property.getParameterizedTypes();

    assertNotNull(types);
    assertEquals(1, types.length);
    assertNotNull(types[0]);
    assertEquals(LocalDate.class, types[0]);
  }

  @Test
  public void test_get_name() {
    TypeReference<TestCls<LocalDate>> typeRef = new TypeReference<>() {};
    Property property = new ParameterizedTypeProperty(typeRef.getType());

    assertEquals("com.drh.json.property.ParameterizedTypePropertyTest$TestCls<java.time.LocalDate>", property.getName());
  }


  @Ann1
  private static class TestCls<T> {
  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.TYPE_USE,  ElementType.METHOD, ElementType.TYPE})
  private @interface Ann1 {

  }

  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
  private @interface Ann2 {

  }
}
