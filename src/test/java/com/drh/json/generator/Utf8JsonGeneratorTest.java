package com.drh.json.generator;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.drh.json.styling.JsonStyling;
import com.drh.json.styling.PrettyJsonStyling;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("UTF8 Json Generator Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class Utf8JsonGeneratorTest {

  private JsonGenerator generator;

  @BeforeEach
  public void beforeEach() {
    generator = new Utf8JsonGenerator(new JsonStyling(0, 0, false));
  }

  @Test
  public void test_write() {
    String expected = "{\"1\":\"abc\",\"2\":\"a\",\"3\":1,\"4\\n\":-10,\"5\":1.1415,\"6\":-1.1415,\"7\":true}";

    assertDoesNotThrow(() ->
        generator.objectStart()
        .write("1", "abc")
        .write("2", 'a')
        .write("3", 1)
        .write("4\n", -10L)
        .write("5", 1.1415F)
        .write("6", -1.1415D)
        .write("7", true)
        .objectEnd());

    String actual = generator.getOutput();

    assertEquals(expected, actual);
  }

  @Test
  public void test_write_value() {
    String expected = "[1,\"two\",null,null,true,false,{\"test\":0},\"4\",5,10,1.1415,1.1415]";

    assertDoesNotThrow(() ->
        generator.arrayStart()
            .writeValue(1)
            .writeValue("two")
            .writeValue(null)
            .writeNull()
            .writeValue(true)
            .writeValue(false)
            .objectStart()
              .write("test", 0)
            .objectEnd()
            .writeValue('4')
            .writeValue(5)
            .writeValue(10L)
            .writeValue(1.1415F)
            .writeValue(1.1415D)
            .arrayEnd());

    String actual = generator.getOutput();

    assertEquals(expected, actual);
  }

  @Test
  public void test_object_start_invalid() {
    generator.objectStart();

    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.objectStart());

    assertEquals("Cannot start object: Expected field", e.getMessage());
  }

  @Test
  public void test_object_end_invalid() {
    generator.objectStart();
    generator.writeField("test");

    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.objectEnd());

    assertEquals("Cannot end object: Expected value", e.getMessage());
  }

  @Test
  public void test_array_start_invalid() {
    generator.objectStart();

    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.arrayStart());

    assertEquals("Cannot start array: Expected field", e.getMessage());
  }

  @Test
  public void test_invalid_start() {
    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.objectEnd());

    assertEquals("Cannot end object: Expected '{' or '['", e.getMessage());
  }

  @Test
  public void test_write_field_invalid_value() {
    generator.objectStart()
        .write("a", "b");

    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.writeValue(1));

    assertEquals("Cannot write int value '1': Expected ',' or '}' or ']'", e.getMessage());
  }

  @Test
  public void test_write_field_after_complete() {
    generator.objectStart()
        .objectEnd();

    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.writeField("test"));

    assertEquals("Cannot write field 'test': Json Structure complete, no more can be added", e.getMessage());
  }

  @Test
  public void test_write_field_invalid() {
    generator.arrayStart();

    RuntimeException e = assertThrows(RuntimeException.class,
        () -> generator.writeField("test"));

    assertEquals("Cannot write field 'test': Expected value", e.getMessage());
  }

  @Test
  public void test_object_with_special_chars() {
    String expected = "{\"test\":\"\\u0001ø\"}";

    assertDoesNotThrow(() ->
        generator.objectStart()
            .write("test", "\1ø")
            .objectEnd());

    String actual = generator.getOutput();

    assertEquals(expected, actual);
  }

  @Test
  public void test_array_large() {
    String expected = '[' + IntStream.rangeClosed(0, 3000)
        .mapToObj(String::valueOf)
        .reduce((prev, next) -> prev + "," + next)
        .orElse("") + ']';

    assertDoesNotThrow(() -> {
      generator.arrayStart();
      IntStream.rangeClosed(0, 3000).forEach(i -> generator.writeValue(i));
      generator.arrayEnd();
    });

    String actual = generator.getOutput();

    assertEquals(expected, actual);
  }

  @Test
  public void test_object_with_pretty_styling() {
    String expected = "{\n  \"test\": 1\n}";

    generator = new Utf8JsonGenerator(new PrettyJsonStyling());

    assertDoesNotThrow(() ->
        generator.objectStart()
            .write("test", 1)
            .objectEnd());

    String actual = generator.getOutput();

    assertEquals(expected, actual);
  }


}
