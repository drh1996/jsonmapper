package com.drh.json;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.drh.json.reference.TypeReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Json Mapper Integration Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonMapperIT {

  @Nested
  class ListDeserializeTests {
    @Test
    public void test_deserialize_list_of_strings() {
      JsonMapper mapper = new JsonMapper();
      List<String> res = mapper.fromJson("[\"one\", \"two\", null, \"3\", \"\"]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly("one", "two", null, "3", "");
    }

    @Test
    public void test_deserialize_list_of_chars() {
      JsonMapper mapper = new JsonMapper();
      List<Character> res = mapper.fromJson("[\"a\", \"b\", null, \"c\"]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly('a', 'b', null, 'c');
    }

    @Test
    public void test_deserialize_list_of_booleans() {
      JsonMapper mapper = new JsonMapper();
      List<Boolean> res = mapper.fromJson("[null, true, false]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly(null, true, false);
    }

    @Test
    public void test_deserialize_list_of_bytes() {
      JsonMapper mapper = new JsonMapper();
      List<Byte> res = mapper.fromJson("[1, 2, null, 3]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly((byte) 1, (byte) 2, null, (byte) 3);
    }

    @Test
    public void test_deserialize_list_of_shorts() {
      JsonMapper mapper = new JsonMapper();
      List<Short> res = mapper.fromJson("[1, 2, null, 3]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly((short) 1, (short) 2, null, (short) 3);
    }

    @Test
    public void test_deserialize_list_of_integers() {
      JsonMapper mapper = new JsonMapper();
      List<Integer> res = mapper.fromJson("[1, 2, null, 3]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly(1, 2, null, 3);
    }

    @Test
    public void test_deserialize_list_of_longs() {
      JsonMapper mapper = new JsonMapper();
      List<Long> res = mapper.fromJson("[1, 2, null, 3]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly(1L, 2L, null, 3L);
    }

    @Test
    public void test_deserialize_list_of_floats() {
      JsonMapper mapper = new JsonMapper();
      List<Float> res = mapper.fromJson("[1.2345, 2.0, null, 3]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly(1.2345F, 2F, null, 3F);
    }

    @Test
    public void test_deserialize_list_of_doubles() {
      JsonMapper mapper = new JsonMapper();
      List<Double> res = mapper.fromJson("[1.2345, 2.0, null, 3]", new TypeReference<>() {});

      assertThat(res)
          .containsExactly(1.2345D, 2D, null, 3D);
    }

    @Test
    public void test_deserialize_list_of_dates() throws ParseException {
      JsonMapper mapper = new JsonMapper();
      List<Date> res = mapper.fromJson(
          "[\"2025-01-01T12:11:10.000\", \"1970-01-01T00:00:00.000\", null, \"1999-12-31T23:59:59.999\"]",
          new TypeReference<>() {});

      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);

      assertThat(res)
          .containsExactly(
              formatter.parse("2025-01-01T12:11:10.000"),
              formatter.parse("1970-01-01T00:00:00.000"),
              null,
          formatter.parse("1999-12-31T23:59:59.999"));
    }

    @Test
    public void test_deserialize_list_of_local_dates() {
      JsonMapper mapper = new JsonMapper();
      List<LocalDate> res = mapper.fromJson(
          "[\"2025-01-01\", \"1970-01-01\", null, \"1999-12-31\"]",
          new TypeReference<>() {});

      assertThat(res)
          .containsExactly(
              LocalDate.of(2025, 1, 1),
              LocalDate.of(1970, 1, 1),
              null,
              LocalDate.of(1999, 12, 31));
    }

    @Test
    public void test_deserialize_list_of_local_date_times() {
      JsonMapper mapper = new JsonMapper();
      List<LocalDateTime> res = mapper.fromJson(
          "[\"2025-01-01T12:11:10.000\", \"1970-01-01T00:00:00.000\", null, \"1999-12-31T23:59:59.999\"]",
          new TypeReference<>() {});

      assertThat(res)
          .containsExactly(
              LocalDateTime.of(2025, 1, 1, 12, 11, 10),
              LocalDateTime.of(1970, 1, 1, 0, 0, 0),
              null,
              LocalDateTime.of(1999, 12, 31, 23, 59, 59, 999_000_000));
    }

    @Test
    public void test_deserialize_list_of_uuids() {
      JsonMapper mapper = new JsonMapper();
      List<UUID> res = mapper.fromJson(
          "[\"ab3ecf90-0630-4fce-b3e8-d570a46a5cc2\", null]",
          new TypeReference<>() {});

      assertThat(res)
          .containsExactly(
              UUID.fromString("ab3ecf90-0630-4fce-b3e8-d570a46a5cc2"),
              null);
    }
  }

  @Nested
  class ArrayDeserializeTests {
    @Test
    public void test_deserialize_list_of_strings() {
      JsonMapper mapper = new JsonMapper();
      String[] res = mapper.fromJson("[\"one\", \"two\", null, \"3\", \"\"]", String[].class);

      assertThat(res)
          .containsExactly("one", "two", null, "3", "");
    }

    @Test
    public void test_deserialize_list_of_objects() {
      JsonMapper mapper = new JsonMapper();
      Object[] res = mapper.fromJson("[{}, null]", Object[].class);

      assertEquals(2, res.length);
      assertNotNull(res[0]);
      assertNull(res[1]);
    }
  }
}
