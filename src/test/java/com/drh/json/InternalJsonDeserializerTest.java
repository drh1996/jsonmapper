package com.drh.json;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.annotation.JsonDeserialize;
import com.drh.json.deserializer.JsonDeserializer;
import com.drh.json.deserializer.PrimitiveDeserializers;
import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonObject;
import com.drh.json.support.DataLoader;
import com.drh.json.support.ReflectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Internal Json Deserializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InternalJsonDeserializerTest {

  @Mock
  private JsonDeserializer<Object> deserializer;

  @Mock
  private DeserializerManager manager;

  private InternalJsonDeserializer internalDeserializer;

  @BeforeEach
  public void beforeEach() throws NoSuchFieldException, IllegalAccessException {
    internalDeserializer = new InternalJsonDeserializer(manager);
  }

  @Test
  public void test_add_deserializer() {
    doNothing().when(manager).addDeserializer(any(), any());

    assertDoesNotThrow(() -> internalDeserializer.addDeserializer(Object.class, deserializer));

    verify(manager).addDeserializer(any(), any());
  }

  @Test
  public void test_deserialize_from_string() {
    when(manager.deserialize(any(), any(), eq(null))).thenReturn(new Object());

    assertDoesNotThrow(
        () -> internalDeserializer.deserialize("{\"one\":1}", Object.class));

    verify(manager).deserialize(any(), any(), eq(null));
  }

  @Test
  public void test_deserialize_from_node() {
    when(manager.deserialize(any(), any(), eq(null))).thenReturn(new Object());

    assertDoesNotThrow(
        () -> internalDeserializer.deserialize(new JsonObject(), Object.class, null));

    verify(manager).deserialize(any(), any(), eq(null));
  }
}
