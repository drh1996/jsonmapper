package com.drh.json.node;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.exception.JsonTransformException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Json Array Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonArrayTest {

  @Test
  public void test_get() {
    JsonArray arr = new JsonArray();
    arr.add(1);

    JsonNode elem = arr.get(0);
    assertTrue(elem.isInt());
    assertEquals(1, elem.getAsInt());
  }

  @Test
  public void test_get_object() {
    JsonArray arr = new JsonArray();
    arr.add(new JsonObject());

    assertTrue(arr.isObject(0));
    JsonObject elem = arr.getObject(0);
    assertEquals(new JsonObject(), elem);
  }

  @Test
  public void test_get_array() {
    JsonArray arr = new JsonArray();
    arr.add(new JsonArray());

    assertTrue(arr.isArray(0));
    JsonArray elem = arr.getArray(0);
    assertEquals(new JsonArray(), elem);
  }

  @Test
  public void test_get_primitive() {
    JsonArray arr = new JsonArray();
    arr.add(new JsonPrimitive(1));

    assertTrue(arr.isPrimitive(0));
    JsonPrimitive elem = arr.getPrimitive(0);
    assertEquals(new JsonPrimitive(1), elem);
  }

  @Test
  public void test_get_string() {
    JsonArray arr = new JsonArray();
    arr.add("test");

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isString(0));
    String elem = arr.getString(0);
    assertEquals("test", elem);
  }

  @Test
  public void test_get_char() {
    JsonArray arr = new JsonArray();
    arr.add("c");

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isChar(0));
    char elem = arr.getChar(0);
    assertEquals('c', elem);
  }

  @Test
  public void test_get_number() {
    JsonArray arr = new JsonArray();
    arr.add(1);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isNumber(0));
    Number elem = arr.getNumber(0);
    assertEquals(1, elem);
  }

  @Test
  public void test_get_int() {
    JsonArray arr = new JsonArray();
    arr.add(1);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isInt(0));
    int elem = arr.getInt(0);
    assertEquals(1, elem);
  }

  @Test
  public void test_get_long() {
    JsonArray arr = new JsonArray();
    arr.add(1L);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isLong(0));
    long elem = arr.getLong(0);
    assertEquals(1L, elem);
  }

  @Test
  public void test_get_short() {
    JsonArray arr = new JsonArray();
    arr.add((short) 1);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isShort(0));
    short elem = arr.getShort(0);
    assertEquals((short) 1, elem);
  }

  @Test
  public void test_get_byte() {
    JsonArray arr = new JsonArray();
    arr.add((byte) 1);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isByte(0));
    byte elem = arr.getByte(0);
    assertEquals((byte) 1, elem);
  }

  @Test
  public void test_get_double() {
    JsonArray arr = new JsonArray();
    arr.add(1D);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isDouble(0));
    double elem = arr.getDouble(0);
    assertEquals(1D, elem);
  }

  @Test
  public void test_get_float() {
    JsonArray arr = new JsonArray();
    arr.add(1F);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isFloat(0));
    float elem = arr.getFloat(0);
    assertEquals(1F, elem);
  }

  @Test
  public void test_get_bool() {
    JsonArray arr = new JsonArray();
    arr.add(true);

    assertTrue(arr.isPrimitive(0));
    assertTrue(arr.isBool(0));
    boolean elem = arr.getBool(0);
    assertTrue(elem);
  }

  @Test
  public void test_get_null() {
    JsonArray arr = new JsonArray();
    arr.add(new JsonNull());

    assertTrue(arr.isNull(0));
    JsonNull elem = arr.getNull(0);
    assertEquals(new JsonNull(), elem);
  }

  @Test
  public void test_get_index_out_of_bounds() {
    JsonArray arr = new JsonArray();
    arr.add(1);

    IndexOutOfBoundsException aboveSize = assertThrows(IndexOutOfBoundsException.class,
        () -> arr.get(1));

    assertEquals("Index 1 out of bounds for length 1", aboveSize.getMessage());

    IndexOutOfBoundsException negative = assertThrows(IndexOutOfBoundsException.class,
        () -> arr.get(-1));

    assertEquals("Index -1 out of bounds for length 1", negative.getMessage());
  }

  @Test
  public void test_add_json_node() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add(new JsonPrimitive(1));

    assertFalse(arr.isEmpty());
    assertEquals(1, arr.size());
    assertEquals(1, arr.get(0).getAsInt());
  }

  @Test
  public void test_add_json_node_with_index() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add(1);
    arr.add(3);
    arr.add(1, new JsonPrimitive(2));

    assertFalse(arr.isEmpty());
    assertEquals(3, arr.size());
    assertEquals(2, arr.get(1).getAsInt());
  }

  @Test
  public void test_add_json_node_nullptr() {
    JsonArray arr = new JsonArray();

    NullPointerException npe = assertThrows(NullPointerException.class,
        () -> arr.add((JsonNode) null));

    assertEquals("element is null", npe.getMessage());
  }

  @Test
  public void test_add_string() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add("test");

    assertFalse(arr.isEmpty());
    assertEquals(1, arr.size());
    assertEquals("test", arr.get(0).getAsString());
  }

  @Test
  public void test_add_string_null() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add((String) null);

    assertFalse(arr.isEmpty());
    assertEquals(1, arr.size());
    assertTrue(arr.get(0).isNull());
  }

  @Test
  public void test_add_string_null_with_index() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add("1");
    arr.add("3");
    arr.add(1, (String) null);

    assertFalse(arr.isEmpty());
    assertEquals(3, arr.size());
    assertTrue(arr.get(1).isNull());
  }

  @Test
  public void test_add_number() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add(1);

    assertFalse(arr.isEmpty());
    assertEquals(1, arr.size());
    assertEquals(1, arr.get(0).getAsInt());
  }

  @Test
  public void test_add_number_with_index() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add(1);
    arr.add(3);

    arr.add(1, 2);

    assertFalse(arr.isEmpty());
    assertEquals(3, arr.size());
    assertEquals(2, arr.get(1).getAsInt());
  }

  @Test
  public void test_add_boolean() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add(true);

    assertFalse(arr.isEmpty());
    assertEquals(1, arr.size());
    assertTrue(arr.get(0).getAsBool());
  }

  @Test
  public void test_add_boolean_with_index() {
    JsonArray arr = new JsonArray();

    assertTrue(arr.isEmpty());

    arr.add(1);
    arr.add(3);
    arr.add(1, true);

    assertFalse(arr.isEmpty());
    assertEquals(3, arr.size());
    assertTrue(arr.get(1).getAsBool());
  }

  @Test
  public void test_add_with_index_out_of_bounds() {
    JsonArray arr = new JsonArray();

    IndexOutOfBoundsException aboveRange = assertThrows(IndexOutOfBoundsException.class,
        () -> arr.add(1, 1));

    assertEquals("Index: 1, Size: 0", aboveRange.getMessage());

    IndexOutOfBoundsException negative = assertThrows(IndexOutOfBoundsException.class,
        () -> arr.add(-1, 1));

    assertEquals("Index: -1, Size: 0", negative.getMessage());
  }

  @Test
  public void test_iterator() {
    final int[] expected = {1,2,3};

    JsonArray arr = new JsonArray()
        .add(1)
        .add(2)
        .add(3);

    int i = 0;
    for (JsonNode node : arr) {
      assertTrue(node.isInt());
      assertEquals(expected[i], node.getAsInt());
      i++;
    }
  }

  @Test
  public void test_equals() {
    JsonArray a1 = new JsonArray()
        .add(1)
        .add(2)
        .add(3);

    JsonArray a2 = new JsonArray()
        .add(1)
        .add(2)
        .add(3);

    assertEquals(a1, a1);
    assertEquals(a1, a2);
    assertEquals(a2, a1);

    assertNotEquals(a1, new JsonArray());
    assertNotEquals(a1, new JsonObject());
  }

  @Test
  public void test_to_string() {
    JsonArray array = new JsonArray().add(1).add(2).add(3);
    assertEquals("[1,2,3]", array.toString());
  }

  @Test
  public void test_is_array() {
    JsonNode node = new JsonArray();
    assertTrue(node.isArray());
    assertDoesNotThrow(node::getAsArray);

    assertFalse(node.isObject());
    JsonTransformException getAsObjEx = assertThrows(JsonTransformException.class, node::getAsObject);
    assertEquals("Cannot transform JsonArray to JsonObject", getAsObjEx.getMessage());

    assertFalse(node.isPrimitive());
    JsonTransformException getAsPrimEx = assertThrows(JsonTransformException.class, node::getAsPrimitive);
    assertEquals("Cannot transform JsonArray to JsonPrimitive", getAsPrimEx.getMessage());

    assertFalse(node.isNull());
    JsonTransformException getAsNullEx = assertThrows(JsonTransformException.class, node::getAsNull);
    assertEquals("Cannot transform JsonArray to JsonNull", getAsNullEx.getMessage());

    assertFalse(node.isString());
    JsonTransformException getAsStrEx = assertThrows(JsonTransformException.class, node::getAsString);
    assertEquals("Cannot transform JsonArray to String", getAsStrEx.getMessage());

    assertFalse(node.isChar());
    JsonTransformException getAsCharEx = assertThrows(JsonTransformException.class, node::getAsChar);
    assertEquals("Cannot transform JsonArray to char", getAsCharEx.getMessage());

    assertFalse(node.isNumber());
    JsonTransformException getAsNumberEx = assertThrows(JsonTransformException.class, node::getAsNumber);
    assertEquals("Cannot transform JsonArray to Number", getAsNumberEx.getMessage());

    assertFalse(node.isInt());
    JsonTransformException getAsIntEx = assertThrows(JsonTransformException.class, node::getAsInt);
    assertEquals("Cannot transform JsonArray to int", getAsIntEx.getMessage());

    assertFalse(node.isLong());
    JsonTransformException getAsLongEx = assertThrows(JsonTransformException.class, node::getAsLong);
    assertEquals("Cannot transform JsonArray to long", getAsLongEx.getMessage());

    assertFalse(node.isShort());
    JsonTransformException getAsShortEx = assertThrows(JsonTransformException.class, node::getAsShort);
    assertEquals("Cannot transform JsonArray to short", getAsShortEx.getMessage());

    assertFalse(node.isByte());
    JsonTransformException getAsByteEx = assertThrows(JsonTransformException.class, node::getAsByte);
    assertEquals("Cannot transform JsonArray to byte", getAsByteEx.getMessage());

    assertFalse(node.isDouble());
    JsonTransformException getAsDblEx = assertThrows(JsonTransformException.class, node::getAsDouble);
    assertEquals("Cannot transform JsonArray to double", getAsDblEx.getMessage());

    assertFalse(node.isFloat());
    JsonTransformException getAsFloatEx = assertThrows(JsonTransformException.class, node::getAsFloat);
    assertEquals("Cannot transform JsonArray to float", getAsFloatEx.getMessage());

    assertFalse(node.isBool());
    JsonTransformException getAsBoolEx = assertThrows(JsonTransformException.class, node::getAsBool);
    assertEquals("Cannot transform JsonArray to boolean", getAsBoolEx.getMessage());
  }
}
