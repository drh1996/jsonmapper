package com.drh.json.node;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.exception.JsonTransformException;
import com.drh.json.parser.JsonParser;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Json Object Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonObjectTest {

  @Test
  public void test_has() {
    JsonObject obj = new JsonObject();
    obj.add("test", 1);

    assertTrue(obj.has("test"));
    assertFalse(obj.has("abc"));
  }

  @Test
  public void test_get() {
    JsonObject obj = new JsonObject();
    obj.add("test", 1);

    JsonNode result = assertDoesNotThrow(() -> obj.get("test"));
    assertEquals(1, result.getAsInt());

    NoSuchElementException notFoundEx = assertThrows(NoSuchElementException.class,
        () -> obj.get("abc"));

    assertEquals("No JsonNode found for key: 'abc'", notFoundEx.getMessage());
  }

  @Test
  public void test_get_object() {
    JsonObject obj = new JsonObject().add("one", new JsonObject());

    assertTrue(obj.isObject("one"));
    JsonObject elem = obj.getObject("one");
    assertEquals(new JsonObject(), elem);
  }

  @Test
  public void test_get_array() {
    JsonObject obj = new JsonObject().add("one", new JsonArray());

    assertTrue(obj.isArray("one"));
    JsonArray elem = obj.getArray("one");
    assertEquals(new JsonArray(), elem);
  }

  @Test
  public void test_get_primitive() {
    JsonObject obj = new JsonObject().add("one", new JsonPrimitive(1));

    assertTrue(obj.isPrimitive("one"));
    JsonPrimitive elem = obj.getPrimitive("one");
    assertEquals(new JsonPrimitive(1), elem);
  }

  @Test
  public void test_get_string() {
    JsonObject obj = new JsonObject().add("one", "test");

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isString("one"));
    String elem = obj.getString("one");
    assertEquals("test", elem);
  }

  @Test
  public void test_get_string_default() {
    JsonObject obj = new JsonObject().add("one", "test");

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isString("one"));
    String elem = obj.getString("one", "default");
    assertEquals("test", elem);


    String defaultVal = obj.getString("1", "default");
    assertEquals("default", defaultVal);
  }

  @Test
  public void test_get_char() {
    JsonObject obj = new JsonObject().add("one", "c");

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isChar("one"));
    char elem = obj.getChar("one");
    assertEquals('c', elem);
  }

  @Test
  public void test_get_char_default() {
    JsonObject obj = new JsonObject().add("one", "c");

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isChar("one"));
    char elem = obj.getChar("one", 'd');
    assertEquals('c', elem);

    char defaultVal = obj.getChar("1", 'd');
    assertEquals('d', defaultVal);
  }

  @Test
  public void test_get_number() {
    JsonObject obj = new JsonObject().add("one", 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isNumber("one"));
    Number elem = obj.getNumber("one");
    assertEquals(1, elem);
  }

  @Test
  public void test_get_number_default() {
    JsonObject obj = new JsonObject().add("one", 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isNumber("one"));
    Number elem = obj.getNumber("one", 4);
    assertEquals(1, elem);

    Number defaultVal = obj.getNumber("1", 4);
    assertEquals(4, defaultVal);
  }

  @Test
  public void test_get_int() {
    JsonObject obj = new JsonObject().add("one", 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isInt("one"));
    int elem = obj.getInt("one");
    assertEquals(1, elem);
  }

  @Test
  public void test_get_int_default() {
    JsonObject obj = new JsonObject().add("one", 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isInt("one"));
    int elem = obj.getInt("one", 4);
    assertEquals(1, elem);

    int defaultVal = obj.getInt("1", 4);
    assertEquals(4, defaultVal);
  }

  @Test
  public void test_get_long() {
    JsonObject obj = new JsonObject().add("one", 1L);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isLong("one"));
    long elem = obj.getLong("one");
    assertEquals(1L, elem);
  }

  @Test
  public void test_get_long_default() {
    JsonObject obj = new JsonObject().add("one", 1L);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isLong("one"));
    long elem = obj.getLong("one", 4L);
    assertEquals(1L, elem);

    long defaultVal = obj.getLong("1", 4L);
    assertEquals(4L, defaultVal);
  }

  @Test
  public void test_get_short() {
    JsonObject obj = new JsonObject().add("one", (short) 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isShort("one"));
    short elem = obj.getShort("one");
    assertEquals((short) 1, elem);
  }

  @Test
  public void test_get_short_default() {
    JsonObject obj = new JsonObject().add("one", (short) 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isShort("one"));
    short elem = obj.getShort("one", (short) 4);
    assertEquals((short) 1, elem);

    short defaultVal = obj.getShort("1", (short) 4);
    assertEquals((short) 4, defaultVal);
  }

  @Test
  public void test_get_byte() {
    JsonObject obj = new JsonObject().add("one", (byte) 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isByte("one"));
    byte elem = obj.getByte("one");
    assertEquals((byte) 1, elem);
  }

  @Test
  public void test_get_byte_default() {
    JsonObject obj = new JsonObject().add("one", (byte) 1);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isByte("one"));
    byte elem = obj.getByte("one", (byte) 4);
    assertEquals((byte) 1, elem);

    byte defaultVal = obj.getByte("1", (byte) 4);
    assertEquals((byte) 4, defaultVal);
  }

  @Test
  public void test_get_double() {
    JsonObject obj = new JsonObject().add("one", 1D);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isDouble("one"));
    double elem = obj.getDouble("one");
    assertEquals(1D, elem);
  }

  @Test
  public void test_get_double_default() {
    JsonObject obj = new JsonObject().add("one", 1D);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isDouble("one"));
    double elem = obj.getDouble("one", 4D);
    assertEquals(1D, elem);

    double defaultVal = obj.getDouble("1", 4D);
    assertEquals(4D, defaultVal);
  }

  @Test
  public void test_get_float() {
    JsonObject obj = new JsonObject().add("one", 1F);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isFloat("one"));
    float elem = obj.getFloat("one");
    assertEquals(1F, elem);
  }

  @Test
  public void test_get_float_default() {
    JsonObject obj = new JsonObject().add("one", 1F);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isFloat("one"));
    float elem = obj.getFloat("one", 4F);
    assertEquals(1F, elem);

    float defaultVal = obj.getFloat("1", 4F);
    assertEquals(4F, defaultVal);
  }

  @Test
  public void test_get_bool() {
    JsonObject obj = new JsonObject().add("one", true);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isBool("one"));
    boolean elem = obj.getBool("one");
    assertTrue(elem);
  }

  @Test
  public void test_get_bool_default() {
    JsonObject obj = new JsonObject().add("one", true);

    assertTrue(obj.isPrimitive("one"));
    assertTrue(obj.isBool("one"));
    boolean elem = obj.getBool("one", false);
    assertTrue(elem);

    boolean defaultVal = obj.getBool("1", false);
    assertFalse(defaultVal);
  }

  @Test
  public void test_get_null() {
    JsonObject obj = new JsonObject().add("one", new JsonNull());

    assertTrue(obj.isNull("one"));
    JsonNull elem = obj.getNull("one");
    assertEquals(new JsonNull(), elem);
  }

  @Test
  public void test_add_property() {
    JsonObject obj = new JsonObject();
    obj.add("testInt", 1);
    obj.add("testStr", "one");
    obj.add("testStrNull", (String) null);
    obj.add("testBool", true);
    obj.add("testNull", new JsonNull());

    assertEquals(5, obj.entrySet().size());
  }

  @Test
  public void test_add_property_duplicate() {
    JsonObject obj = new JsonObject();
    obj.add("test", 1);

    IllegalArgumentException exInt = assertThrows(IllegalArgumentException.class,
        () -> obj.add("test", 2));

    assertEquals("The key 'test' already has a value", exInt.getMessage());

    IllegalArgumentException exNode = assertThrows(IllegalArgumentException.class,
        () -> obj.add("test", new JsonObject()));

    assertEquals("The key 'test' already has a value", exNode.getMessage());
  }

  @Test
  public void test_for_each() {
    JsonObject a1 = new JsonObject()
        .add("one", 1)
        .add("two", 2)
        .add("three", 3);

    JsonObject a2 = new JsonObject()
        .add("one", 1)
        .add("two", 2)
        .add("three", 3);

    a1.forEach((key, value) -> {
      assertTrue(a2.has(key));
      assertEquals(value, a2.get(key));
    });
  }

  @Test
  public void test_equals() {
    JsonObject a1 = new JsonObject()
        .add("one", 1)
        .add("two", 2)
        .add("three", 3);

    JsonObject a2 = new JsonObject()
        .add("one", 1)
        .add("two", 2)
        .add("three", 3);

    assertEquals(a1, a1);
    assertEquals(a1, a2);
    assertEquals(a2, a1);

    assertNotEquals(a1, new JsonArray());
    assertNotEquals(a1, new JsonObject().add("one", 1));
  }

  @Test
  public void test_to_string() {
    JsonObject object = new JsonObject().add("one", 1).add("two", 2);
    assertEquals("{\"one\":1,\"two\":2}", object.toString());
  }


  @Test
  public void test_is_object() {
    JsonNode node = new JsonObject();
    assertTrue(node.isObject());
    assertDoesNotThrow(node::getAsObject);

    assertFalse(node.isArray());
    JsonTransformException getAsArrEx = assertThrows(JsonTransformException.class, node::getAsArray);
    assertEquals("Cannot transform JsonObject to JsonArray", getAsArrEx.getMessage());

    assertFalse(node.isPrimitive());
    JsonTransformException getAsPrimEx = assertThrows(JsonTransformException.class, node::getAsPrimitive);
    assertEquals("Cannot transform JsonObject to JsonPrimitive", getAsPrimEx.getMessage());

    assertFalse(node.isNull());
    JsonTransformException getAsNullEx = assertThrows(JsonTransformException.class, node::getAsNull);
    assertEquals("Cannot transform JsonObject to JsonNull", getAsNullEx.getMessage());

    assertFalse(node.isString());
    JsonTransformException getAsStrEx = assertThrows(JsonTransformException.class, node::getAsString);
    assertEquals("Cannot transform JsonObject to String", getAsStrEx.getMessage());

    assertFalse(node.isChar());
    JsonTransformException getAsCharEx = assertThrows(JsonTransformException.class, node::getAsChar);
    assertEquals("Cannot transform JsonObject to char", getAsCharEx.getMessage());

    assertFalse(node.isInt());
    JsonTransformException getAsIntEx = assertThrows(JsonTransformException.class, node::getAsInt);
    assertEquals("Cannot transform JsonObject to int", getAsIntEx.getMessage());

    assertFalse(node.isLong());
    JsonTransformException getAsLongEx = assertThrows(JsonTransformException.class, node::getAsLong);
    assertEquals("Cannot transform JsonObject to long", getAsLongEx.getMessage());

    assertFalse(node.isShort());
    JsonTransformException getAsShortEx = assertThrows(JsonTransformException.class, node::getAsShort);
    assertEquals("Cannot transform JsonObject to short", getAsShortEx.getMessage());

    assertFalse(node.isByte());
    JsonTransformException getAsByteEx = assertThrows(JsonTransformException.class, node::getAsByte);
    assertEquals("Cannot transform JsonObject to byte", getAsByteEx.getMessage());

    assertFalse(node.isDouble());
    JsonTransformException getAsDblEx = assertThrows(JsonTransformException.class, node::getAsDouble);
    assertEquals("Cannot transform JsonObject to double", getAsDblEx.getMessage());

    assertFalse(node.isFloat());
    JsonTransformException getAsFloatEx = assertThrows(JsonTransformException.class, node::getAsFloat);
    assertEquals("Cannot transform JsonObject to float", getAsFloatEx.getMessage());

    assertFalse(node.isBool());
    JsonTransformException getAsBoolEx = assertThrows(JsonTransformException.class, node::getAsBool);
    assertEquals("Cannot transform JsonObject to boolean", getAsBoolEx.getMessage());
  }
}
