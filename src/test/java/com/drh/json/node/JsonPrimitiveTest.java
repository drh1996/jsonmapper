package com.drh.json.node;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.exception.JsonTransformException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Json Primitive Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonPrimitiveTest {

  @Test
  public void test_set_value() {
    JsonPrimitive prim = new JsonPrimitive("one");
    prim.setValue(1);

    assertTrue(prim.isInt());
    assertEquals(1, prim.getAsInt());
  }

  @Test
  public void test_get_as_string() {
    JsonPrimitive prim = new JsonPrimitive("one");
    assertEquals("one", prim.getAsString());
  }

  @Test
  public void test_get_as_char() {
    JsonPrimitive prim = new JsonPrimitive("1");
    assertEquals('1', prim.getAsChar());
  }

  @Test
  public void test_get_as_char_len_gt_one() {
    JsonPrimitive prim = new JsonPrimitive("one");
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        prim::getAsChar);

    assertEquals("Failed to convert value \"one\" to a Character", ex.getMessage());
  }

  @Test
  public void test_get_as_number() {
    JsonPrimitive prim = new JsonPrimitive(1);
    assertEquals(1, prim.getAsNumber());
  }

  @Test
  public void test_get_as_int() {
    JsonPrimitive prim = new JsonPrimitive(1);
    assertEquals(1, prim.getAsInt());
  }

  @Test
  public void test_get_as_long() {
    JsonPrimitive prim = new JsonPrimitive(1);
    assertEquals(1, prim.getAsLong());
  }

  @Test
  public void test_get_as_short() {
    JsonPrimitive prim = new JsonPrimitive(1);
    assertEquals(1, prim.getAsShort());
  }

  @Test
  public void test_get_as_byte() {
    JsonPrimitive prim = new JsonPrimitive(1);
    assertEquals(1, prim.getAsByte());
  }

  @Test
  public void test_get_as_double() {
    JsonPrimitive prim = new JsonPrimitive(1D);
    assertEquals(1D, prim.getAsDouble());
  }

  @Test
  public void test_get_as_float() {
    JsonPrimitive prim = new JsonPrimitive(1D);
    assertEquals(1F, prim.getAsFloat());
  }

  @Test
  public void test_get_as_bool() {
    JsonPrimitive prim = new JsonPrimitive(true);
    assertTrue(prim.getAsBool());
  }

  @Test
  public void test_invalid_value() {
    IllegalArgumentException invalidVal =  assertThrows(IllegalArgumentException.class,
        () -> new JsonPrimitive(new Object()));

    assertEquals("JsonPrimitive cannot be Object", invalidVal.getMessage());

    IllegalArgumentException nullVal =  assertThrows(IllegalArgumentException.class,
        () -> new JsonPrimitive(null));

    assertEquals("JsonPrimitive cannot be null", nullVal.getMessage());
  }

  @Test
  public void test_equals() {
    JsonPrimitive a1 = new JsonPrimitive(1);
    JsonPrimitive a2 = new JsonPrimitive(1);

    assertEquals(a1, a1);
    assertEquals(a1, a2);
    assertEquals(a2, a1);

    assertNotEquals(a1, new JsonPrimitive("a"));
    assertNotEquals(a1, new JsonObject());
  }

  @Test
  public void test_to_string() {
    JsonPrimitive primInt = new JsonPrimitive(1);
    assertEquals("1", primInt.toString());

    JsonPrimitive primString = new JsonPrimitive("1");
    assertEquals("\"1\"", primString.toString());
  }

  @Test
  public void test_is_primitive() {
    JsonNode node = new JsonPrimitive(false);
    assertTrue(node.isPrimitive());
    assertDoesNotThrow(node::getAsPrimitive);
    assertTrue(node.isBool());
    assertDoesNotThrow(node::getAsBool);

    assertFalse(node.isObject());
    JsonTransformException getAsObjEx = assertThrows(JsonTransformException.class, node::getAsObject);
    assertEquals("Cannot transform JsonPrimitive to JsonObject", getAsObjEx.getMessage());

    assertFalse(node.isArray());
    JsonTransformException getAsArrEx = assertThrows(JsonTransformException.class, node::getAsArray);
    assertEquals("Cannot transform JsonPrimitive to JsonArray", getAsArrEx.getMessage());

    assertFalse(node.isNull());
    JsonTransformException getAsNullEx = assertThrows(JsonTransformException.class, node::getAsNull);
    assertEquals("Cannot transform JsonPrimitive to JsonNull", getAsNullEx.getMessage());

    assertFalse(node.isString());
    JsonTransformException getAsStrEx = assertThrows(JsonTransformException.class, node::getAsString);
    assertEquals("Cannot transform JsonPrimitive->BOOLEAN to String", getAsStrEx.getMessage());

    assertFalse(node.isChar());
    JsonTransformException getAsCharEx = assertThrows(JsonTransformException.class, node::getAsChar);
    assertEquals("Cannot transform JsonPrimitive to char", getAsCharEx.getMessage());

    assertFalse(node.isNumber());
    JsonTransformException getAsNumberEx = assertThrows(JsonTransformException.class, node::getAsNumber);
    assertEquals("Cannot transform JsonPrimitive to Number", getAsNumberEx.getMessage());

    assertFalse(node.isInt());
    JsonTransformException getAsIntEx = assertThrows(JsonTransformException.class, node::getAsInt);
    assertEquals("Cannot transform JsonPrimitive to int", getAsIntEx.getMessage());

    assertFalse(node.isLong());
    JsonTransformException getAsLongEx = assertThrows(JsonTransformException.class, node::getAsLong);
    assertEquals("Cannot transform JsonPrimitive to long", getAsLongEx.getMessage());

    assertFalse(node.isShort());
    JsonTransformException getAsShortEx = assertThrows(JsonTransformException.class, node::getAsShort);
    assertEquals("Cannot transform JsonPrimitive to short", getAsShortEx.getMessage());

    assertFalse(node.isByte());
    JsonTransformException getAsByteEx = assertThrows(JsonTransformException.class, node::getAsByte);
    assertEquals("Cannot transform JsonPrimitive to byte", getAsByteEx.getMessage());

    assertFalse(node.isDouble());
    JsonTransformException getAsDblEx = assertThrows(JsonTransformException.class, node::getAsDouble);
    assertEquals("Cannot transform JsonPrimitive to double", getAsDblEx.getMessage());

    assertFalse(node.isFloat());
    JsonTransformException getAsFloatEx = assertThrows(JsonTransformException.class, node::getAsFloat);
    assertEquals("Cannot transform JsonPrimitive to float", getAsFloatEx.getMessage());
  }
}
