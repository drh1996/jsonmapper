package com.drh.json.node;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.exception.JsonTransformException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Json Null Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonNullTest {

  @Test
  public void test_to_string() {
    assertEquals("null", new JsonNull().toString());
  }

  @Test
  public void test_is_null() {
    JsonNode node = new JsonNull();
    assertTrue(node.isNull());
    assertDoesNotThrow(node::getAsNull);

    assertFalse(node.isObject());
    JsonTransformException getAsObjEx = assertThrows(JsonTransformException.class, node::getAsObject);
    assertEquals("Cannot transform JsonNull to JsonObject", getAsObjEx.getMessage());

    assertFalse(node.isArray());
    JsonTransformException getAsArrEx = assertThrows(JsonTransformException.class, node::getAsArray);
    assertEquals("Cannot transform JsonNull to JsonArray", getAsArrEx.getMessage());

    assertFalse(node.isPrimitive());
    JsonTransformException getAsPrimEx = assertThrows(JsonTransformException.class, node::getAsPrimitive);
    assertEquals("Cannot transform JsonNull to JsonPrimitive", getAsPrimEx.getMessage());

    assertFalse(node.isString());
    JsonTransformException getAsStrEx = assertThrows(JsonTransformException.class, node::getAsString);
    assertEquals("Cannot transform JsonNull to String", getAsStrEx.getMessage());

    assertFalse(node.isChar());
    JsonTransformException getAsCharEx = assertThrows(JsonTransformException.class, node::getAsChar);
    assertEquals("Cannot transform JsonNull to char", getAsCharEx.getMessage());

    assertFalse(node.isInt());
    JsonTransformException getAsIntEx = assertThrows(JsonTransformException.class, node::getAsInt);
    assertEquals("Cannot transform JsonNull to int", getAsIntEx.getMessage());

    assertFalse(node.isLong());
    JsonTransformException getAsLongEx = assertThrows(JsonTransformException.class, node::getAsLong);
    assertEquals("Cannot transform JsonNull to long", getAsLongEx.getMessage());

    assertFalse(node.isShort());
    JsonTransformException getAsShortEx = assertThrows(JsonTransformException.class, node::getAsShort);
    assertEquals("Cannot transform JsonNull to short", getAsShortEx.getMessage());

    assertFalse(node.isByte());
    JsonTransformException getAsByteEx = assertThrows(JsonTransformException.class, node::getAsByte);
    assertEquals("Cannot transform JsonNull to byte", getAsByteEx.getMessage());

    assertFalse(node.isDouble());
    JsonTransformException getAsDblEx = assertThrows(JsonTransformException.class, node::getAsDouble);
    assertEquals("Cannot transform JsonNull to double", getAsDblEx.getMessage());

    assertFalse(node.isFloat());
    JsonTransformException getAsFloatEx = assertThrows(JsonTransformException.class, node::getAsFloat);
    assertEquals("Cannot transform JsonNull to float", getAsFloatEx.getMessage());

    assertFalse(node.isBool());
    JsonTransformException getAsBoolEx = assertThrows(JsonTransformException.class, node::getAsBool);
    assertEquals("Cannot transform JsonNull to boolean", getAsBoolEx.getMessage());
  }
}
