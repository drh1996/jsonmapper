package com.drh.json.node;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Json Node Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonNodeTest {

  @Test
  public void test_is_type() {
    JsonNode arr = new JsonArray();
    assertTrue(arr.isType(JsonType.ARRAY));
    assertFalse(arr.isType(JsonType.OBJECT));
    assertFalse(arr.isType(JsonType.NUMBER));
    assertFalse(arr.isType(JsonType.STRING));
    assertFalse(arr.isType(JsonType.BOOLEAN));
    assertFalse(arr.isType(JsonType.NULL));
  }
}
