package com.drh.json;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.styling.JsonStyling;
import com.drh.json.support.ReflectionUtils;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Json Mapper Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonMapperTest {

  @Mock
  private InternalJsonSerializer internalSerializer;

  @Mock
  private InternalJsonDeserializer internalDeserializer;

  private JsonMapper jsonMapper;

  @BeforeEach
  public void beforeEach() throws NoSuchFieldException, IllegalAccessException {
    jsonMapper = new JsonMapper();
    ReflectionUtils.setFieldValue(JsonMapper.class, "internalJsonSerializer", jsonMapper, internalSerializer);
    ReflectionUtils.setFieldValue(JsonMapper.class, "internalJsonDeserializer", jsonMapper, internalDeserializer);
  }

  @Test
  public void test_to_json() {

    when(internalSerializer.serialize(any())).thenReturn("[1,2,3,4]");

    assertDoesNotThrow(() -> jsonMapper.toJson(Arrays.asList(1,2,3,4)));

    verify(internalSerializer).serialize(any());

  }

  @Test
  public void test_from_json() {
    when(internalDeserializer.deserialize(anyString(), eq(Object.class))).thenReturn(new Object());

    assertDoesNotThrow(() -> jsonMapper.fromJson("{}", Object.class));

    verify(internalDeserializer).deserialize(anyString(),eq(Object.class));
  }

  @Test
  public void test_add_serializer() {
    doNothing().when(internalSerializer).addSerializer(any(), any());

    assertDoesNotThrow(() -> jsonMapper.addSerializer(
        Object.class, (obj, gen, serializerManager) -> {}));

    verify(internalSerializer).addSerializer(any(), any());
  }

  @Test
  public void test_add_deserializer() {
    doNothing().when(internalDeserializer).addDeserializer(any(), any());

    assertDoesNotThrow(() -> jsonMapper.addDeserializer(
        Object.class, (node, manager) -> new Object()));

    verify(internalDeserializer).addDeserializer(any(), any());
  }

  @Test
  public void test_builder() {
    JsonMapper result = assertDoesNotThrow(() -> JsonMapper.builder()
        .styling(new JsonStyling(0, 0, false))
        .build());

    assertNotNull(result);
  }

  @Test
  public void test_builder_default_styling() {
    JsonMapper result = assertDoesNotThrow(() -> JsonMapper.builder().build());

    assertNotNull(result);
  }

  @Test
  public void test_builder_pretty() {
    JsonMapper result = assertDoesNotThrow(() -> JsonMapper.builder()
        .prettyStyling()
        .build());

    assertNotNull(result);
  }
}
