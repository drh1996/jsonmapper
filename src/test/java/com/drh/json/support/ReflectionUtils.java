package com.drh.json.support;

import java.lang.reflect.Field;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReflectionUtils {

  public static void setFieldValue(Class<?> cls, String fieldName, Object src, Object value)
      throws NoSuchFieldException, IllegalAccessException {
    Field f = cls.getDeclaredField(fieldName);
    f.setAccessible(true);
    f.set(src, value);
    f.setAccessible(false);
  }


  public <T> T getField(Class<T> clazz, String fieldName, Object src, Class<T> returnClazz)
      throws NoSuchFieldException, IllegalAccessException {
    return returnClazz.cast(clazz.getField(fieldName).get(src));
  }
}
