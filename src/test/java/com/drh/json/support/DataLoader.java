package com.drh.json.support;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DataLoader {

  private static final Map<String, String> FILE_CACHE = new HashMap<>();

  public static String getData(String filepath) {
    if (FILE_CACHE.containsKey(filepath)) {
      return FILE_CACHE.get(filepath);
    }

    String data = loadData(filepath);
    FILE_CACHE.put(filepath, data);
    return data;
  }

  private static String loadData(String filepath) {
    try (InputStream is = DataLoader.class.getClassLoader().getResourceAsStream(filepath)) {
      assert is != null;
      return readInputStream(is);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String readInputStream(InputStream is) throws IOException {
    int readBytes;
    byte[] buffer = new byte[1024];
    StringBuilder sb = new StringBuilder();
    while((readBytes = is.read(buffer, 0, 1024)) != -1) {
      sb.append(new String(buffer, 0, readBytes, StandardCharsets.UTF_8));
    }
    return sb.toString();
  }
}
