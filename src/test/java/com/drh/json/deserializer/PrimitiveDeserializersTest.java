package com.drh.json.deserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonNull;
import com.drh.json.node.JsonPrimitive;
import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@DisplayName("Primitive Deserializers Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PrimitiveDeserializersTest {

  @Mock
  private DeserializerManager manager;

  @Test
  public void test_get_primitive_deserializers() {
    Map<Class<?>, JsonDeserializer<?>> deserializerMap = PrimitiveDeserializers.getPrimitiveDeserializers();

    assertEquals(17, deserializerMap.size());
  }

  @Test
  public void test_string_deserializer() {
    JsonDeserializer<String> deserializer = new PrimitiveDeserializers.StringDeserializer();

    String actual = deserializer.deserialize(new JsonPrimitive("test"), manager);
    assertEquals("test", actual);

    String actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_char_deserializer() {
    JsonDeserializer<Character> deserializer = new PrimitiveDeserializers.CharacterDeserializer();

    char actual = deserializer.deserialize(new JsonPrimitive("1"), manager);
    assertEquals('1', actual);

    Character actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_boolean_deserializer() {
    JsonDeserializer<Boolean> deserializer = new PrimitiveDeserializers.BooleanDeserializer();

    boolean actual = deserializer.deserialize(new JsonPrimitive(true), manager);
    assertTrue(actual);

    Boolean actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_int_deserializer() {
    JsonDeserializer<Integer> deserializer = new PrimitiveDeserializers.IntegerDeserializer();

    int actual = deserializer.deserialize(new JsonPrimitive(1), manager);
    assertEquals(1, actual);

    int actualStr = deserializer.deserialize(new JsonPrimitive("1"), manager);
    assertEquals(1, actualStr);

    Integer actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_short_deserializer() {
    JsonDeserializer<Short> deserializer = new PrimitiveDeserializers.ShortDeserializer();

    short actual = deserializer.deserialize(new JsonPrimitive(1), manager);
    assertEquals(1, actual);

    short actualStr = deserializer.deserialize(new JsonPrimitive("1"), manager);
    assertEquals(1, actualStr);

    Short actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_long_deserializer() {
    JsonDeserializer<Long> deserializer = new PrimitiveDeserializers.LongDeserializer();

    long actual = deserializer.deserialize(new JsonPrimitive(1), manager);
    assertEquals(1, actual);

    long actualStr = deserializer.deserialize(new JsonPrimitive("1"), manager);
    assertEquals(1, actualStr);

    Long actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_byte_deserializer() {
    JsonDeserializer<Byte> deserializer = new PrimitiveDeserializers.ByteDeserializer();

    byte actual = deserializer.deserialize(new JsonPrimitive(1), manager);
    assertEquals(1, actual);

    byte actualStr = deserializer.deserialize(new JsonPrimitive("1"), manager);
    assertEquals(1, actualStr);

    Byte actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_float_deserializer() {
    JsonDeserializer<Float> deserializer = new PrimitiveDeserializers.FloatDeserializer();

    float actual = deserializer.deserialize(new JsonPrimitive(1.21F), manager);
    assertEquals(1.21F, actual);

    float actualStr = deserializer.deserialize(new JsonPrimitive("1.21"), manager);
    assertEquals(1.21F, actualStr);

    Float actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }

  @Test
  public void test_double_deserializer() {
    JsonDeserializer<Double> deserializer = new PrimitiveDeserializers.DoubleDeserializer();

    double actual = deserializer.deserialize(new JsonPrimitive(1.21D), manager);
    assertEquals(1.21D, actual);

    double actualStr = deserializer.deserialize(new JsonPrimitive("1.21"), manager);
    assertEquals(1.21D, actualStr);

    Double actualNull = deserializer.deserialize(new JsonNull(), manager);
    assertNull(actualNull);
  }
}
