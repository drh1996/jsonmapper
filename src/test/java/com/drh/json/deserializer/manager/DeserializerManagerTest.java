package com.drh.json.deserializer.manager;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.annotation.JsonDeserialize;
import com.drh.json.deserializer.ContextualDeserializer;
import com.drh.json.deserializer.JsonDeserializer;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.node.JsonNull;
import com.drh.json.node.JsonObject;
import com.drh.json.node.JsonPrimitive;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.Property;
import com.drh.json.support.ReflectionUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Deserializer Manager Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class DeserializerManagerTest {

  @Mock
  private Map<Class<?>, JsonDeserializer> deserializerMap;

  @Mock
  private Map<Class<?>, JsonDeserializer> annotatedDeserializerMap;

  @Mock
  private MockContextualJsonDeserializer mockDeserializer;

  private DeserializerManager manager;

  @BeforeEach
  public void beforeEach() throws NoSuchFieldException, IllegalAccessException {
    manager = new DeserializerManager(deserializerMap);
    ReflectionUtils.setFieldValue(DeserializerManager.class, "annotatedDeserializerMap", manager, annotatedDeserializerMap);
    ReflectionUtils.setFieldValue(DeserializerManager.class, "defaultArrayDeserializer", manager, mockDeserializer);
    ReflectionUtils.setFieldValue(DeserializerManager.class, "defaultEnumDeserializer", manager, mockDeserializer);
    ReflectionUtils.setFieldValue(DeserializerManager.class, "defaultCollectionDeserializer", manager, mockDeserializer);
    ReflectionUtils.setFieldValue(DeserializerManager.class, "defaultMapDeserializer", manager, mockDeserializer);
    ReflectionUtils.setFieldValue(DeserializerManager.class, "defaultObjectDeserializer", manager, mockDeserializer);
  }

  @Test
  public void test_add_deserializer() {
    when(deserializerMap.put(any(Class.class), any(JsonDeserializer.class)))
        .thenReturn(mockDeserializer);

    assertDoesNotThrow(() -> manager.addDeserializer(Object.class, (node, manager) -> null));

    verify(deserializerMap).put(any(Class.class), any(JsonDeserializer.class));
  }

  @Test
  public void test_deserializer_annotated() {
    DummyJsonDeserializer dummyDeserializer = mock(DummyJsonDeserializer.class);

    when(annotatedDeserializerMap.containsKey(eq(DummyJsonDeserializer.class)))
        .thenReturn(false);
    when(annotatedDeserializerMap.put(eq(DummyJsonDeserializer.class), any(JsonDeserializer.class)))
        .thenReturn(dummyDeserializer);

    assertDoesNotThrow(() -> manager.deserialize(TestAnnotatedDeserializerCls.class, new JsonObject()));

    verify(annotatedDeserializerMap).containsKey(eq(DummyJsonDeserializer.class));
    verify(annotatedDeserializerMap).put(eq(DummyJsonDeserializer.class), any(JsonDeserializer.class));
  }

  @Test
  public void test_deserializer_annotated_cached() {
    DummyJsonDeserializer dummyDeserializer = mock(DummyJsonDeserializer.class);

    when(annotatedDeserializerMap.containsKey(eq(DummyJsonDeserializer.class)))
        .thenReturn(true);
    when(annotatedDeserializerMap.get(eq(DummyJsonDeserializer.class)))
        .thenReturn(dummyDeserializer);
    when(dummyDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new TestAnnotatedDeserializerCls());

    assertDoesNotThrow(() -> manager.deserialize(TestAnnotatedDeserializerCls.class, new JsonObject()));

    verify(annotatedDeserializerMap).containsKey(eq(DummyJsonDeserializer.class));
    verify(annotatedDeserializerMap).get(eq(DummyJsonDeserializer.class));
    verify(dummyDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  @Test
  public void test_deserializer_annotated_contextual() {
    DummyContextualJsonDeserializer dummyDeserializer = mock(DummyContextualJsonDeserializer.class);

    when(annotatedDeserializerMap.containsKey(eq(DummyContextualJsonDeserializer.class)))
        .thenReturn(true);
    when(annotatedDeserializerMap.get(eq(DummyContextualJsonDeserializer.class)))
        .thenReturn(dummyDeserializer);
    when(dummyDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(dummyDeserializer);
    when(dummyDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new TestAnnotatedDeserializerClsCtx());

    assertDoesNotThrow(
        () -> manager.deserialize(TestAnnotatedDeserializerClsCtx.class, new JsonObject(), new ClassProperty(TestAnnotatedDeserializerClsCtx.class)));

    verify(annotatedDeserializerMap).containsKey(eq(DummyContextualJsonDeserializer.class));
    verify(annotatedDeserializerMap).get(eq(DummyContextualJsonDeserializer.class));
    verify(dummyDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(dummyDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  @Test
  public void test_deserialize_annotated_contextual_invalid_constructor() {
    when(annotatedDeserializerMap.containsKey(eq(DummyPrivateContextualJsonDeserializer.class)))
        .thenReturn(false);

    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> manager.deserialize(TestAnnotatedDeserializerClsCtx2.class, new JsonObject(), new ClassProperty(TestAnnotatedDeserializerClsCtx2.class)));

    verify(annotatedDeserializerMap).containsKey(eq(DummyPrivateContextualJsonDeserializer.class));

    assertEquals("Failed to deserialize using annotated deserializer: com.drh.json.deserializer.manager.DeserializerManagerTest$DummyPrivateContextualJsonDeserializer", ex.getMessage());
  }

  @Test
  public void test_deserializer_custom() {
    DummyJsonDeserializer dummyDeserializer = mock(DummyJsonDeserializer.class);

    when(deserializerMap.containsKey(eq(TestDeserializerCls.class)))
        .thenReturn(true);
    when(deserializerMap.get(eq(TestDeserializerCls.class)))
        .thenReturn(dummyDeserializer);
    when(dummyDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new TestDeserializerCls());

    TestDeserializerCls result = assertDoesNotThrow(
        () -> manager.deserialize(TestDeserializerCls.class, new JsonObject(), new ClassProperty(TestDeserializerCls.class)));

    verify(deserializerMap).containsKey(eq(TestDeserializerCls.class));
    verify(deserializerMap).get(eq(TestDeserializerCls.class));
    verify(dummyDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));

    assertNotNull(result);
  }

  @Test
  public void test_deserializer_custom_contextual() {
    DummyContextualJsonDeserializer dummyDeserializer = mock(DummyContextualJsonDeserializer.class);

    when(deserializerMap.containsKey(eq(TestDeserializerClsCtx.class)))
        .thenReturn(true);
    when(deserializerMap.get(eq(TestDeserializerClsCtx.class)))
        .thenReturn(dummyDeserializer);
    when(dummyDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(dummyDeserializer);
    when(dummyDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new TestDeserializerClsCtx());

    assertDoesNotThrow(() -> manager.deserialize(TestDeserializerClsCtx.class, new JsonObject(), new ClassProperty(TestDeserializerClsCtx.class)));

    verify(deserializerMap).containsKey(eq(TestDeserializerClsCtx.class));
    verify(deserializerMap).get(eq(TestDeserializerClsCtx.class));
    verify(dummyDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(dummyDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  @Test
  public void test_deserializer_null() {
    when(deserializerMap.containsKey(eq(Object.class)))
        .thenReturn(false);

    Object result = assertDoesNotThrow(
        () -> manager.deserialize(Object.class, new JsonNull(), new ClassProperty(TestDeserializerCls.class)));

    verify(deserializerMap).containsKey(eq(Object.class));

    assertNull(result);
  }

  @Test
  public void test_deserializer_default_array() {
    when(deserializerMap.containsKey(eq(int[].class)))
        .thenReturn(false);
    when(mockDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(mockDeserializer);
    when(mockDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new int[] {1,2,3});

    assertDoesNotThrow(() -> manager.deserialize(int[].class, new JsonArray()));

    verify(deserializerMap).containsKey(eq(int[].class));
    verify(mockDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(mockDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  @Test
  public void test_deserializer_default_enum() {
    when(deserializerMap.containsKey(eq(TestEnum.class)))
        .thenReturn(false);
    when(mockDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(mockDeserializer);
    when(mockDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(TestEnum.TEST);

    assertDoesNotThrow(() -> manager.deserialize(TestEnum.class, new JsonPrimitive("TEST")));

    verify(deserializerMap).containsKey(eq(TestEnum.class));
    verify(mockDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(mockDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  @Test
  public void test_deserializer_default_collection() {
    when(deserializerMap.containsKey(eq(List.class)))
        .thenReturn(false);
    when(mockDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(mockDeserializer);
    when(mockDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new ArrayList<>());

    assertDoesNotThrow(() -> manager.deserialize(List.class, new JsonArray()));

    verify(deserializerMap).containsKey(eq(List.class));
    verify(mockDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(mockDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  @Test
  public void test_deserializer_default_map() {
    when(deserializerMap.containsKey(eq(Map.class)))
        .thenReturn(false);
    when(mockDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(mockDeserializer);
    when(mockDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new HashMap<>());

    assertDoesNotThrow(() -> manager.deserialize(Map.class, new JsonObject()));

    verify(deserializerMap).containsKey(eq(Map.class));
    verify(mockDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(mockDeserializer).deserialize(any(), any());
  }

  @Test
  public void test_deserializer_default_object() {
    when(deserializerMap.containsKey(eq(Object.class)))
        .thenReturn(false);
    when(mockDeserializer.createContextualDeserializer(any(DeserializerManager.class), any(Property.class)))
        .thenReturn(mockDeserializer);
    when(mockDeserializer.deserialize(any(JsonNode.class), any(DeserializerManager.class)))
        .thenReturn(new Object());

    assertDoesNotThrow(() -> manager.deserialize(Object.class, new JsonObject()));

    verify(deserializerMap).containsKey(eq(Object.class));
    verify(mockDeserializer).createContextualDeserializer(any(DeserializerManager.class), any(Property.class));
    verify(mockDeserializer).deserialize(any(JsonNode.class), any(DeserializerManager.class));
  }

  private enum TestEnum {
    TEST
  }

  @NoArgsConstructor
  @JsonDeserialize(DummyJsonDeserializer.class)
  private static class TestAnnotatedDeserializerCls extends TestDeserializerCls {
  }

  @NoArgsConstructor
  private static class TestDeserializerCls {
  }

  @NoArgsConstructor
  @JsonDeserialize(DummyContextualJsonDeserializer.class)
  private static class TestAnnotatedDeserializerClsCtx extends TestDeserializerClsCtx {
  }

  @NoArgsConstructor
  @JsonDeserialize(DummyPrivateContextualJsonDeserializer.class)
  private static class TestAnnotatedDeserializerClsCtx2 extends TestDeserializerClsCtx {
  }

  @NoArgsConstructor
  private static class TestDeserializerClsCtx {
  }

  @NoArgsConstructor
  private static class DummyJsonDeserializer implements JsonDeserializer<TestDeserializerCls> {
    @Override
    public TestDeserializerCls deserialize(JsonNode node, DeserializerManager manager) {
      return new TestAnnotatedDeserializerCls();
    }
  }

  @NoArgsConstructor
  private static class DummyContextualJsonDeserializer implements JsonDeserializer<TestDeserializerClsCtx>,
      ContextualDeserializer {

    @Override
    public DummyContextualJsonDeserializer createContextualDeserializer(DeserializerManager manager,
                                                            Property property) {
      return null;
    }

    @Override
    public TestDeserializerClsCtx deserialize(JsonNode node, DeserializerManager manager) {
      return new TestDeserializerClsCtx();
    }
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  private static class DummyPrivateContextualJsonDeserializer implements JsonDeserializer<TestDeserializerClsCtx>,
      ContextualDeserializer {

    @Override
    public DummyContextualJsonDeserializer createContextualDeserializer(DeserializerManager manager,
                                                                        Property property) {
      return null;
    }

    @Override
    public TestDeserializerClsCtx deserialize(JsonNode node, DeserializerManager manager) {
      return new TestDeserializerClsCtx();
    }
  }

  @NoArgsConstructor
  private static class MockContextualJsonDeserializer implements JsonDeserializer<Object>, ContextualDeserializer {
    @Override
    public JsonDeserializer<Object> createContextualDeserializer(DeserializerManager manager,
                                                            Property property) {
      return null;
    }

    @Override
    public Object deserialize(JsonNode node, DeserializerManager manager) {
      return null;
    }
  }
}
