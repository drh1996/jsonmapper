package com.drh.json.deserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.FieldProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Collection Deserializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CollectionDeserializerTest {

  private CollectionDeserializer collectionDeserializer;

  @Mock
  private DeserializerManager manager;

  @BeforeEach
  public void beforeEach() {
    collectionDeserializer = new CollectionDeserializer();
  }

  @Test
  public void test_create_contextual_deserializer() {
    JsonDeserializer<?> deserializer = collectionDeserializer.createContextualDeserializer(manager, new ClassProperty(int[].class));

    assertNotEquals(collectionDeserializer, deserializer);
  }

  @Test
  public void test_deserialize_list() throws NoSuchFieldException {
    List<Integer> expected = Arrays.asList(1, 2);
    when(manager.deserialize(eq(Integer.class), any(JsonNode.class), any(ClassProperty.class)))
        .thenReturn(1, 2);

    List<?> actual = (List<?>) collectionDeserializer
        .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("intList")))
        .deserialize(new JsonArray().add(1).add(2), manager);

    verify(manager, times(2)).deserialize(eq(Integer.class), any(JsonNode.class), any(ClassProperty.class));

    for (int i = 0; i < expected.size(); i++) {
      assertEquals(expected.get(i), actual.get(i));
    }
  }

  @Test
  public void test_deserialize_set() throws NoSuchFieldException {
    Set<Integer> expected = Set.of(1, 2);
    when(manager.deserialize(eq(Integer.class), any(JsonNode.class), any(ClassProperty.class)))
        .thenReturn(1, 2);

    Set<?> actual = (Set<?>) collectionDeserializer
        .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("intSet")))
        .deserialize(new JsonArray().add(1).add(2), manager);

    verify(manager, times(2)).deserialize(eq(Integer.class), any(JsonNode.class), any(ClassProperty.class));

    assertEquals(actual, expected);
  }

  @Test
  public void test_deserialize_queue() throws NoSuchFieldException {
    Queue<Integer> expected = new PriorityQueue<>(Arrays.asList(1, 2));
    when(manager.deserialize(eq(Integer.class), any(JsonNode.class), any(ClassProperty.class)))
        .thenReturn(1, 2);

    Queue<?> actual = (Queue<?>) collectionDeserializer
        .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("intQueue")))
        .deserialize(new JsonArray().add(1).add(2), manager);

    verify(manager, times(2)).deserialize(eq(Integer.class), any(JsonNode.class), any(ClassProperty.class));

    for (int i = 0; i < expected.size(); i++) {
      assertEquals(actual.poll(), expected.poll());
    }
  }

  @Test
  public void test_deserialize_invalid_constructor()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> collectionDeserializer
          .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testInvalidCollection")))
          .deserialize(new JsonArray().add(1).add(2), manager));

    assertEquals("No default constructor is present in class: com.drh.json.deserializer.CollectionDeserializerTest$TestCollectionInvalidConstructor", ex.getMessage());
  }

  @Test
  public void test_deserialize_invalid_parameterized_types()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> collectionDeserializer
            .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testNoParamType")))
            .deserialize(new JsonArray().add(1).add(2), manager));

    assertEquals("Could not obtain parameterized types for: 'com.drh.json.deserializer.CollectionDeserializerTest$TestCollectionNoParamType testNoParamType'", ex.getMessage());
  }

  @Test
  public void test_deserialize_constructor_throws()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> collectionDeserializer
            .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testConstructorThrows")))
            .deserialize(new JsonArray().add(1).add(2), manager));

    assertEquals("Failed to constructor object for class: com.drh.json.deserializer.CollectionDeserializerTest$TestCollectionConstructorThrows", ex.getMessage());
  }

  @Setter
  @Getter
  @NoArgsConstructor
  private static class TestFields {
    private List<Integer> intList;
    private Set<Integer> intSet;
    private PriorityQueue<Integer> intQueue;
    private TestCollectionInvalidConstructor<Integer> testInvalidCollection;
    private TestCollectionNoParamType testNoParamType;
    private TestCollectionConstructorThrows<Integer> testConstructorThrows;
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  private static class TestCollectionInvalidConstructor<T> extends ArrayList<T> {
  }

  @NoArgsConstructor
  private static class TestCollectionNoParamType extends ArrayList<Integer> {
  }

  private static class TestCollectionConstructorThrows<T> extends ArrayList<T> {
    public TestCollectionConstructorThrows() {
      throw new RuntimeException("test exception");
    }
  }
}
