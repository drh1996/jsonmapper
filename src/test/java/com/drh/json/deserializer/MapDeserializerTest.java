package com.drh.json.deserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonObject;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.FieldProperty;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Map Deserializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class MapDeserializerTest {

  private MapDeserializer mapDeserializer;

  @Mock
  private DeserializerManager manager;

  @BeforeEach
  public void beforeEach() {
    mapDeserializer = new MapDeserializer();
  }

  @Test
  public void test_create_contextual_deserializer() {
    JsonDeserializer<?> deserializer = mapDeserializer.createContextualDeserializer(manager, new ClassProperty(int[].class));

    assertNotEquals(mapDeserializer, deserializer);
  }

  @Test
  public void test_deserialize_map() throws NoSuchFieldException {
    Map<String, Integer> expected = Map.of("test", 1, "two", 2);
    when(manager.deserialize(any(), any(), any(ClassProperty.class))).thenReturn(1, 2);

    Map<?, ?> actual = (Map<?, ?>) mapDeserializer
        .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("intMap")))
        .deserialize(new JsonObject().add("test", 1).add("two", 2), manager);

    verify(manager, times(2)).deserialize(any(), any(), any(ClassProperty.class));

    for (Map.Entry<String, Integer> entry : expected.entrySet()) {
      assertTrue(actual.containsKey(entry.getKey()));
      assertEquals(entry.getValue(), actual.get(entry.getKey()));
    }
  }

  @Test
  public void test_deserialize_hashmap() throws NoSuchFieldException {
    Map<String, Integer> expected = Map.of("test", 1, "two", 2);
    when(manager.deserialize(any(), any(), any(ClassProperty.class))).thenReturn(1, 2);

    Map<?, ?> actual = (Map<?, ?>) mapDeserializer
        .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("intHashMap")))
        .deserialize(new JsonObject().add("test", 1).add("two", 2), manager);

    verify(manager, times(2)).deserialize(any(), any(), any(ClassProperty.class));

    for (Map.Entry<String, Integer> entry : expected.entrySet()) {
      assertTrue(actual.containsKey(entry.getKey()));
      assertEquals(entry.getValue(), actual.get(entry.getKey()));
    }
  }

  @Test
  public void test_deserialize_invalid_constructor()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> mapDeserializer
          .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testInvalidMap")))
          .deserialize(new JsonObject().add("test", 1).add("two", 2), manager));

    assertEquals("No default constructor is present in class: com.drh.json.deserializer.MapDeserializerTest$TestMapInvalidConstructor", ex.getMessage());
  }

  @Test
  public void test_deserialize_invalid_parameterized_types()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> mapDeserializer
            .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testNoParamType")))
            .deserialize(new JsonObject().add("test", 1).add("two", 2), manager));

    assertEquals("Could not obtain parameterized types for: 'com.drh.json.deserializer.MapDeserializerTest$TestMapNoParamType testNoParamType'", ex.getMessage());
  }

  @Test
  public void test_deserialize_constructor_throws()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> mapDeserializer
            .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testConstructorThrows")))
            .deserialize(new JsonObject().add("test", 1).add("two", 2), manager));

    assertEquals("Failed to constructor object for class: com.drh.json.deserializer.MapDeserializerTest$TestMapConstructorThrows", ex.getMessage());
  }

  @Test
  public void test_deserialize_invalid_key_type()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> mapDeserializer
            .createContextualDeserializer(manager, new FieldProperty(TestFields.class.getDeclaredField("testInvalidKeyType")))
            .deserialize(new JsonObject().add("test", 1).add("two", 2), manager));

    assertEquals("Map key type must be String for field: com.drh.json.deserializer.MapDeserializerTest$TestMapInvalidKeyType testInvalidKeyType", ex.getMessage());
  }

  @NoArgsConstructor
  private static class TestFields {
    private Map<String, Integer> intMap;
    private HashMap<String, Integer> intHashMap;
    private TestMapInvalidConstructor<String, Integer> testInvalidMap;
    private TestMapNoParamType testNoParamType;
    private TestMapConstructorThrows<String, Integer> testConstructorThrows;
    private TestMapInvalidKeyType<Object, Integer> testInvalidKeyType;
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  private static class TestMapInvalidConstructor<K, V> extends HashMap<K, V> {
  }

  @NoArgsConstructor
  private static class TestMapNoParamType extends HashMap<String, Integer> {
  }

  private static class TestMapConstructorThrows<K, V> extends HashMap<K, V> {
    public TestMapConstructorThrows() {
      throw new RuntimeException("test exception");
    }
  }

  @NoArgsConstructor
  private static class TestMapInvalidKeyType<K, V> extends HashMap<K, V> {
  }
}
