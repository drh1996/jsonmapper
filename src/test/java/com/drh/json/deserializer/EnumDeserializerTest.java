package com.drh.json.deserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonPrimitive;
import com.drh.json.property.ClassProperty;
import java.lang.reflect.Array;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Enum Deserializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class EnumDeserializerTest {

  private EnumDeserializer enumDeserializer;

  @Mock
  private DeserializerManager manager;

  @BeforeEach
  public void beforeEach() {
    enumDeserializer = new EnumDeserializer();
  }

  @Test
  public void test_create_contextual_deserializer() {
    JsonDeserializer<?> deserializer = enumDeserializer.createContextualDeserializer(manager, new ClassProperty(TestEnum.class));

    assertNotEquals(enumDeserializer, deserializer);
  }

  @Test
  public void test_create_contextual_deserializer_object() {
    JsonDeserializer<?> deserializer = enumDeserializer.createContextualDeserializer(manager, new ClassProperty(Enum.class));

    assertEquals(enumDeserializer, deserializer);
  }

  @Test
  public void test_deserialize() {

    TestEnum actual = (TestEnum) enumDeserializer
        .createContextualDeserializer(manager, new ClassProperty(TestEnum.class))
        .deserialize(new JsonPrimitive("TEST"), manager);

    assertEquals(TestEnum.TEST, actual);
  }

  private enum TestEnum {
    TEST
  }
}
