package com.drh.json.deserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.exception.JsonDeserializingException;
import com.drh.json.exception.JsonTransformException;
import com.drh.json.node.JsonObject;
import com.drh.json.property.ClassProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Object Deserializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ObjectDeserializerTest {

  private ObjectDeserializer objectDeserializer;

  @Mock
  private DeserializerManager manager;

  @BeforeEach
  public void beforeEach() {
    objectDeserializer = new ObjectDeserializer();
  }

  @Test
  public void test_create_contextual_deserializer() {
    JsonDeserializer<?> deserializer = objectDeserializer.createContextualDeserializer(manager, new ClassProperty(int[].class));

    assertNotEquals(objectDeserializer, deserializer);
  }

  @Test
  public void test_create_contextual_deserializer_object() {
    JsonDeserializer<?> deserializer = objectDeserializer.createContextualDeserializer(manager, new ClassProperty(Object.class));

    assertEquals(objectDeserializer, deserializer);
  }

  @Test
  public void test_deserialize_string() {
    when(manager.deserialize(any(), any(), any())).thenReturn("one");

    objectDeserializer
        .createContextualDeserializer(manager, new ClassProperty(StringWrapper.class))
        .deserialize(new JsonObject().add("string", "one"), manager);

    verify(manager, times(1)).deserialize(any(), any(), any());

  }

  @Test
  public void test_deserialize_invalid_field_constructor()  {
    when(manager.deserialize(any(), any(), any())).thenThrow(new JsonTransformException("test exception"));

    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> objectDeserializer
            .createContextualDeserializer(manager, new ClassProperty(StringWrapper.class))
            .deserialize(new JsonObject().add("string", "one"), manager));

    verify(manager, times(1)).deserialize(any(), any(), any());

    assertEquals("Failed to Deserialize to field 'string', reason: test exception", ex.getMessage());
  }

  @Test
  public void test_deserialize_constructor_class_constructor()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> objectDeserializer
            .createContextualDeserializer(manager, new ClassProperty(TestInvalidConstructor.class))
            .deserialize(new JsonObject().add("string", "one"), manager));

    assertEquals("No default constructor is present in class: com.drh.json.deserializer.ObjectDeserializerTest$TestInvalidConstructor", ex.getMessage());
  }

  @Test
  public void test_deserialize_constructor_throws()  {
    JsonDeserializingException ex = assertThrows(JsonDeserializingException.class,
        () -> objectDeserializer
            .createContextualDeserializer(manager, new ClassProperty(TestConstructorThrows.class))
            .deserialize(new JsonObject().add("string", "one"), manager));

    assertEquals("Failed to constructor object for class: com.drh.json.deserializer.ObjectDeserializerTest$TestConstructorThrows", ex.getMessage());
  }

  @NoArgsConstructor
  private static class StringWrapper {
    private String string;
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  private static class TestInvalidConstructor {
  }

  private static class TestConstructorThrows {
    public TestConstructorThrows() {
      throw new RuntimeException("test exception");
    }
  }

}
