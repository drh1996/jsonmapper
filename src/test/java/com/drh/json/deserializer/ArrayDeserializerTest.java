package com.drh.json.deserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.deserializer.manager.DeserializerManager;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNode;
import com.drh.json.property.ClassProperty;
import com.drh.json.property.Property;
import java.lang.reflect.Array;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Array Deserializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ArrayDeserializerTest {

  private ArrayDeserializer arrayDeserializer;

  @Mock
  private DeserializerManager manager;

  @BeforeEach
  public void beforeEach() {
    arrayDeserializer = new ArrayDeserializer();
  }

  @Test
  public void test_create_contextual_deserializer() {
    JsonDeserializer<?> deserializer = arrayDeserializer.createContextualDeserializer(manager, new ClassProperty(int[].class));

    assertNotEquals(arrayDeserializer, deserializer);
  }

  @Test
  public void test_create_contextual_deserializer_object() {
    JsonDeserializer<?> deserializer = arrayDeserializer.createContextualDeserializer(manager, new ClassProperty(Object[].class));

    assertEquals(arrayDeserializer, deserializer);
  }

  @Test
  public void test_deserialize() {
    int[] expected = {1, 2};
    when(manager.deserialize(eq(int.class), any(JsonNode.class)))
        .thenReturn(1, 2);

    Object actual = arrayDeserializer
        .createContextualDeserializer(manager, new ClassProperty(int[].class))
        .deserialize(new JsonArray().add(1).add(2), manager);

    verify(manager, times(2)).deserialize(eq(int.class), any(JsonNode.class));

    for (int i = 0; i < expected.length; i++) {
      assertEquals(expected[i], Array.get(actual, i));
    }
  }
}
