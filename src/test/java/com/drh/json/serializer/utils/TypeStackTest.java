package com.drh.json.serializer.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Type Stack Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TypeStackTest {

  private TypeStack typeStack;

  @BeforeEach
  public void beforeEach() {
    typeStack = new TypeStack();
  }

  @Test
  public void test_head() {
    int headType = typeStack.head();

    assertEquals(TypeStack.TYPE_NONE, headType);
  }

  @Test
  public void test_head_throws_when_empty() {
    assertFalse(typeStack.empty());

    int headType = typeStack.pop();
    assertEquals(TypeStack.TYPE_NONE, headType);

    assertTrue(typeStack.empty());

    assertThrows(NoSuchElementException.class, () -> typeStack.head());
  }

  @Test
  public void test_pop() {
    int headType = typeStack.pop();

    assertEquals(TypeStack.TYPE_NONE, headType);
  }

  @Test
  public void test_pop_throws_when_empty() {
    assertFalse(typeStack.empty());

    int headType = typeStack.pop();
    assertEquals(TypeStack.TYPE_NONE, headType);

    assertTrue(typeStack.empty());

    assertThrows(NoSuchElementException.class, () -> typeStack.pop());
  }

  @Test
  public void test_push() {
    assertEquals(TypeStack.TYPE_NONE, typeStack.head());

    typeStack.push(TypeStack.TYPE_OBJ);

    assertEquals(TypeStack.TYPE_OBJ, typeStack.head());
  }

}
