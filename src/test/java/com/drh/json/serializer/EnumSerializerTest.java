package com.drh.json.serializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Enum Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class EnumSerializerTest {

  private EnumSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @BeforeEach
  public void beforeEach() {
    serializer = new EnumSerializer();
  }

  @Test
  public void test_serialize_date() {
    when(generator.writeValue(anyString())).thenReturn(generator);

    ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
    serializer.serialize(TestEnum.ONE, generator, serializerManager);

    verify(generator).writeValue(captor.capture());

    assertEquals("ONE", captor.getValue());
  }

  @Test
  public void test_serialize_null() {
    when(generator.writeNull()).thenReturn(generator);

    serializer.serialize(null, generator, serializerManager);

    verify(generator).writeNull();
  }

  private enum TestEnum {
    ONE
  }
}
