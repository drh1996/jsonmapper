package com.drh.json.serializer;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonObject;
import com.drh.json.serializer.manager.SerializerManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Json Array Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonArraySerializerTest {

  private JsonArraySerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager manager;

  @BeforeEach
  public void beforeEach() {
    serializer = new JsonArraySerializer();
  }

  @Test
  public void test_serialize() {
    JsonArray obj = new JsonArray()
        .add("test")
        .add((String) null);

    when(generator.arrayStart()).thenReturn(generator);
    doNothing().when(manager).serialize(any(), any(), any());
    when(generator.arrayEnd()).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(obj, generator, manager));

    verify(generator).arrayStart();
    verify(manager, times(2)).serialize(any(), any(), any());
    verify(generator).arrayEnd();
  }

  @Test
  public void test_serialize_null() {
    when(generator.writeNull()).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(null, generator, manager));

    verify(generator).writeNull();
  }
}
