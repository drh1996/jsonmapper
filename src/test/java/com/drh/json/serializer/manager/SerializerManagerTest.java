package com.drh.json.serializer.manager;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.property.Property;
import com.drh.json.serializer.context.ContextualSerializer;
import com.drh.json.serializer.JsonSerializer;
import com.drh.json.styling.PrettyJsonStyling;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Array Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class SerializerManagerTest {

  @Mock
  private Map<Class<?>, JsonSerializer> mockSerializerMap;

  @Mock
  private JsonSerializer<?> mockSerializer;

  @Mock
  private JsonGenerator generator;

  private SerializerManager manager;

  @BeforeEach
  public void beforeEach() throws NoSuchFieldException, IllegalAccessException {
    manager = new SerializerManager(mockSerializerMap);
    replaceInternalFieldWithMock(manager, "defaultEnumSerializer", mockSerializer);
    replaceInternalFieldWithMock(manager, "defaultObjectSerializer", mockSerializer);
    replaceInternalFieldWithMock(manager, "defaultCollectionSerializer", mockSerializer);
    replaceInternalFieldWithMock(manager, "defaultArraySerializer", mockSerializer);
    replaceInternalFieldWithMock(manager, "defaultMapSerializer", mockSerializer);
  }

  @Test
  public void test_add_serializer() {
    when(mockSerializerMap.put(any(), any(JsonSerializer.class))).thenReturn(null);

    manager.addSerializer(Object.class, (obj, gen, serializerManager) -> {
    });

    verify(mockSerializerMap).put(any(), any(JsonSerializer.class));
  }

  @Test
  public void test_create_generator() {
    assertDoesNotThrow(() -> manager.createGenerator(StandardCharsets.UTF_8, new PrettyJsonStyling()));
  }

  @Test
  public void test_create_generator_invalid_charset() {
    IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
        () -> manager.createGenerator(StandardCharsets.UTF_16, new PrettyJsonStyling()));

    assertEquals("Unsupported charset: UTF-16", ex.getMessage());
  }

  @Test
  public void test_serialize_using_map() {
    when(mockSerializerMap.containsKey(any())).thenReturn(true);
    when(mockSerializerMap.get(any())).thenReturn(mockSerializer);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(String.class, "one", generator);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializerMap).get(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_using_enum_serializer() {
    when(mockSerializerMap.containsKey(any())).thenReturn(false);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(TestEnum.class, TestEnum.ONE, generator);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_using_collection_serializer() {
    when(mockSerializerMap.containsKey(any())).thenReturn(false);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(List.class, List.of("one"), generator);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_using_array_serializer() {
    when(mockSerializerMap.containsKey(any())).thenReturn(false);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(int[].class, new int[] {1}, generator);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_using_map_serializer() {
    when(mockSerializerMap.containsKey(any())).thenReturn(false);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(Map.class, Map.of("one", 1), generator);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_using_object_serializer() {
    when(mockSerializerMap.containsKey(any())).thenReturn(false);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(Object.class, new Object(), generator);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_contextual() {
    TestSerializer mockContextualSerializer = mock(TestSerializer.class);
    when(mockSerializerMap.containsKey(any())).thenReturn(true);
    when(mockSerializerMap.get(any())).thenReturn(mockContextualSerializer);
    when(mockContextualSerializer.createContextualSerializer(any(), any())).thenReturn(mockSerializer);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(String.class, "one", generator, null);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializerMap).get(any());
    verify(mockContextualSerializer).createContextualSerializer(any(), any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_with_field_not_contextual() {
    when(mockSerializerMap.containsKey(any())).thenReturn(true);
    when(mockSerializerMap.get(any())).thenReturn(mockSerializer);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(String.class, "one", generator, null);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializerMap).get(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  @Test
  public void test_serialize_with_field_using_default_serializer() {
    when(mockSerializerMap.containsKey(any())).thenReturn(false);
    doNothing().when(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));

    manager.serialize(String.class, "one", generator, null);

    verify(mockSerializerMap).containsKey(any());
    verify(mockSerializer).serialize(any(), any(JsonGenerator.class), any(SerializerManager.class));
  }

  private void replaceInternalFieldWithMock(SerializerManager manager, String fieldName, Object mock)
      throws NoSuchFieldException, IllegalAccessException {
    Field field = SerializerManager.class.getDeclaredField(fieldName);
    field.setAccessible(true);
    field.set(manager, mock);
    field.setAccessible(false);
  }

  private enum TestEnum {
    ONE
  }

  @NoArgsConstructor
  private static class TestSerializer implements JsonSerializer<Object>, ContextualSerializer {
    @Override
    public JsonSerializer createContextualSerializer(SerializerManager manager, Property property) {
      return this;
    }

    @Override
    public void serialize(Object obj, JsonGenerator gen, SerializerManager serializerManager) {
      throw new UnsupportedOperationException("this is a test class");
    }
  }
}
