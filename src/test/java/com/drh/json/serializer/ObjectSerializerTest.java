package com.drh.json.serializer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.time.LocalDate;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Map Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ObjectSerializerTest {

  private ObjectSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @BeforeEach
  public void beforeEach() {
    serializer = new ObjectSerializer();
  }

  @Test
  public void test_serialize_object() {
    TestObject obj = new TestObject();

    when(generator.objectStart()).thenReturn(generator);
    when(generator.writeField(anyString())).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), any(), any(JsonGenerator.class), any());
    when(generator.objectEnd()).thenReturn(generator);

    serializer.serialize(obj, generator, serializerManager);

    verify(generator).objectStart();
    verify(generator, times(5)).writeField(anyString());
    verify(serializerManager, times(5)).serialize(any(), any(), any(JsonGenerator.class), any());
    verify(generator).objectEnd();
  }

  @Test
  public void test_serialize_null_object() {
    when(generator.writeNull()).thenReturn(generator);

    serializer.serialize(null, generator, serializerManager);

    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_empty_object() {
    when(generator.objectStart()).thenReturn(generator);
    when(generator.objectEnd()).thenReturn(generator);

    serializer.serialize(new TestEmptyObject(), generator, serializerManager);

    verify(generator).objectStart();
    verify(generator).objectEnd();
  }

  private class TestEmptyObject {
  }

  @Getter
  private static class TestObject {
    private int x = 1;
    private String y = "one";
    private boolean z = false;

    @JsonDateFormat(format = "dd/MM/yyyy")
    private LocalDate date;

    public String getTestMethod(){
      return "should be called";
    }

    public static String getTestStaticMethod(){
      return "should not be called";
    }
  }
}
