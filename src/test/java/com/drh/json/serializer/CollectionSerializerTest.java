package com.drh.json.serializer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Collection Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CollectionSerializerTest {

  private CollectionSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @BeforeEach
  public void beforeEach() {
    serializer = new CollectionSerializer();
  }

  @ParameterizedTest
  @MethodSource("provideCollectionToSerialize")
  public void test_serialize_list(Collection<?> collection) {
    when(generator.arrayStart()).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), any(), any(JsonGenerator.class));
    when(generator.arrayEnd()).thenReturn(generator);

    serializer.serialize(collection, generator, serializerManager);

    verify(generator).arrayStart();
    verify(serializerManager, times(collection.size())).serialize(any(), any(), any(JsonGenerator.class));
    verify(generator).arrayEnd();
  }

  @Test
  public void test_serialize_empty_list() {
    List<Integer> list = List.of();

    when(generator.arrayStart()).thenReturn(generator);
    when(generator.arrayEnd()).thenReturn(generator);

    serializer.serialize(list, generator, serializerManager);

    verify(generator).arrayStart();
    verify(serializerManager, never()).serialize(any(), any(), any(JsonGenerator.class));
    verify(generator).arrayEnd();
  }

  @Test
  public void test_serialize_null_list() {
    List<Integer> list = null;

    when(generator.writeNull()).thenReturn(generator);

    serializer.serialize(list, generator, serializerManager);
    
    verify(generator).writeNull();
    verify(serializerManager, never()).serialize(any(), any(), any(JsonGenerator.class));
  }

  @Test
  public void test_serialize_null_elements() {
    List<Integer> list = Arrays.asList(null, null);

    when(generator.arrayStart()).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), any(), any(JsonGenerator.class));
    when(generator.arrayEnd()).thenReturn(generator);

    serializer.serialize(list, generator, serializerManager);

    verify(generator).arrayStart();
    verify(serializerManager, times(2)).serialize(any(), any(), any(JsonGenerator.class));
    verify(generator).arrayEnd();
  }

  private static Stream<Arguments> provideCollectionToSerialize() {
    return Stream.of(
        Arguments.of(List.of(1, 2, 3, 4, 5)),
        Arguments.of(List.of(1.2D, 2.1D, 3.5D, 4.9D, 5.0D)),
        Arguments.of(List.of(true, false)),
        Arguments.of(List.of('a', 'b', 'c', 'd', 'e')),
        Arguments.of(List.of("one", "two", "three", "four", "five")),
        Arguments.of(Set.of(1, 2, 3, 4, 5)),
        Arguments.of(Set.of(1.2D, 2.1D, 3.5D, 4.9D, 5.0D)),
        Arguments.of(Set.of(true, false)),
        Arguments.of(Set.of('a', 'b', 'c', 'd', 'e')),
        Arguments.of(Set.of("one", "two", "three", "four", "five")),
        Arguments.of(new PriorityQueue<>(List.of(1, 2, 3, 4, 5))),
        Arguments.of(new PriorityQueue<>(Set.of(1.2D, 2.1D, 3.5D, 4.9D, 5.0D))),
        Arguments.of(new PriorityQueue<>(Set.of(true, false))),
        Arguments.of(new PriorityQueue<>(Set.of('a', 'b', 'c', 'd', 'e'))),
        Arguments.of(new PriorityQueue<>(Set.of("one", "two", "three", "four", "five")))
    );
  }
}
