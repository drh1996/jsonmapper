package com.drh.json.serializer;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Primitive Serializers Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PrimitiveSerializersTest {

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @Test
  public void test_get_primitive_serializers() {
    Map<Class<?>, JsonSerializer<?>> serializerMap = PrimitiveSerializers.getPrimitiveSerializers();

    assertEquals(17, serializerMap.size());
  }

  @Test
  public void test_serialize_string() {
    PrimitiveSerializers.StringSerializer serializer = new PrimitiveSerializers.StringSerializer();

    when(generator.writeValue(anyString())).thenReturn(generator);
    serializer.serialize("one", generator, serializerManager);
    verify(generator).writeValue(anyString());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_character() {
    PrimitiveSerializers.CharacterSerializer serializer = new PrimitiveSerializers.CharacterSerializer();

    when(generator.writeValue(anyChar())).thenReturn(generator);
    serializer.serialize('1', generator, serializerManager);
    verify(generator).writeValue(anyChar());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_boolean() {
    PrimitiveSerializers.BooleanSerializer serializer = new PrimitiveSerializers.BooleanSerializer();

    when(generator.writeValue(anyBoolean())).thenReturn(generator);
    serializer.serialize(true, generator, serializerManager);
    verify(generator).writeValue(anyBoolean());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_integer() {
    PrimitiveSerializers.IntegerSerializer serializer = new PrimitiveSerializers.IntegerSerializer();

    when(generator.writeValue(anyInt())).thenReturn(generator);
    serializer.serialize(1, generator, serializerManager);
    verify(generator).writeValue(anyInt());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_short() {
    PrimitiveSerializers.ShortSerializer serializer = new PrimitiveSerializers.ShortSerializer();

    when(generator.writeValue(anyInt())).thenReturn(generator);
    serializer.serialize((short) 1, generator, serializerManager);
    verify(generator).writeValue(anyInt());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_long() {
    PrimitiveSerializers.LongSerializer serializer = new PrimitiveSerializers.LongSerializer();

    when(generator.writeValue(anyLong())).thenReturn(generator);
    serializer.serialize(1L, generator, serializerManager);
    verify(generator).writeValue(anyLong());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_byte() {
    PrimitiveSerializers.ByteSerializer serializer = new PrimitiveSerializers.ByteSerializer();

    when(generator.writeValue(anyInt())).thenReturn(generator);
    serializer.serialize((byte) 1, generator, serializerManager);
    verify(generator).writeValue(anyInt());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_float() {
    PrimitiveSerializers.FloatSerializer serializer = new PrimitiveSerializers.FloatSerializer();

    when(generator.writeValue(anyFloat())).thenReturn(generator);
    serializer.serialize(1.0F, generator, serializerManager);
    verify(generator).writeValue(anyFloat());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_double() {
    PrimitiveSerializers.DoubleSerializer serializer = new PrimitiveSerializers.DoubleSerializer();

    when(generator.writeValue(anyDouble())).thenReturn(generator);
    serializer.serialize(1.0D, generator, serializerManager);
    verify(generator).writeValue(anyDouble());

    when(generator.writeNull()).thenReturn(generator);
    serializer.serialize(null, generator, serializerManager);
    verify(generator).writeNull();
  }

}
