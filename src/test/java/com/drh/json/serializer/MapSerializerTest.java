package com.drh.json.serializer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Map Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class MapSerializerTest {

  private MapSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @BeforeEach
  public void beforeEach() {
    serializer = new MapSerializer();
  }

  @ParameterizedTest
  @MethodSource("provideMapsToSerialize")
  public void test_serialize_list(Map<?, ?> map) {
    when(generator.objectStart()).thenReturn(generator);
    when(generator.writeField(anyString())).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), any(), any(JsonGenerator.class));
    when(generator.objectEnd()).thenReturn(generator);

    serializer.serialize(map, generator, serializerManager);

    verify(generator).objectStart();
    verify(generator, times(map.size())).writeField(anyString());
    verify(serializerManager, times(map.size())).serialize(any(), any(), any(JsonGenerator.class));
    verify(generator).objectEnd();
  }

  @Test
  public void test_serialize_empty_map() {
    Map<Integer, Integer> map = Map.of();

    when(generator.objectStart()).thenReturn(generator);
    when(generator.objectEnd()).thenReturn(generator);

    serializer.serialize(map, generator, serializerManager);

    verify(generator).objectStart();
    verify(generator).objectEnd();
  }

  @Test
  public void test_serialize_null_map() {
    when(generator.writeNull()).thenReturn(generator);

    serializer.serialize(null, generator, serializerManager);

    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_null_elements() {
    Map<Integer, Integer> map = new HashMap<>();
    map.put(1, 100);
    map.put(2, null);

    when(generator.objectStart()).thenReturn(generator);
    when(generator.writeField(anyString())).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), any(), any(JsonGenerator.class));
    when(generator.writeNull()).thenReturn(generator);
    when(generator.objectEnd()).thenReturn(generator);

    serializer.serialize(map, generator, serializerManager);

    verify(generator).objectStart();
    verify(generator, times(map.size())).writeField(anyString());
    verify(serializerManager).serialize(any(), any(), any(JsonGenerator.class));
    verify(generator).writeNull();
    verify(generator).objectEnd();
  }

  private static Stream<Arguments> provideMapsToSerialize() {
    return Stream.of(
        Arguments.of(Map.of(1, 1000, 2, 2000)),
        Arguments.of(Map.of(1.2D, 2.1D, 3.5D, 4.9D)),
        Arguments.of(Map.of(true, false)),
        Arguments.of(Map.of('a', 'b', 'c', 'd')),
        Arguments.of(Map.of("one", "two", "three", "four"))
    );
  }
}
