package com.drh.json.serializer;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonArray;
import com.drh.json.node.JsonNull;
import com.drh.json.serializer.manager.SerializerManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Json Null Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonNullSerializerTest {

  private JsonNullSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager manager;

  @BeforeEach
  public void beforeEach() {
    serializer = new JsonNullSerializer();
  }

  @Test
  public void test_serialize() {
    when(generator.writeNull()).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonNull(), generator, manager));

    verify(generator).writeNull();
  }
}
