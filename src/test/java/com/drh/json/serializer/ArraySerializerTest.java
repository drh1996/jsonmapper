package com.drh.json.serializer;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Array Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ArraySerializerTest {

  private ArraySerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @BeforeEach
  public void beforeEach() {
    serializer = new ArraySerializer();
  }

  @Test
  public void test_serialize_int_array() {
    int[] arr = {1,2,3,4,5};

    when(generator.arrayStart()).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), anyInt(), any(JsonGenerator.class));
    when(generator.arrayEnd()).thenReturn(generator);

    serializer.serialize(arr, generator, serializerManager);

    verify(generator).arrayStart();
    verify(serializerManager, times(5)).serialize(any(), anyInt(), any(JsonGenerator.class));
    verify(generator).arrayEnd();
  }

  @Test
  public void test_serialize_empty_array() {
    int[] arr = {};

    when(generator.arrayStart()).thenReturn(generator);
    when(generator.arrayEnd()).thenReturn(generator);

    serializer.serialize(arr, generator, serializerManager);

    verify(generator).arrayStart();
    verify(serializerManager, never()).serialize(any(), anyInt(), any(JsonGenerator.class));
    verify(generator).arrayEnd();
  }

  @Test
  public void test_serialize_null_array() {
    int[] arr = null;

    when(generator.writeNull()).thenReturn(generator);

    serializer.serialize(arr, generator, serializerManager);

    verify(generator).writeNull();
  }

  @Test
  public void test_serialize_null_elements() {
    String[] arr = {null, null};

    when(generator.arrayStart()).thenReturn(generator);
    doNothing().when(serializerManager).serialize(any(), eq(null), any(JsonGenerator.class));
    when(generator.arrayEnd()).thenReturn(generator);

    serializer.serialize(arr, generator, serializerManager);

    verify(generator).arrayStart();
    verify(serializerManager, times(2)).serialize(any(), eq(null), any(JsonGenerator.class));
    verify(generator).arrayEnd();
  }
}
