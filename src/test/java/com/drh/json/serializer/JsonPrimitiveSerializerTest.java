package com.drh.json.serializer;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyByte;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyShort;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.node.JsonNull;
import com.drh.json.node.JsonPrimitive;
import com.drh.json.serializer.manager.SerializerManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Json Primitive Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JsonPrimitiveSerializerTest {

  private JsonPrimitiveSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager manager;

  @BeforeEach
  public void beforeEach() {
    serializer = new JsonPrimitiveSerializer();
  }

  @Test
  public void test_serialize_string() {
    when(generator.writeValue(anyString())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive("test"), generator, manager));

    verify(generator).writeValue(anyString());
  }

  @Test
  public void test_serialize_bool() {
    when(generator.writeValue(anyBoolean())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive(true), generator, manager));

    verify(generator).writeValue(anyBoolean());
  }

  @Test
  public void test_serialize_int() {
    when(generator.writeValue(anyInt())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive(1), generator, manager));

    verify(generator).writeValue(anyInt());
  }

  @Test
  public void test_serialize_double() {
    when(generator.writeValue(anyDouble())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive(1D), generator, manager));

    verify(generator).writeValue(anyDouble());
  }

  @Test
  public void test_serialize_float() {
    when(generator.writeValue(anyFloat())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive(1F), generator, manager));

    verify(generator).writeValue(anyFloat());
  }

  @Test
  public void test_serialize_long() {
    when(generator.writeValue(anyLong())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive(1L), generator, manager));

    verify(generator).writeValue(anyLong());
  }

  @Test
  public void test_serialize_short() {
    when(generator.writeValue(anyInt())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive((short) 1), generator, manager));

    verify(generator).writeValue(anyInt());
  }

  @Test
  public void test_serialize_byte() {
    when(generator.writeValue(anyInt())).thenReturn(generator);

    assertDoesNotThrow(() -> serializer.serialize(new JsonPrimitive((byte) 1), generator, manager));

    verify(generator).writeValue(anyInt());
  }
}
