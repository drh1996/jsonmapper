package com.drh.json.serializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.annotation.JsonDateFormat;
import com.drh.json.generator.JsonGenerator;
import com.drh.json.property.FieldProperty;
import com.drh.json.serializer.manager.SerializerManager;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Date Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class DateSerializerTest {

  private DateSerializer serializer;

  @Mock
  private JsonGenerator generator;

  @Mock
  private SerializerManager serializerManager;

  @Captor
  private ArgumentCaptor<String> dateCaptor;

  @BeforeEach
  public void beforeEach() {
    serializer = new DateSerializer(new SimpleDateFormat("yyyy-MM-dd"));
  }

  @Test
  public void test_serialize() {
    when(generator.writeValue(anyString())).thenReturn(generator);

    serializer.serialize(Date.from(Instant.parse("2023-08-13T17:00:00Z")), generator, serializerManager);

    verify(generator).writeValue(anyString());
  }

  @Test
  public void test_serialize_date_contextual() throws NoSuchFieldException {

    JsonSerializer<Date> contextualSerializer = serializer.createContextualSerializer(
        serializerManager,
        new FieldProperty(TestCls.class.getDeclaredField("dateWithAnnot")));

    assertNotEquals(serializer, contextualSerializer);

    when(generator.writeValue(anyString())).thenReturn(generator);

    contextualSerializer.serialize(Date.from(Instant.parse("2023-08-13T00:00:00Z")), generator, serializerManager);

    verify(generator).writeValue(dateCaptor.capture());

    assertEquals("13/08/2023", dateCaptor.getValue());
  }

  @Test
  public void test_serialize_date_non_contextual() throws NoSuchFieldException {

    JsonSerializer<Date> contextualSerializer = serializer.createContextualSerializer(
        serializerManager,
        new FieldProperty(TestCls.class.getDeclaredField("dateNoAnnot")));

    assertEquals(serializer, contextualSerializer);

    when(generator.writeValue(anyString())).thenReturn(generator);

    contextualSerializer.serialize(Date.from(Instant.parse("2023-08-13T00:00:00Z")), generator, serializerManager);

    verify(generator).writeValue(dateCaptor.capture());

    assertEquals("2023-08-13", dateCaptor.getValue());
  }

  @Test
  public void test_serialize_null() {
    when(generator.writeNull()).thenReturn(generator);

    serializer.serialize(null, generator, serializerManager);

    verify(generator).writeNull();
  }

  private static class TestCls {
    @JsonDateFormat(format = "dd/MM/yyyy")
    private Date dateWithAnnot;
    private Date dateNoAnnot;
  }
}
