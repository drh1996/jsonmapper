package com.drh.json.reference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Type;
import java.time.LocalDate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayName("Parameterized Type Property Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TypeReferenceTest {

  @Test
  public void test_get_type() {
    TypeReference<TestCls<LocalDate>> typeRef = new TypeReference<>() {};

    Type type = typeRef.getType();
    assertNotNull(type);
    assertEquals("com.drh.json.reference.TypeReferenceTest$TestCls<java.time.LocalDate>", type.getTypeName());

  }

  @Test
  public void test_no_param() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> new TypeReference() {});

    assertEquals("Invalid Type Reference", e.getMessage());
  }

  private static class TestCls<T> {
  }
}
