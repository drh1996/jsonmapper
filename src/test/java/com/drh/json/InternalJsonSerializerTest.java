package com.drh.json;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.drh.json.generator.JsonGenerator;
import com.drh.json.serializer.manager.SerializerManager;
import com.drh.json.styling.JsonStyling;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Internal Json Serializer Unit Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InternalJsonSerializerTest {

  @Captor
  private ArgumentCaptor<JsonGenerator> generatorCaptor;

  @Mock
  private SerializerManager manager;

  @Mock
  private JsonStyling styling;

  @Mock
  private JsonGenerator generator;

  private InternalJsonSerializer serializer;

  @BeforeEach
  public void beforeEach() throws NoSuchFieldException, IllegalAccessException {
    serializer = new InternalJsonSerializer(styling, manager);
  }

  @Test
  public void test_serialize() {
    when(manager.createGenerator(any(), any())).thenReturn(generator);
    doNothing().when(manager).serialize(any(), any(), any());
    when(generator.getOutput()).thenReturn("[]");

    assertDoesNotThrow(() -> serializer.serialize(emptyList()));

    verify(manager).createGenerator(any(), any());
    verify(manager).serialize(any(), any(), generatorCaptor.capture());
    verify(generator).getOutput();
  }

  @Test
  public void test_add_serializer() {
    doNothing().when(manager).addSerializer(any(), any());

    assertDoesNotThrow(() -> serializer.addSerializer(
        Object.class, (obj, gen, serializerManager) -> {}));

    verify(manager).addSerializer(any(), any());
  }
}
